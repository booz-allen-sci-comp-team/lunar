# Lunar
__Updated 8.9.2016__

## Getting Started

###API Endpoints

PRODUCTION

     https://172.25.234.51/dictationlensapi

DEVELOPMENT

     https://127.0.0.1:8000/dictationlensapi


### Starting API

from lunar home:

Start API:

     python dictation/api.py [-d development server 127.0.0.1:8000]
                             [-m model terms][-c curated terms] <-- one of these is required

###Key Resources

Trained model
      
      C:\DataExtracts\Dictations\word2vec\w2v.model

Curated terms 

     C:\DataExtracts\Dictations\word2vec\matchcandidates.xlsx


Custom stop phrases

      C:\DataExtracts\chief_complaints\stop_words.txt


Backpain IVDU use case terms

       ..\lunar\backpain_ivdu_output.txt






##Older (possibly outdated) material

### Requirements

## Metamap Parsing

### Parsing

#### Start by parsing the critical terms
Save the previous 'critical_terms_acr_processed.xml' generated to an empty input folder, as in 'lunar/input/critical_terms_acr_processed.xml'
Use -s (--store) flag for storing output to the database
Use -d (--debug) to print metamap parsing data

```python
python parse_report.py xml_reports/ -s [-d]
```
This should always be done before parsing other documents so the critical terms will have report_id of 1.

### Parse sample documents

```python
python parse_report.py radiology_sample_output/ [-s] [-d]
```

### Return critical matches
The script `compare terms.py` loads a critical terms dictionary from report_id 1, and then compares the reports with report_id 2 and 3, returning a critical result only if the report being compared has all candidates that a the given critical result mapped to. If you loaded the two documents from `radiology_sample_output` in the previous section, you should see output that indicates reports 3 and 4 were non-critical, and report 2 and 5 mapped to 'large subchorionic hemorrhage' and 'Pneumothorax' respectively.