'''
Extract cohort timeline data incl., diagnoses and procedures
'''
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dt

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

matplotlib.style.use('ggplot')
from sqlalchemy import create_engine
from dictation.utils import mssql_connect
server = '10.130.172.93'
adt_connection = mssql_connect(server=server, database="azADT")
import sys, os, inspect

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

case_level = """
select *  from [azADT].[dbo].[v_DG1601] (nolock) as diag
inner join
	(select EID, pid.PatientMedicalRecordNumber from
	 [azADT].[dbo].[v_PID601] as pid
	inner join
	(select  distinct top 10 PatientMedicalRecordNumber FROM /*only take 10 pts for testing*/
	  (SELECT  distinct EID /*,DiagnosisDateTime,Diagnosis,DiagnosisCode*/
	  FROM [azADT].[dbo].[v_DG1601] (nolock)
	  where DiagnosisCode like ('%I2%') /*CAD codes*/
	  and DiagnosisDateTime > '2015-01-01 00:00:00.000' and  /* diagnosis in 2015*/
	  DiagnosisDateTime < '2016-01-01 00:00:00.000') as cad_ids
	  inner join
		  [azADT].[dbo].[v_PID601] (nolock) as pt_ids
	  on cad_ids.EID = pt_ids.EID) as cohort_mrns
	  on pid.PatientMedicalRecordNumber = cohort_mrns.PatientMedicalRecordNumber) as EID_MRN
on diag.EID = EID_MRN.EID
inner join
(select EID, ProcedureDescription, ProcedureCode, ProcedureDateTime from [azADT].[dbo].[v_PR1601] (nolock)) as procs
on diag.EID = procs.EID
"""

print "reading cohort data from Amalga"
df = pd.read_sql(case_level, adt_connection)
print "done"
#Set time series index
#df = df.set_index(df.DiagnosisDateTime)
first_pt = df.groupby(df.PatientMedicalRecordNumber).count().index[0]
case = df[df.PatientMedicalRecordNumber==first_pt]
plt.figure()
mx = case.groupby('Diagnosis').DiagnosisDateTime.max()
mn = case.groupby('Diagnosis').DiagnosisDateTime.min()
prc = case.groupby('ProcedureDescription').ProcedureDateTime.min()
d=pd.concat([mn,mx],1)
d.columns = ['mn','mx']
d=d.dropna()
d['y'] = np.arange(len(d))+1
d['amin'] = pd.to_datetime(d.mx).astype(datetime)
d['amax'] = pd.to_datetime(d.mn).astype(datetime)
d.amax = d.amax+pd.DateOffset(days=30)
#d=d.sort_values('amin', ascending=False)
plt.hlines(d.y.values, d.amin.values, d.amax.values, lw=5, color='blue', alpha=.8)
#plt.hlines(d.head().y.values, d.head().amin.values, d.head().amax.values, lw=5, color='blue')
#plt.xticks(np.arange(min(d.y), max(d.y)+1, 1.0))
plt.axes().xaxis_date()
plt.yticks(np.arange(min(d.y.values), max(d.y.values)+1, 1.0))
#plt.axes().xaxis_date()
plt.axes().set_yticklabels(d.index.values)
#[plt.axvline(k, lw=.5,color='grey', ls='dashed') for k in d.amax.unique()]
[plt.axvline(k, lw=.5,color='grey', ls='dashed') for k in d.amin.unique()]
#[plt.text(k,d.y.max()+1,'Chief Complaint', rotation=90, fontsize=8) for k in d.amax.unique()]
[plt.text(k,d.y.max()+1,'Chief Complaint', rotation=90, fontsize=8) for k in d.amin.unique()]
[plt.text(k,d.y.min()+10,i, rotation=45, fontsize=8, color='grey')  for i,k in prc.iteritems()]
plt.show()