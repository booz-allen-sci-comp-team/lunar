Name:
Patient ID:
History:
Date of Birth:
Study: MRI Right Shoulder
Facility:
Physician: XXXXX XXXXX, MD
Date of Service: xx/xx/xxxx xx:xx:xx
PROCEDURE: MRI Right Shoulder
REASON FOR EXAM: Male, 15 years old. The patient complains of shoulder pain x 2 weeks. The patient plays baseball
and is a pitcher.
TECHNIQUE: Standardized fat and water weighted pulse sequences were obtained in all 3 orthogonal planes. The
following pulse sequences were obtained: Axial T1, T2; coronal oblique: T1, T2, STIR; sagittal: T2 fat sat.
COMPARISON: None.
FINDINGS: Normal supraspinatus tendon. Normal infraspinatus tendon. Normal subscapularis tendon. Normal teres
minor tendon.
Normal supraspinatus muscle. Normal infraspinatus muscle. Normal subscapularis muscle. Normal teres minor muscle.
Normal glenohumeral articulation. Normal humeral head and visualized proximal humerus. Normal biceps labral
complex. Normal intracapsular long biceps tendon. Normal labrum. Normal capsuloligamentous complex. Normal
rotator interval.
Normal acromioclavicular articulation. There is an unfused acromial apophysis with intense cancellous marrow
edema (sagittal T2 fat sat series 7, image 10), with mild surrounding soft tissue edema (coronal STIR series 5, image
10). The findings are consistent with apophysitis of the unfused acromial apophysis. There is a Type I morphology (flat
undersurface), with a posterior downsloping orientation. There is no subacromial-subdeltoid bursal fluid.
Normal visualized coracohumeral and coracoacromial ligaments. Normal quadrilateral space. Normal axillary space.
Normal deltoid muscle. Normal trapezius muscle.
IMPRESSION:
1. Unfused acromial apophysis with intense cancellous marrow edema and surrounding soft tissue edema consistent
with an acromial apophysitis.
2. Type I morphology the acromion with a posterior downsloping orientation.
3. Normal rotator cuff.
