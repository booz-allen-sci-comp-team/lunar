
# coding: utf-8

# In[121]:

import pymssql
import pandas as pd
import numpy as np
import warnings
warnings.simplefilter(action = "ignore")
import matplotlib
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from patsy import *
conn = pymssql.connect(server='MI2-BAH-DSDB01', user='SXB806', password='4SB@mi2*', database='Medstar')
pau_df = pd.read_sql('SELECT * from MasterPAU', conn)


# In[122]:

eligible = pau_df[(pau_df["ELIGIBLE FOR A READMISSION - DENOMINATOR"]==1)]


# Calculate sample size for control cases assuming a 14% readmission rate across the board
# http://www.chiamass.gov/assets/Uploads/A-Focus-on-Provider-Quality-Jan-2015.pdf

# In[123]:

N_control = (100/14)*28610 


# In[124]:

#select target and control sample
eligible['readm_30day'] = eligible['READMISSION WITHIN 30 DAYS']
readmitted = eligible[eligible['readm_30day']==1]
readmitted['cond']='target'
#don't use cases with missing bdays
readmitted = readmitted[~readmitted['BirthDate'].isnull()] 
control = pd.read_sql('select  * from Demographic where EID in (select top {} EID from Demographic order by newid())'.format(N_control), conn)


# In[125]:

control['cond']='control'
control = control[~control['BirthDate'].isnull()]
df = pd.concat([readmitted,control], axis=0)
pd.DataFrame([control.shape, readmitted.shape, df.shape], index=['control', 'readmitted', 'total'], columns=['Rows','Columns'])


# In[126]:

df.groupby(['cond','GenderCode'])['GenderCode'].count().unstack(-1)[['F','M']].plot(kind='bar')


# In[127]:

import datetime as DT
now = pd.Timestamp(DT.datetime.now())
dob = pd.to_datetime(df.BirthDate, errors='coerce')
#dob = pd.to_datetime(df['BirthDate']
df['age'] = (((now - dob).dt.days)/365).round(1)
df['agegroup']=pd.cut(df['age'],8)
counts = df.groupby(['cond','agegroup']).BirthDate.count()


# In[128]:

counts.unstack(0)[0:-2].plot( kind='line',  rot=90,  title='Case counts by age group and condition')


# ## Readmission Latency (Days)
# * Get patients eligible for readm
# * Get patients readmitted within 30 days
# * See if there is a relationship between readmission latency, condition and cost

# In[129]:

readm = eligible[eligible['readm_30day'] ==1]
conditions = list(readm.groupby('DESCRIPTION OF APR_DRG').count().sort('HOSPITAL ID',0, ascending=False).head(10).index)
readm.index = readm['DESCRIPTION OF APR_DRG']
top10 = readm.loc[conditions]
#days = top10['DAYS']
#top10series = top10.pivot(index='PATIENT ACCOUNT NUMBER', columns='DESCRIPTION OF APR_DRG', values='DAYS')
#top10series.plot(subplots=True, kind = 'kde', layout=(3, 4), figsize=(24, 15), sharey=True)


# In[130]:

top10.DAYS.hist(by=top10['DESCRIPTION OF APR_DRG'], 
                figsize=(24, 24), 
                normed=True,
                sharey=True, 
                bins=15, 
                color='k', 
                alpha=.5)
plt.suptitle('Probability of readmission by day after initial admission (by condition)', size='xx-large')


# In[131]:

pd.crosstab(df.RaceCode, df.cond).plot(kind='barh')


# In[132]:

pd.crosstab(df.cond, df.GenderCode).plot(kind='bar')


# In[133]:

from scipy import stats

dob = pd.to_datetime(eligible.BirthDate, errors='coerce')
#dob = pd.to_datetime(df['BirthDate']
eligible['age'] = (((now - dob).dt.days)/365).round(1)
eligible['agegroup']=pd.cut(eligible['age'],8)
gp = eligible.groupby(['GenderCode', 'agegroup'])
means = gp.mean().unstack(level=0)[:-2]
errors = gp.sem().unstack(level=0)[:-2]


# In[134]:

means['TOTAL PAU COSTS'][['F','M']].plot( kind='line',  
                                         rot=90, 
                                         yerr=errors['TOTAL PAU COSTS'], 
                                         title='Average PAU cost by age group and gender', 
                                         )


# In[135]:

means['SEVERITY LEVEL'][['F','M']].plot( kind='line',  
                                         rot=90, 
                                         yerr=errors['SEVERITY LEVEL'], 
                                         title='Average Severity by age group and gender', 
                                         )


# In[136]:

eligible.groupby(['agegroup','DESCRIPTION OF APR_DRG']).count()['HOSPITAL ID'].unstack(level=-1)[conditions][:-3].plot(kind='area',  title='Top 10 most prevalent conditions by age group', layout=(4, 3), figsize=(25, 18),sharey=True, subplots=True,legend=True)


# In[137]:

eligible.groupby(['GenderCode','DESCRIPTION OF APR_DRG']).count().readm_30day.unstack(-1)[conditions][0:-1].T.plot(title='Readmission counts by gender',kind='bar')


# In[138]:

cost = top10.groupby('DESCRIPTION OF APR_DRG')['TOTAL PAU COSTS'].agg([np.mean, stats.sem]).sort('mean', ascending = False)
m = cost['mean']
err = cost['sem'].values
m.plot( kind='bar',yerr=err,  title='Average PAU cost by condition (top 10 most prevalent)')                                  


# In[139]:

top10.columns


# ## Logistic Regression

# In[141]:

#prep data
df['RaceCode'] = df['RaceCode'].astype(object) #factorize this
df['zip'] = df['zip'].astype(object)#factorize this
df['agegroup'] = df['agegroup'].astype(object)#factorize this
df['EthnicGroupCode'] = df['EthnicGroupCode'].astype(object)
df['MaritalStatusCode'] = df['MaritalStatusCode'].astype(object)
df['VeteranMilitaryStatusCode'] = df['VeteranMilitaryStatusCode'].astype(object)
#df['drgtext'] = df['DESCRIPTION OF APR_DRG']
#df['severity'] = df ['SEVERITY LEVEL']
#df=df.rename(columns = {'SHORT-TERM DIABETES':'DIABETES_ST',
#                                    u'ADULT ASTHMA': u'ADULT_ASTHMA',
#                                     u'ANGINA': u'ANGINA',
#                                     u'BACTERIAL PNEUMONIA': u'BACTERIAL_PNEUMONIA',
#                                     u'CHRONIC OBSTRUCTIVE PULMONARY DISEASE': u'COPD',
#                                     u'CONGESTIVE HEART FAILURE': u'CHF',
#                                     u'DEHYDRATION': u'DEHYDRATION',
#                                     u'HYPERTENSION': u'HYPERTENSION',
#                                     u'LONG-TERM DIABETES': u'DIABETES_LT',
#                                     u'LOWER EXTREMITY AMPUTATION AMONG PATIENTS WITH DIABETES': u'DIABETES_AMPUT',
#                                     u'PERFORATED APPENDIX': u'APPENDIX',
#                                     u'UNCONTROLLED DIABETES': u'DIABETES_NOT_CONTROLLED',
#                                     u'URINARY TRACT INFECTION': u'UTI'
#                                    })


# In[142]:

from sklearn.linear_model import LogisticRegression
from patsy import *
from sklearn.cross_validation import train_test_split
from sklearn import metrics
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import classification_report

#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
#from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis


# In[143]:

df['condnum'] = (df['cond'] == 'target').astype(int)
dta = df

y, X = dmatrices("condnum ~  GenderCode + MaritalStatusCode + RaceCode + EthnicGroupCode + age + InstitutionName",
                  dta, return_type="dataframe")
# flatten y into a 1-D array
y = np.ravel(y)


# ## Classifier comparison

# In[144]:

classifiers = [
    KNeighborsClassifier(3),
    SVC(kernel="linear", C=0.025),
    SVC(gamma=0.1, C=1),
    DecisionTreeClassifier(max_depth=5),
    RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
    AdaBoostClassifier(),
    GaussianNB()]
names = ["Nearest Neighbors", "Linear SVM", "RBF SVM", "Decision Tree",
         "Random Forest", "AdaBoost", "Naive Bayes"]


# In[145]:

#Normalize X
X = StandardScaler().fit_transform(X)


# In[146]:

# iterate over classifiers
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
# for name, clf in zip(names[[2,6]], classifiers[[2,6]]):
#     print "="*10, name, "="*10
#     clf.fit(X_train, y_train)
#     score = clf.score(X_test, y_test)
#     print "Overall accuracy:", score
#     #print metrics.accuracy_score(y_test, predicted)
#     #print metrics.roc_auc_score(y_test, probs[:, 1])
#     predicted = clf.predict(X_test)
#     print classification_report(y_test, predicted, target_names=['control','target'])


# In[ ]:

rbf, bayes = classifiers[2], classifiers[6]
for clf in [rbf,bayes]:
    print clf
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test)
    print "Overall accuracy:", score
    #print metrics.accuracy_score(y_test, predicted)
    #print metrics.roc_auc_score(y_test, probs[:, 1])
    predicted = clf.predict(X_test)
    print classification_report(y_test, predicted, target_names=['control','target'])


# In[ ]:


# instantiate a logistic regression model, and fit with X and y
model = LogisticRegression()
#model =  AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
#                         algorithm="SAMME",
#                         n_estimators=200)
model = model.fit(X, y)

# check the accuracy on the training set
model.score(X, y)


# In[ ]:

# what percentage had readmissions?
y.mean()


# In[ ]:

# examine the coefficients
coefs = pd.DataFrame(zip(X.columns, np.transpose(model.coef_)))


# In[ ]:

# evaluate the model by splitting into train and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
model2 = model
model2.fit(X_train, y_train)


# In[ ]:

# predict class labels for the test set
predicted = model2.predict(X_test)
print predicted


# In[ ]:

# generate class probabilities
probs = model2.predict_proba(X_test)
print probs[0:10]


# In[ ]:

# generate evaluation metrics


# In[ ]:

probs[:, 1][0:10]


# In[ ]:

# Plot all ROC curves
from sklearn.metrics import roc_curve, auc

# Compute ROC curve and ROC area for each class
fpr = dict()
tpr = dict()
roc_auc = dict()

fpr, tpr, thr = roc_curve(y_test, probs[:, 1])
roc_auc = auc(fpr, tpr)

# Compute micro-average ROC curve and ROC area
#fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
#roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])


plt.figure()
plt.plot(fpr, tpr, linewidth=.7, color='k')
plt.title('ROC curve (area = {})'.format(str(roc_auc.round(2))))


# In[ ]:

roc_auc


# In[ ]:

# evaluate the model using 10-fold cross-validation
scores = cross_val_score(LogisticRegression(), X, y, scoring='accuracy', cv=10)
print scores
print scores.mean()


# In[ ]:

#predict probability of readmission for a person
#model.predict_proba(np.array([1. ,   1. ,   0. ,   0. ,   0. ,   0. ,   0. ,   0. ,   1. ,
#         0. ,  43.0]))[0][1]

