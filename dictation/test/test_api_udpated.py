import requests
import urllib
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import nose
import json


class TestAPI(object):
    """
    Run all tests by running
    nosetests ./
    from the test directory

    You must first start the API in dev mode by running
    dictations/api.py -c -d
    from the lunar root folder
    """
    def setUp(self):
        # TODO: Add start of API server on test port on setUp, stop on tearDown
        data_list = [('chiefComplaint', 'chest pain'),
             ('dictations[0][BLOBIN]',
              '   MEDSTAR WASHINGTON HOSPITAL CENTER\n   Department of Radiology\n   crushing acid reflux Ward/Clinic:WHCEMR\n   Patient Name:xxxxxx, xxxxxx MARIE\n   MRN:xxxxxxx Patient Age:xx years\n   Admitting Provider:<Unknown>\n\n\n                                 Diagnostic Imaging\n\n   Accession Number   Exam Date/Time      Procedure          Ordering Doctor\n   DX-xx-xxxxxxx      x/xx/xx xx:xx:xx    DX Chest PA/Lat    Amirshahi MD,\n                      AM                                      MaryAnn E\n\n   CPTx Codes\n   xxxxx (xxxxx),  xxxxx (DX Chest PA/Lat)\n\n\n   Reason for Exam\n   Cough/Fever/Pneumonia\n\n\n   Read\n   EXAM:  PA and lateral views of the chest.\n\n   REASON: Cough\n\n   FINDINGS: The lungs are well expanded and clear.\n   The heart and mediastinum are normal.\n   There is no pleural effusion or pneumothorax.\n   The bones and soft tissues are unremarkable for age.\n\n   IMPRESSION:\n\n   Normal chest\n   Dictating Radiologist: Haveric MD, Namik              xx/xx/xx xx:xx\n   Electronically Signed by: Haveric MD, Namik           xx/xx/xx xx:xx\n                                                        chest pain, sob, acid_reflux, cough, dyspnea xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n   Transcribed by: NH                                    xx/xx/xx xx:xx '),
             ('dictations[0][TEXTLEN]', '781'),
             ('dictations[0][EVENT_TAG]', 'Chest PA and LAT'),
             ('dictations[0][EVENT_START_DT_TM]', '1468833901'),
             ('dictations[0][TYPE]', 'RAD'),
             ('dictations[0][EVENT_ID]', '1234'),
             ('dictations[0][EVENT_CD]', '54335800'),
             ('dictations[0][CATALOG_CD]', 'Radiology'),
             ('[0]_', '1468859992856'),
             ('dictations[1][BLOBIN]',
              '   MEDSTAR WASHINGTON HOSPITAL CENTER\n   Department of Radiology\n  crushing warfarin tightness Ward This patient is on cocaine everyday /Clinic:WHCEMR\n   Patient Name:xxxxxx, xxxxxx MARIE\n   MRN:xxxxxxx Patient Age:xx years\n   Admitting Provider:<Unknown>\n\n\n                                 Diagnostic Imaging\n\n   Accession Number   Exam Date/Time      Procedure          Ordering Doctor\n   DX-xx-xxxxxxx      x/xx/xx xx:xx:xx    DX Chest PA/Lat    Amirshahi MD,\n                      AM                                      MaryAnn E\n\n   CPTx Codes\n   xxxxx (xxxxx),  xxxxx (DX Chest PA/Lat)\n\n\n   Reason for Exam\n   Cough/Fever/Pneumonia\n\n\n   Read\n   EXAM:  PA and lateral views of the chest.\n\n   REASON: Cough\n\n   FINDINGS: The lungs are well expanded and clear.\n   The heart and mediastinum are normal.\n   There is no pleural effusion or pneumothorax.\n   The bones and soft tissues are unremarkable for age.\n\n   IMPRESSION:\n\n   Normal chest\n   Dictating Radiologist: Haveric MD, Namik              xx/xx/xx xx:xx\n   Electronically Signed by: Haveric MD, Namik           xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n   Transcribed by: NH                                    xx/xx/xx xx:xx '),
             ('dictations[1][TEXTLEN]', '781'),
             ('dictations[1][EVENT_TAG]', 'Chest PA and LAT'),
             ('dictations[1][EVENT_START_DT_TM]', '1468833902'),
             ('dictations[1][TYPE]', 'RAD'),
             ('dictations[1][EVENT_ID]', '1235'),
             ('dictations[1][EVENT_CD]', '54335801'),
             ('dictations[1][CATALOG_CD]', 'Radiology'),
             ('[1]_', '146885999285')]
        self.params = urllib.urlencode(data_list)

    def test_dictationlensapi_post(self):
        url = 'http://127.0.0.1:8000/dictationlensapi'
        # TODO: Change to test port once it is set up
        # url = 'https://172.25.234.51:8478/dictationlensapi'
        rpost = requests.post(url, params=self.params, verify=False)
        with open('../fixtures/expected_v2.json') as infile:
            expected_response = json.load(infile)

        expected_dicts = expected_response['result']['dictations'] # dict
        actual_dicts = rpost.json()['result']['dictations']  # dict
        nose.tools.assert_equal(actual_dicts, expected_dicts, msg="Dictations Lense Post Result Did Not Match")

    def test_dictationlens_header(self):
        url = 'http://127.0.0.1:8000/dictationlensapi'
        # TODO: Change to test port once it is set up
        # url = 'https://172.25.234.51:8478/dictationlensapi'
        rpost = requests.post(url, params=self.params, verify=False)
        with open('../fixtures/expected_v2.json') as infile:
            expected_response = json.load(infile)

        expected_header = expected_response['result']['header'] # dict
        actual_header = rpost.json()['result']['header']  # dict

        keys = expected_response['result']['header'].keys()
        keys.remove('oid')
        keys.remove('model_params')
        keys.remove('targetTerms')
        for key in keys:
            nose.tools.assert_equal(actual_header[key], expected_header[key],
                                    msg='Header field %s did not match expected result' %key)

    def test_get_critical_overlap(self):
        nose.tools.assert_almost_equal(0.333333333333333333, 1/3.0)

