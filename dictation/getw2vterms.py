from gensim.models import word2vec
import pandas as pd

model = word2vec.Word2Vec.load('C:\DataExtracts\Dictations\word2vec\w2v.model')
categories = pd.read_excel('C:\DataExtracts\Dictations\word2vec\input_matchcandidates.xlsx')

def get_w2v_terms(list):
    term_list = []
    original_term_list = []
    for item in list:
        try:
            results = model.most_similar(item.lower().replace(" ","_"))
            for index in range(len(results)):
                term_list.append(results[index][0].replace("_"," "))
                original_term_list.append(item)
        except:
            print "No results for %s." % item

    return term_list, original_term_list

if __name__ == '__main__':

    terms = categories.groupby(['chief_complaint'])['candidate_term'].apply(lambda x: x.tolist()).reset_index()
    w2v_term_list = []
    original_term_list = []

    # for index in range(len(terms)):
    for index in range(len(terms)):
        w2v_terms, original_terms = get_w2v_terms(terms['candidate_term'][index])
        w2v_term_list.append(w2v_terms)
        original_term_list.append(original_terms)

    terms['w2v_term'] = w2v_term_list #add new column of w2v terms
    terms['original_term'] = original_term_list

    # #combine w2vterm and candidate term (filter for unique) (NOT SURE IF THIS WORKS FOR ORIGINAL TERMS TOO)
    combined_term_list = []
    original_term_list = []
    for index in range(len(terms)):
        combined_term_list.append(list(set(terms['w2v_term'][index] + terms['candidate_term'][index])))
        original_term_list.append(terms['original_term'][index])


    terms['combined'] = combined_term_list

    # create a new data frame with the correct excel format
    chief_complaint = []
    candidate_term = []
    for index in range(len(terms)):
        for term in terms['combined'][index]:
            chief_complaint.append(terms['chief_complaint'][index])
            candidate_term.append(term)

    df = pd.DataFrame({'chief_complaint':chief_complaint, 'candidate_term': candidate_term})
    df = df[['chief_complaint','candidate_term', 'original_term']] # rearrange column order
    df.to_excel('C:\DataExtracts\Dictations\word2vec\matchcandidates_orig.xlsx', index = False)
    print "Saved to excel file."
    #print to excel
