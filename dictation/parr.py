__author__ = 'sergeyblok'

import pickle, string
import multiprocessing

import string, math, os, sys, inspect, logging, nltk, re, time
from ipyparallel import Client
import multiprocessing as mp
from multiprocessing import Process, Array

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)



logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from negex.negex import *




def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return:
    '''
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]
    sent_tokens =  [[j.strip().lower() for j in sentence.split()] for sentence in sentences if sentence]

    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    return stripped

if __name__ == '__main__':
	#df = load_files('../dictation/data')
	#documents = df['data'][0:100]
	documents = ["This is my doc. Sentence 2.","This is my doc","This is my doc","This is my doc"]
	print "done loading files"
	docs = [filter(lambda x: x in string.printable, d) for d in documents]
	print "removed nonprintables"
	#unigram_features = []
	pool = multiprocessing.Pool(processes=8)
	output = pool.map(tokenize, docs)
	print output
	#with open("tokenizer_output.pickle", 'w') as f:
	#    pickle.dump(output, f)
	print "Done"
	pool.close()
	pool.join()



