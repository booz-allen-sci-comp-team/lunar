#! /usr/bin/env python

"""

This module is the main API response handler for the front-end.
It interacts with the javascript in the front end and routes
REST calls to various other functions, including both general
and app-specific tasks.

"""

import werkzeug
import argparse
from sqlalchemy import engine
from lens import meds, main_annotator, cc_related_terms
from flask import Flask, url_for, request
from flask_cors import CORS, cross_origin
from utils import num
import pandas as pd
from sqlalchemy import create_engine

from flask import jsonify
stored_terms=False

# app = Flask(__name__)
# app = Flask(__name__, static_url_path='c:/dictation-ui-cerner/default')
app = Flask(__name__, static_url_path='/static')


@app.route('/dictationlensapi', methods=['GET', 'POST'])
@cross_origin()
def sample_dictation_lens():
    '''
    Main Dictation Lens Endpoint
    Receives dictations from Cerner
    Returns: an annotation result object as defined in the API documentation
    '''
    if request.method == 'GET':
        return "Hello World!"
    else:
        content = request.values
        if not content:
            return jsonify({"error":"no data"})
        ordered_tuples = [(k, content[k]) for k in sorted(content.keys())]
        dictations_text = [k[1] for k in ordered_tuples if 'BLOBIN' in k[0]]
        chief_complaint = content['chiefComplaint']
        metadata_keys = [k[0] for k in ordered_tuples if 'BLOBIN' not in k[0] and
                         'chiefComplaint' not in k[0]]
        md = werkzeug.MultiDict([[k[0].replace("]","").split("[")[2]]+[k[1]] for k in ordered_tuples
                        if k[0] in metadata_keys and ']_' not in k[0]])
        df = pd.DataFrame([md.getlist(k) for k in md.keys()]).T
        df.columns = md.keys()
        metadata = df.to_dict(orient='records')
        result = main_annotator(chief_complaint, dictations_text, metadata, stored_terms)
    return jsonify(result)



@app.route('/terms', methods=['GET','POST'])
@cross_origin()
def get_terms_for_cc():
    '''
    Standalone term generator interface

    Returns: an annotation result object as defined in the API documentation
    '''
    if request.method == 'GET':
        print request.values.keys()
        query = request.values['cc']
        kwargs = dict([(k[0],num(k[1])) for k in request.values.items() if k[0] !='cc'])
        query = query.replace("_"," ").replace("+"," ").replace("/"," ").lower() #in case URL encoding is used for space
        print "Chief Complaint:", query
        result = cc_related_terms(query, **kwargs)
    else:
        return "Hello World!"
    return jsonify(result)

@app.route('/meds/',methods=['GET', 'POST'])
@cross_origin()
def sample_meds():
    '''

    Returns:

    '''
    if request.method == 'GET':
        if not request.values:
            return jsonify({"error":"no data"})
        result = "Hello World!"
    else:
        content = request.get_json(force=True)
        if not content:
            return jsonify({"error":"no data"})
        dicts = [k['BLOBIN'] for k in content['dictations']['DATA_REC']['DATA_LIST']]
        result = meds(dicts)
    return jsonify(result)

@app.route('/testui', methods=['GET', 'POST'])
@cross_origin()
def testui():
    '''
    https://172.25.234.51:443
    Args:
        cc:

    Returns:

    '''
    if request.method == 'GET':
        return "hello world"
    else:
        content = request.get_json(force=True)
        if not content:
            print "no data"
            return jsonify({"error":"no data"})
        result = content
        print result
        pd.DataFrame.from_dict([result]).to_sql('clicks', engine, if_exists='append')
    return jsonify(result)

def write_to_db(json_object):
    data = json_object.to_dict()
    fields = ['TimeString', 'UserId','ChiefComplaint','Term','UserAction','TermRelevance']

    for field in fields:
        try:
            data[field]
        except KeyError:
            print "Missing %s field. Inserting null." %(field)
            data[field] = 'null'

    query = "INSERT INTO [dbo].[userdata] ([TimeString], [UserId], [ChiefComplaint], [Term], [UserAction], [TermRelevance]) VALUES ('" + data['TimeString'] + "','" + data['UserId'] + "','" + data['ChiefComplaint'] + "','" + data['Term'] + "','" + data['UserAction'] + "','" + data['TermRelevance'] + "');"
    print query

    engine_name = "mssql://pandas:p4nd4s@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server"
    engine = create_engine(engine_name)
    conn = engine.connect()
    print "Connected to database."
    success = conn.execute(query)
    conn.close()

    if success: return True # return True if successful
    else: return False

@app.route('/storejson', methods=['GET','POST'])
@cross_origin()
def storejson():
    '''

    Capture UI events to DB
    Args:
        cc:

    Returns:

    '''
    if request.method == 'GET':
        return "Incorrect request method."
    elif request.method == 'POST':
        content = request.values
        if not content:
            print "no data"
            return jsonify({"error":"no data"})

        if write_to_db(content) == True:
            return jsonify({"response":"success"})
        else:
            return jsonify({"response":"failed to write to db."})



if __name__ == "__main__":
     parser = argparse.ArgumentParser()
     parser.add_argument('-d', '--dev', action='store_true', help='run in DEV mode')
     parser.add_argument('-m', '--model', action='store_true', help='use word2vec model for term generation')
     parser.add_argument('-c', '--curated', action='store_true', help='bypass word2vec model for term generation and use stored term bank')

     args = parser.parse_args()
     if not  args.model and not args.curated:
         print "You must select a term source -m  for word2vec model or -c for hand-curated pre-stored list"
         exit()
     if args.curated:
        stored_terms = True

     context = (r'ServerCertificate.crt', r'server.key')
     port = 443
     if args.dev:
        port = 8000
        context = None
        print "Starting in DEV Mode"
     app.run(host="0.0.0.0", port=port, ssl_context=context, threaded=True, debug=True)

