# -*- coding: UTF-8 -*-
__author__ = 'sergeyblok'

import config
import argparse

import json
import requests
import itertools
import re
from nltk.stem.porter import PorterStemmer
import sys, os, inspect
from gensim.models import Phrases, Word2Vec
import difflib
import config
from xml.etree import ElementTree
import multiprocessing
from datetime import date, timedelta
import random
import datetime
import pickle
porter_stemmer = PorterStemmer()
import pandas as pd



pd.set_option('display.max_colwidth', -1)
diagnosis_types = ['Finding', 'Disease or Syndrome', 'Therapeutic or Preventive Procedure', 'Laboratory or Test Result',\
                'Pathologic Function','Neoplastic Process','Congenital Abnormality',\
                'Acquired Abnormality','Mental or Behavioral Dysfunction','Injury or Poisoning']

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from metamap import metamap_interface
from dictation.utils import tokenize

mm = metamap_interface.MetamapInterface(ade_types=['sosy', 'fndg', 'dsyn', 'mobd', 'patf', 'pdsu', 'phsu','topp'])
if not mm.metamapstr("tylenol"):
    print "======>Warning: MetaMap did not start."
    # sys.exit()
# with open('C:\DataExtracts\chief_complaints\stop_words.txt', 'r') as f:
#     manual_filter = f.readlines()
# stop_phrases = sorted(list(set(manual_filter[0].split(","))))

try:
    print "Loading word to vec model"
    model = Word2Vec.load(config.w2v_model_path)
except:
    print "missing model"

print "Loading Word2Vec UMLS mappings and filtering vocabulary"
try:
    with open(config.w2v_umls_filter_path, 'r') as f:
            mm_result = pickle.load(f)

    filter_by_types=['dsyn', 'mobd', 'patf', 'pdsu','phsu', 'topp']
    filtered_w2v_vocab = []
    for k in mm_result:
         if k[1]:
             for j in k[1].split(","):
                 if j in filter_by_types:
                     filtered_w2v_vocab.append(k[0])
    filtered_w2v_vocab = list(set(filtered_w2v_vocab))
    
    print "After filtering, we have %i vocabulary items" %len(filtered_w2v_vocab)
except:
    print "Failed to load UMLS filter vocabulary"
    filtered_w2v_vocab = []



#with open('C:\DataExtracts\Dictations\word2vec\w2v_mm_map.pickle', 'r') as f:
#            mm_result = pickle.load(f)



#cond_proc = pd.DataFrame.from_csv('cond_proc_save.csv', sep="|")
#rare = cond_proc[(cond_proc.pubmedscore > 100000) & (cond_proc.pubmedscore < 200000) & (cond_proc.cnt < 8)]
#
# try:
#     with open('cond_proc_final.pickle', 'r') as f:
#         cond_proc_final = pickle.load(f)
#         critical_terms = cond_proc_final
#
# except:
#     # limit vocabulary to semantic types procedure, condition
#     with open('mimic_win20.w2v_mm', 'r') as f:
#         vocab_postproc = pickle.load(f)
#     print "done loading vocab"
#     df = pd.DataFrame(vocab_postproc, columns=['phrase', 'type', 'cnt'])
#     cond_proc = df[df.type.isin(['dsyn', 'topp', 'neop'])]
#     critical_terms = list(cond_proc.phrase.values)
#     critical_terms = sorted([c for c in critical_terms if c not in stop_phrases])
#     cond_proc_final = critical_terms
#     with open('cond_proc_final.pickle', 'w') as f:
#         pickle.dump(cond_proc_final, f)
#
# base_ct_backpain = ['back_pain', 'ivda', 'ivdu', 'vertebral', 'osteo', 'myelitis', 'heroin',
#                     'epidural_abscess', 'aortic_dissection', 'marfan', 'aortic_aneurysm']
# critical_terms_back_pain = [c for c in base_ct_backpain if c in critical_terms]
# set([k[0].replace("_", " ") for k in w2v.most_similar(c, [], 5)])
#
#
# spinal_myelitis = ["fever", "back_pain", "radiculopathy","chest_pain","abd_pain",\
#                   "abdominal_pain","paraplegia","epidural_abscess",\
#                   "incontinence","headache","neck_pain","meningitis"]
#
#
# # #print "Loading sample dictations"
# #test_doc_df = load_files('../dictation/data')
# #docs = test_doc_df['data'][11000:11100]
#
# #rare_terms = list(rare.phrase.values)
# #rare_terms = sorted([c for c in rare_terms if c not in stop_phrases])
#
#
# def get_topn_matrix(n, show=False, sample=False):
#     if sample:
#         topn = cond_proc.sample(n).phrase.values
#     else:
#         topn = cond_proc.sort('cnt', ascending=False)[0:n].phrase.values
#     phrases = [k for k in topn if k.replace(" ", "_") in w2v.vocab]
#     mat = []
#     for row in phrases:
#         row = row.replace(" ", "_")
#         for col in phrases:
#             col = col.replace(" ", "_")
#             sim = w2v.similarity(row, col)
#             mat.append([row.replace("_", " "), col.replace("_", " "), sim])
#     df = pd.DataFrame(mat, columns=['a', 'b', 'sim'])
#     df.set_index(['a', 'b'], inplace=True)
#     result = df.unstack()
#     # if show:
#     #     plt.xticks(rotation=90)
#     #     sns.set(font="monospace")
#     #     sns.clustermap(result.corr(), linewidths=.5,
#     #                    figsize=(14, 10))
#     return result



def pubmedscore(phrase):
    '''
    Get Pubmed Score for a phrase
    :param phrase:
    :return:
    '''
    phrase = phrase.replace(" ", "+")
    # url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=%s&field=title" %phrase
    url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=%s" % phrase
    result = int(ElementTree.fromstring(requests.get(url).content)._children[0].text)
    return result


def deviant(condition):
    target = condition
    word2vec_sims = model.most_similar(target, [], 400)
    word2vec_sims = [(w[0].replace("s/p", ""), w[1]) for w in word2vec_sims]
    pubmed_base = [pubmedscore("\"" + k[0] + "\"") for k in word2vec_sims]
    pubmed_assoc = [pubmedscore("\"" + target + "\"+and+\"" + "\"" + k[0] + "\"") for k in word2vec_sims]
    df = pd.DataFrame(word2vec_sims, columns=["phrase", "w2v_sim"])
    df['pubmed'] = pubmed_assoc
    pubmed_base = [pubmedscore("\"" + k[0] + "\"") for k in word2vec_sims]
    df['pubmed_base'] = pubmed_base
    df['pubmed_rate'] = df.pubmed / df.pubmed_base
    df = df.query('pubmed > 0 & pubmed_base>1000')
    df['sim_rank'] = df.w2v_sim.rank()
    df['pubmed_rank'] = df.pubmed.rank()
    df['pubmed_rate_rank'] = df.pubmed_rate.rank()
    print "R: ", df.corr()['sim_rank']['pubmed_rate_rank']
    ax = df.plot(x='pubmed_rate_rank', y='sim_rank', kind='scatter', title=condition)

    for i, txt in enumerate(df.phrase.values):
        ax.annotate(txt.replace("_", " "), (df.pubmed_rate_rank.irow(i), df.sim_rank.irow(i)),
                    xytext=(10, -5),
                    textcoords='offset points',
                    size=12,
                    color='darkslategrey')


# def add_pubmed_scores():
#     pubmedscores = []
#     for i, k in enumerate(cond_proc.phrase.values):
#         pubmedscores.append(pubmedscore(k[0]))  # add PubMed scores
#     cond_proc['pubmedscore'] = pubmedscores
#     cond_proc.to_csv('cond_proc_save.csv', sep='|')
#     print "Done getting PubMed scores"
#
#     critical_terms_df = cond_proc[cond_proc.pubmedscore > 100000]
#     # cond_proc[(cond_proc.cnt > 20) & (cond_proc.cnt < 15000) & (
#     # cond_proc.dist.abs() > .03)]  # cond_proc[cond_proc.dist.abs() > .12].sort('cnt', ascending=False)[0:200]
#
#
l = range(5000)


def demoreport(doc, annotated, snippets, match_terms, all_matches_for_this_patient, score, cc, tokens=None):
    rj = {
        "dictationType": "Gastro",
        "chiefComplaint": cc,
        "dictationText": doc,
        "admitDate": (datetime.datetime.utcnow() - timedelta(days=random.sample(l, 1)[0])).isoformat(),
        "id": "15",
        "oid": "9A51D03A-232F-4C9F-921B-4128124B45F8",
        "keyEvents": None,
        "facility": "Example Center",
        "_version_": 1528814374916980700,
        "tokens": tokens,
        "matchedTerms": match_terms,
        "dictationTextAnnotated": annotated,
        "actualWords": None,
        "actualWordsTokenized": None,
        "spansTrue": snippets,
        "spans": snippets,
        "score": score
    }
    return rj


def add_span(document, class_label, pos):
    newdoc = document[:pos[0]] + '<span class=%s>%s</span>' % (
        "'" + class_label + "'", document[pos[0]:pos[1]]) + document[pos[1]:]
    # print '<span class=%s>%s</span>' % ("'" + class_label + "'", document[pos[0]:pos[1]])
    return newdoc


def metamap(documents, concept):
    mm = metamap_interface.MetamapInterface()
    if not mm.metamapstr("tylenol"):
        print " Metamap error, make sure servers are up."
        exit()
    resultjson = []
    target_concepts = mm.ade(mm.metamapstr(concept.replace("_", " ")))
    for d in documents:
        document_metamap_prcessed = mm.ade(mm.metamapstr(d))
        d_annotated = d
        unigram_features = tokenize(d)
        bigram = Phrases(unigram_features, threshold=1)
        trigram = Phrases(bigram[unigram_features], threshold=1)
        doc_features = set([k[0].replace("_", " ") for k in trigram.vocab.items()])
        common_features = target_concepts.intersection(doc_features)
        snippets = []
        matches = []
        score = 0
        if common_features:
            score = len(common_features)
            for phrase in common_features:
                matches.append(phrase)
                try:
                    first_match = [k for k in re.finditer(re.compile(phrase, re.IGNORECASE), d_annotated)][0]
                    d_annotated = add_span(d_annotated, 'keyword', first_match.span())
                    first_start_pos = first_match.span()[0]
                    snippets.append(d_annotated[first_start_pos - 200:first_start_pos + 200])
                except:
                    print "phrase", first_match, "doesn't appear to be in the document-- make sure the same tokenization process" \
                                                 "is applied at model training and model application."
            resultjson.append(demoreport(d, concept, d_annotated, snippets, matches, score))
    with open(concept + '_w2v.json', 'w') as outfile:
        json.dump(resultjson, outfile)

def query_mm(vocab_entry):
    phrase = vocab_entry[0].replace("_", " ")
    mm_result = mm.metamapstr(phrase)
    if mm_result:
        umls_type = mm_result[0][5].replace("[", "").replace("]", "")
        umls_concept_id = mm_result[0][4]
        umls_concept = mm_result[0][3]
    else:
        umls_type = None
        umls_concept_id = None
        umls_concept = None
    count = vocab_entry[1].count
    print phrase, umls_type, count
    return phrase, umls_type, count

def get_mm_semantic_type(term):
    phrase = term.replace("_", " ")
    mm_result = mm.metamapstr(phrase)
    if mm_result:
        umls_type = mm_result[0][5].replace("[", "").replace("]", "")
    else:
        umls_type = None
    # print phrase, umls_type, count
    return umls_type

def add_mm_data(model):
    pool = multiprocessing.Pool(processes=8)
    vocab_postproc = pool.map(query_mm,
                              model.vocab.iteritems())  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()
    # vocab_postproc = [query_mm(k) for k in model.vocab.iteritems()]
    return vocab_postproc


def add_mm_data2(myterms):
    pool = multiprocessing.Pool(processes=8)
    vocab_postproc = pool.map(query_mm,
                              myterms)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()
    # vocab_postproc = [query_mm(k) for k in model.vocab.iteritems()]
    return vocab_postproc




# # load MetaMap synonyms
# print "Loading UMLS Vocabulary..."
# column_names = ['CUI', 'LAT', 'TS', 'LUI', 'STT', 'SUI', 'ISPREF', 'AUI', 'SAUI', 'SCUI', 'SDUI',
#                 'SAB', 'TTY', 'CODE', 'STR', 'SRL', 'SUPPRESS', 'CVF', 'BLANK']
# mt = pd.DataFrame.from_csv('../metathesaurus/MRCONSO.RRF', header=None, index_col=None, sep='|')
# mt.columns = column_names
# mt = mt[mt.LAT == 'ENG']
# mt.STR = mt.STR.str.lower()
# umls_vocab = set(mt.STR.values)
# #mx = pd.DataFrame.from_csv('../metathesaurus/MRXW_ENG.RRF', header=None, index_col=None, sep='|')
# #mx.columns = ['LAT', 'WD', 'CUI', 'LUI', 'SUI', 'BLANK']
# msty = pd.DataFrame.from_csv('../metathesaurus/MRSTY.RRF', header=None, index_col=None, sep='|')
# msty.columns = ['CUI', 'TUI', 'STN', 'STY', 'ATUI', 'CVF', 'BLANK']
# mr = pd.merge(mt, msty, on='CUI', how='left')
# mr = mr[['CUI', 'STR', 'STY']]
# mr = mr.drop_duplicates()
# mr_dict = mr.groupby('STR').first().STY.to_dict()
# with open('metathesaurus/mr_dict.pickle', 'r') as f:
#    mr_dict = pickle.load(f)
#
# mx = pd.DataFrame.from_csv('../metathesaurus/MRREL.RRF', header=None, index_col=None, sep='|')
# mxcols = ['CUI1',	'AUI1',	'STYPE1',	'REL',	'CUI2',	'AUI2',	'STYPE2',	'RELA',	'RUI',
#           'SRUI',	'SAB',	'SL',	'RG',	'DIR',	'SUPPRESS',	'CVF', 'BLANK']
# mx.columns = mxcols
# mx_narrow = mx[['CUI1','CUI2','RELA']]
# mx_narrow = pd.DataFrame.dropna(mx_narrow)
# merged_df = mx_narrow.merge(mt[['CUI','STR']], left_on = 'CUI1', right_on='CUI')
# merged_df = merged_df.merge(mt[['CUI','STR']], left_on = 'CUI2', right_on='CUI')
# merged_df = merged_df.drop_duplicates()

#load MetaMap synonyms
# print "Loading UMLS Vocabulary..."
# column_names = ['CUI', 'LAT', 'TS', 'LUI', 'STT', 'SUI', 'ISPREF', 'AUI', 'SAUI', 'SCUI', 'SDUI',
#                 'SAB', 'TTY', 'CODE', 'STR', 'SRL', 'SUPPRESS', 'CVF', 'BLANK']
# mt = pd.DataFrame.from_csv('../metathesaurus/MRCONSO.RRF', header=None, index_col=None, sep='|')
# mt.columns = column_names
# mt = mt[mt.LAT == 'ENG']
# mt.STR = mt.STR.str.lower()
# umls_vocab = set(mt.STR.values)
# #mx = pd.DataFrame.from_csv('../metathesaurus/MRXW_ENG.RRF', header=None, index_col=None, sep='|')
# #mx.columns = ['LAT', 'WD', 'CUI', 'LUI', 'SUI', 'BLANK']
# msty = pd.DataFrame.from_csv('../metathesaurus/MRSTY.RRF', header=None, index_col=None, sep='|')
# msty.columns = ['CUI', 'TUI', 'STN', 'STY', 'ATUI', 'CVF', 'BLANK']
# mr = pd.merge(mt, msty, on='CUI', how='left')
# mr = mr[['CUI', 'STR', 'STY']]
# mr = mr.drop_duplicates()
# mr_dict = mr.groupby('STR').first().STY.to_dict()
# #with open('../metathesaurus/mr_dict.pickle', 'r') as f:
# #   mr_dict = pickle.load(f)
#
# mx = pd.DataFrame.from_csv('../metathesaurus/MRREL.RRF', header=None, index_col=None, sep='|')
# mxcols = ['CUI1',	'AUI1',	'STYPE1',	'REL',	'CUI2',	'AUI2',	'STYPE2',	'RELA',	'RUI',
#           'SRUI',	'SAB',	'SL',	'RG',	'DIR',	'SUPPRESS',	'CVF', 'BLANK']
# mx.columns = mxcols
# mx_narrow = mx[['CUI1','CUI2','RELA']]
# mx_narrow = pd.DataFrame.dropna(mx_narrow)
# merged_df = mx_narrow.merge(mt[['CUI','STR']], left_on = 'CUI1', right_on='CUI')
# merged_df = merged_df.merge(mt[['CUI','STR']], left_on = 'CUI2', right_on='CUI')
# merged_df = merged_df.drop_duplicates()
# merged_df_small = merged_df[['STR_x', 'STR_y']]
merged_df_small = pd.DataFrame.from_csv(config.umls_path)

print "Completed UMLS Vocabulary Load"


def simset_umls(query):
    '''
    General similarity Function
    :param query:
    :return: list of similar terms
    '''

    simset = list(set(merged_df_small[merged_df_small.index==query].STR_y.values))
    if not simset:
        try:
            query = difflib.get_close_matches(query, merged_df_small.index.values, cutoff=.70, n=1)[0]
            simset = list(set(merged_df_small[merged_df_small.index == query].STR_y.values))
            return simset
        except:
            return None
    else:
        return simset


def candidate_set(terms, theta):
    '''
    Given multiple possible word2vec vocabulary items, compute centraility of each terms with respect to other terms

    Args:
        term: a valid word2vec vocabulary entry
        theta: a similarity threshold to decide if a link exists

    Returns: int


    '''
    linked_terms =[]
    for i in terms:
        for j in terms:
            if model.similarity(i,j) > theta and i <> j:
                linked_terms.append((i,j))
    candidate_set = list(set([j[0] for j in linked_terms]))
    return candidate_set






def fuzzy_string_match():
    pass



model_terms, term_f = [k for k in model.vocab], [model.vocab[k].count for k in model.vocab]

merged_df_small = pd.DataFrame.from_csv(config.umls_path)
#merged_df_small.loc['acute abdomen']
#merged_df_small=merged_df_small.convert_objects('str')


def preproc(query, method=None):
    query = query.lower()#lower, strip punct
    query = re.sub(r'\(\)\.\,\;\:\/\)','',query)
    #query = porter_stemmer.stem(query)
    return query


def simset_umls(query):
    '''
    General similarity Function
    :param query:
    :return:
    '''
    query = preproc(query)
    try:
        simset = list(set(list(merged_df_small.loc[query].STR_y)))
    except:
        simset = []
    return simset

model_terms, term_f = [k for k in model.vocab], [model.vocab[k].count for k in model.vocab]
cnt_dict = dict(zip(model_terms, term_f))
from collections import defaultdict
umlsvocab = list(set(list(merged_df_small.index)))


def get_closest2(phrase, threshold=.70):
    phrase = preproc(phrase)
    result = sorted([(ph,difflib.SequenceMatcher(lambda x: x == " ",ph,phrase).ratio())
                     for ph in [k for k in set(umlsvocab) if k[0]== phrase[0]]], key=lambda x: x[1], reverse=True)
    filtered = [k[0] for k in result if k[1]>threshold]
    return filtered



def get_closest3(phrase):
    '''
    A combination algorithm that returns a list of literally similar terms to the query. The algorithm combines
    word2vec similarity with UMLS conceptual relatedness.
    Args:
        phrase: query phrase

    Returns: list of phrases

    '''
    print 'working on:  ', phrase
    phrase = preproc(phrase)
    if phrase.replace(" ","_") in model.vocab:
        result = sorted(list(set([k[0].replace("_"," ") for k
                                  in model.most_similar(phrase.replace(" ","_"),[],100) if k[1] > .75])))
        if result:
            return result
    mmdf = pd.DataFrame(mm.metamapstr(phrase))
    if len(mmdf)>0:
        mmdf = mmdf[mmdf[2].astype(float) > 5.0]
        candidate_list = list(mmdf[3].values)
        second_order = [" ".join(list(j)) for j in [item for sublist in
                                                    [[l for l in itertools.combinations(candidate_list, k)]
                                                     for k in range(3)] for item in sublist] if j]
        mmdf2 = pd.DataFrame([item for sublist in  [mm.metamapstr(i) for i in second_order] for item in sublist])
        if len(mmdf2)>0:
            mmdf2 = mmdf2[mmdf2[2].astype(float) > 5.0][3]
            result = list(set(list(([item for sublist in [merged_df_small.loc[k.lower()].STR_y.values for k in mmdf2.values
                                           if k.lower() in merged_df_small.index] for item in sublist]))))
        else:
            result = []
    else:
        result = []
    return sorted(result)

def get_closest4(phrase):
    # type: (object) -> object
    '''


    Returns: list of phrases

    '''
    print 'working on:  ', phrase
    phrase = preproc(phrase)
    if phrase.replace(" ","_") in model.vocab:
        cue_set = [phrase.replace(" ","_")]
        result = sorted(list(set([k[0].replace("_"," ") for k
                                  in model.most_similar(phrase.replace(" ","_"),[],100) if k[1] > .75 and get_freq(k[0]) > 400])))
    ###try splitting the terms
    else:
        cue_set =  [w.replace(" ","_") for w in phrase.split() if w in model.vocab]
        if cue_set:
            result = sorted(list(set([k[0].replace("_"," ") for k
                                  in model.most_similar(cue_set,[],200) if k[1] > .50  and get_freq(k[0])>400])))
        else:
            result = []
    return {'result': sorted(result),'query_phrase':phrase, 'cue_set': cue_set}


def verbatim_to_model_vocab(vt):
    pass

def get_freq(model_vocab_entry):
    """
    Get training corpus frequency for a valid model vocabulary entry
    :param w2_vocab_entry:
    :return:
    """
    try:
        result = cnt_dict[model_vocab_entry]
    except:
        print model_vocab_entry, "not a valid in term in model vcabulary"
        result = None
    return result


def umls_to_w2v(umls_vt_list):
    # type: (object) -> object
    '''
    Map a set of UMLS verbatim strings to a set of close-enough w2v vocab items
    :return:
    '''
    assert (type(umls_vt_list) == list),'Input requires a list of non-zero length'
    assert (len(umls_vt_list) > 0), 'Input requires a list of non-zero length'
    umls_vt_list = [i.replace(" ","_") for i in umls_vt_list]
    #first see if there are any exact matches
    exact = list(set(umls_vt_list).intersection(set(model_terms)))
    if exact:
        most_frequent = sorted([(e,cnt_dict[e]) for e in exact if cnt_dict[e] < 4000], key=lambda x: x[1], reverse=True)[0][0]
        return most_frequent
    else:
        print "Unable to convert translate Chief Complaint to a suitable term in the trained model."
        return None

def to_w2v_vocab(query):
    """
    Translate chief complaint to closest model vocabulary entry
    :param query:
    :return:
    """
    result = umls_to_w2v(simset_umls(query))
    print "Mapped\n", query, "====>>>>",result
    return result

def simset_w2v(query, context_string = None, threshold = .75):
    '''
    w2v syn set
    :param query:
    :return:
    '''
    if context_string:
        query = [query.replace(" ","_").lower(),context_string.replace(" ", "_").lower()]
    else:
        query = query.replace(" ","_").lower()
    # close_m = get_close_matches(query, model.vocab.keys(), cutoff=.80,n=50)
    # cset = candidate_set(close_m, threshold)
    cset = to_w2v_vocab(query)
    mostsim = [k[0] for k in model.most_similar(cset, [], 50) if k[1]>threshold]
    return mostsim



if __name__ == "__main__":
    pass
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-cp', '--cond_proc', action='store_true', help='generate data for conditions/procedures lens')
    # parser.add_argument('-cc', '--chief_complaint', help='generate data for chief complaint')
    # parser.add_argument('-r', '--rare', action='store_true', help='generate data for rare terms')
    # args = parser.parse_args()
    # if args.cond_proc:
    #     critical(docs)
    # elif args.chief_complaint:
    #     chief_complaint(args.chief_complaint.replace(" ", "_"))
    # else:
    #     pass
    #     # print "Generating Data for Demo (", len(docs), "documents)"
    #     # demo_data = []
    #     # demo_data.extend(data_gen(docs, 'abdominal_pain'))
    #     # demo_data.extend(data_gen(docs, 'fever'))
    #     # demo_data.extend(data_gen(docs, 'headache'))
    #     # demo_data.extend(data_gen(docs, chief_complaint=None))
    #     # print "written", len(demo_data), "dictations"
    #     # with open('critical-analysis.json', 'w') as outfile:
    #     #     json.dump(demo_data, outfile)
