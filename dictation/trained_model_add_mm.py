__author__ = 'sergeyblok'
import argparse
from gensim.models import Word2Vec
import pickle
# load model


from semantics import add_mm_data

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model_path', help='model path')
    args = parser.parse_args()
    try:
        print "Loading word to vec model"
        model = Word2Vec.load(args.model_path)
    except:
        print "Couldn't load model"
        exit()
    print "Getting MM types"
    mm_data = add_mm_data(model)
    print "saving model"
    with open(args.model_path + "_mm", 'w') as f:
        pickle.dump(mm_data, f)
    print "done"
