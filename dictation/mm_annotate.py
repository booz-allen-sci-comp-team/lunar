__author__ = 'sergeyblok'
from word2vec_train_mimic import tokenize
import pickle, string
import multiprocessing
from sklearn.datasets import load_files
import time
from metamap import metamap_interface
mm = metamap_interface.MetamapInterface()

def mm_annotate(doc):
    raw = mm.metamapstr(doc)
    non_negated = mm.ade(raw)
    return non_negated

df = load_files('../dictation/data')
documents = df['data'][0:1000]
#documents = ["This is my heart rate doc. Sentence 2.","This is my tonsil doc","This is my headache and ear ache doc","This is my cancer doc"]
print "done loading files"
docs = [filter(lambda x: x in string.printable, d) for d in documents]
print "removed nonprintables"
print time.asctime()
#unigram_features = []
pool = multiprocessing.Pool(processes=4)
output = pool.map(mm_annotate, docs)
#print output
print time.asctime()
with open("mm_output.pickle", 'w') as f:
    pickle.dump(output, f)
print "Done"
pool.close()
pool.join()


with open("mm_output.pickle", 'r') as f:
    mm_results = pickle.load(f)




