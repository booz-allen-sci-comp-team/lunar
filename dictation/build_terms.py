'''
Build a list of related terms based on a chief complaint as well as default terms
'''

import pickle
import re
import time as t

import config
#import semantics


from semantics import get_freq, model, get_closest4, add_mm_data, filtered_w2v_vocab
with open('backpain_ivdu_output.txt', 'r') as f:
    ivduterms = f.readlines()
ivduterms = [k.strip() for k in ivduterms]

freqs = dict([(j,model.vocab[j].count) for j in model.vocab])

default_terms = ["difficult airway","warfarin", "coumadin","cord compression","lupus"]

w2v_default_terms = ['trachea','difficult','intubation','warfarin','cord_compression','lupus']

with open(config.stopwords_path, 'r') as f:
    manual_filter = f.read()#['seems','although','accurate','problem','issue','flare']
    manual_filter = sorted(list(set(manual_filter[0].split(","))))


class termset():
    '''
    Class produces related terms for a chief complaint as well as universal (default) terms
    '''

    def __init__(self, cue_phrase, top_initial=150, top_secondary=5, sim_threshold=.45, max_freq=5000000, min_freq=70,
                 umls_type_filter=True, curated=False):
        self.cue_phrase = cue_phrase
        self.top_initial = top_initial
        self.top_secondary = top_secondary
        self.sim_threshold = sim_threshold
        self.min_freq = min_freq
        self.max_freq = max_freq
        self.umls_type_filter = umls_type_filter

        print "Generating related terms"
        start = t.time()
        if not curated:
            self.related_terms = self.target_list(cue_phrase)
            dt = sorted(list(set([item[0] for sublist in [model.most_similar(w,[],3)
                                               for w in w2v_default_terms]
                                              for item in sublist]+w2v_default_terms)))
        else: # Model is curated so don't need to load word2vec model, related terms come from Excel sheets
            self.related_terms = None
            dt = list(set(w2v_default_terms))

        self.default_terms = sorted([j for j in dt if j.replace("_"," ") in filtered_w2v_vocab])
        self.time_to_task = t.time() - start
        print self.time_to_task

    def get_related_terms(self, chief_compl, dimension=None):
        '''
        Get a single list of related terms with an optional dimension argument
        Args:
            chief_compl: string
            dimension: string

        Returns:

        '''
        cp = [chief_compl.replace(" ","_")]
        if cp[0] not in model.vocab:
            close_terms = get_closest4(chief_compl)
            self.phrase = close_terms['query_phrase'],
            self.cue_set = close_terms['cue_set']
            cp = [k.replace(" ","_") for k in close_terms['result'] if k.replace(" ","_") in model.vocab]
            if not cp:
                print "Chief complaint %s not found in model vocabulary" %cp
                #print "Using default set."
                #cp = w2v_default_terms
                return []
        if dimension:
            dimweight = len(cp)
        else:
            dimweight = 0
        query = cp+[dimension]*dimweight
        init_targets = [k[0] for k in
                        model.most_similar(query, [], self.top_initial) if self.min_freq < freqs[k[0]]
                        and k[1] > self.sim_threshold]
        secondary_targets = [item for sublist in
                             [[i[0] for i in model.most_similar(k, [], self.top_secondary)
                               if self.min_freq < freqs[i[0]]
                               and i[1] > self.sim_threshold] for k in init_targets]
                             for item in sublist]
        combined_targets = sorted(list(set(init_targets+secondary_targets)))
        #noun_phrases = [k[0] for k in [nltk.pos_tag([j.replace("_"," ")])[0] for j in combined_targets]
        #               if 'N' in k[1] or '$' in k[1]]
        combined_targets = [w for w in combined_targets if w not in manual_filter]
        return combined_targets

    def target_list(self, cp):
        '''
        Main function to generate related terms across several dimensions and combine results
        Args:
            cp: str

        Returns: dict

        '''
        result = sorted(list(set(self.get_related_terms(cp, dimension='diagnosis')+
                  self.get_related_terms(cp,dimension='procedure')+
                  self.get_related_terms(cp,dimension='surgery'))))
        #filter leading "/" chars
        result = sorted([re.sub('^/','',k) for k in result])
        result = sorted([j[1] for j in list(set([(cp,k,get_freq(k.replace(" ","_"))) for k in result if get_freq(k.replace(" ","_"))<self.max_freq and get_freq(k.replace(" ","_"))> self.min_freq]))])
        #filter only to procedure and condition UMLS types
        if self.umls_type_filter:
            print "Applying UMLS filter"
            result = sorted([j.replace("_"," ") for j in result if j.replace("_"," ") in filtered_w2v_vocab])
        print len(result), 'terms for chief complaint', cp
        return {'result':result,
                'phrase':get_closest4(cp)['query_phrase'],
                'cue_set':get_closest4(cp)['cue_set']}

if __name__ == "__main__":
    mm_result = add_mm_data(model)
    with open(config.w2v_umls_filter_path, 'w') as f:
        pickle.dump(mm_result, f)

