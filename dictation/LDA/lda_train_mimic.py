#Julia Hu
#old LDA model using TFIDF Vectorizer

import string
import os
import sys
import inspect
import logging
import nltk
from sklearn.datasets import load_files
import gensim
import multiprocessing
from gensim import corpora, models, similarities
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
import numpy as np 
from gensim import matutils
from gensim.models.ldamodel import LdaModel
from sklearn import linear_model
import nltk
import string
import os, sys, inspect
import re

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from negex.negex import *

def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return:
    '''
    document = filter(lambda x: x in string.printable, document)
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]
    sent_tokens = [[j.strip().lower() for j in sentence.split()] for sentence in sentences if sentence]

    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    return ' '.join(str(r) for v in stripped for r in v)
    #return stripped

def print_topics(lda, vocab, n=10):
    """ Print the top words for each topic. """
    topics = lda.show_topics(topics=-1, topn=n, formatted=False)
    for ti, topic in enumerate(topics):
        print 'topic %d: %s' % (ti, ' '.join('%s/%.2f' % (t[1], t[0]) for t in topic))


def fit_lda(X, vocab, num_topics=5, passes=20):
    """ Fit LDA from a scipy CSR matrix (X). """
    print 'fitting lda...'

    dictionary = dict([(i, s) for i, s in enumerate(vocab)])
    corpus = matutils.Sparse2Corpus(X)
    #dictionary = gensim.corpora.dictionary.Dictionary(vocab)

    return LdaModel(corpus, num_topics=num_topics,
                    passes=passes,
                    id2word=dictionary), dictionary, corpus

if __name__ == '__main__':
    print "Training LDA model on MIMIC data"
    df = load_files('./sampledata')
    print "Loaded MIMIC data"

    docs = df['data']
    print "Tokenizing corpus"
    features = []
    pool = multiprocessing.Pool(processes=8)
    features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()
    print features

    # create tfidf vectorization model
    tfidf = TfidfVectorizer(min_df=1) #if we add tokenizer, it won't work because list object is unhashable
    vec = tfidf.fit_transform(features)
    vocab = tfidf.get_feature_names()

    #fit lda topic model to tfidf 
    lda, dictionary, corpus = fit_lda(vec, vocab)

    lda.save('ldaModel')

    ldamodel = LdaModel.load('ldaModel', mmap='r')

    #print_topics(lda, vocab)




