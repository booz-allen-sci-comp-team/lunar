#Julia Hu, 
# Generates LDA model and visualization html

import string
import os
import sys
import inspect
import logging
import nltk
from sklearn.datasets import load_files
import gensim
import multiprocessing
from gensim import corpora, models, similarities
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
import numpy as np 
from gensim import matutils
from gensim.models.ldamodel import LdaModel
from sklearn import linear_model
import nltk
import string
import os, sys, inspect
import re
import pyLDAvis.gensim as gensimvis
import pyLDAvis
from gensim.models import Phrases

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from negex.negex import *

def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return:
    '''
    document = filter(lambda x: x in string.printable, document)
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]

    sent_tokens = [(j.strip().lower() for j in sentence.split()) for sentence in sentences if sentence]
    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    return stripped

if __name__ == '__main__':
    print "Training LDA model on MIMIC data"
    df = load_files('./data')
    print "Loaded MIMIC data"
    docs = df['data']
    print "Tokenizing corpus"
    pool = multiprocessing.Pool(processes=8)
    unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()

    features = []
    for l1 in unigram_features:
        for l2 in l1:
            if l2 != []:
                features.append(l2)

    print "Done with unigrams"
    unigram = [item for sublist in unigram_features for item in sublist]
    #print 'unigram', unigram
    bigram = Phrases(unigram, threshold=5)
    #print 'bigram',list(bigram[unigram])
    trigram = Phrases(bigram[unigram], threshold=5)
    #print 'trigram', list(trigram[bigram[unigram]])
    print "Done with bigrams & trigrams. Training model now."

    features = list(trigram[bigram[unigram]])
    dictionary = corpora.Dictionary(features)
    #print dictionary.items()
    #print (dictionary.token2id)
    corpus = [dictionary.doc2bow(feature) for feature in features]
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    model = LdaModel(corpus_tfidf, id2word=dictionary, num_topics=10)
    model.save('lda_tfidf_ngrams_10_model')
    #ldamodel = LdaModel.load('ldaModel', mmap='r')
    #ldamodel.print_topic(1,topn=30)

    vis_data = gensimvis.prepare(model, corpus_tfidf, dictionary)
    visualization = pyLDAvis.save_html(vis_data,'vis_lda_tfidf_ngrams_10.html')
    #print_topics(lda, vocab)





