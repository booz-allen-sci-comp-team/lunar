#Julia Hu, 
# Generates LDA model and visualization html

import string
import os
import sys
import inspect
import logging
import nltk
from sklearn.datasets import load_files
import gensim
import multiprocessing
from gensim import corpora, models, similarities
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
import numpy as np 
from gensim import matutils
from gensim.models.ldamodel import LdaModel
from sklearn import linear_model
import nltk
import string
import os, sys, inspect
import re
import pyLDAvis.gensim as gensimvis
import pyLDAvis
from gensim.models import Phrases

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from negex.negex import *

if __name__ == '__main__':
    print "Training LDA model on MIMIC data"
    df = load_files('./data')
    print "Loaded MIMIC data"
    docs = df['data']

    ldamodel = LdaModel.load('lda_tfidf_ngrams_10_model', mmap='r')
    print ldamodel.log_perplexity(docs)