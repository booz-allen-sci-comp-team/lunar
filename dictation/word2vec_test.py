from gensim import models
import pandas as pd
import numpy as np
model = models.Word2Vec.load('dictations2.w2v')
df=pd.read_csv('ernest_bigrams.csv')
complaints = df.Complaint.unique()
complaints[5] = 'shortness_breath'
probes = ["_".join(k.split(' ')) for k in complaints]
results = [[k[0] for k in model.most_similar([j],[],50)] for j in probes if j != 'hip_pain']
df_w2v = pd.DataFrame(np.array(results).T, columns = [j for j in probes if j != 'hip_pain'])
df_w2v.to_html('word2vec_init_result.html')
