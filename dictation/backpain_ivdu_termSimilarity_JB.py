__author__ = 'jessicabonnie'

import gensim

#Load Model
model=gensim.models.Word2Vec.load("C:\DataExtracts\Dictations\word2vec\w2v.model")

# Experiments
model.most_similar(positive=["backache","radiating","spine"],topn=30)

model.most_similar(positive=["hematogenous","backache"],topn=30)

model.most_similar(positive=["backache","iv","drug_abuse", "radiating"],topn=30)

model.most_similar(positive=["drug_abuse","iv"],topn=30)


#Just insteresting. Not relevant to use case

model.most_similar(positive=["homeless"],topn=30)
model.most_similar(positive=["sexual_assault"],topn=30)

# Read termlist

with open("back_ivdu_termlist.txt") as f:
    termlist=f.readlines()
termlist = [x.strip('\n') for x in termlist]
termlist = [x.strip('-') for x in termlist]
outdict={}

# create hash of most_similar results for each term and phrase
for t in termlist:
    subterms = [t]
    badterms = []
    print subterms
    if len(t.split("_")) > 1:
        subterms.extend(t.split("_"))
    for s in subterms:
        try:
            outdict[s] = model.most_similar(positive=s,topn=10)
        except KeyError:
            if len(subterms) > 1:
                #subterms[:] = [thing for thing in subterms if thing != s]
                badterms.append(s)
            else:
                break
    newwords = [x for x in subterms if x not in badterms]
    try:
        outdict["/".join(newwords)] = model.most_similar(positive=newwords,topn=10)
    except KeyError:
        pass

# if we want a simpler dictionary without associated similarity values

cleandict =  {k: [x for (x,y) in outdict[k]] for k in outdict.viewkeys()}


# create unique similar term list

keys = outdict.keys()
outset = set(keys)
for key in keys:
    outset.union([x for (x,y) in outdict[key]])

# if there's a term in this set that we are interested in, use this to find where it came from:

[k for k, v in cleandict.iteritems() if  "diskitis" in v]
#Note in the following example, "motor_function" is found on more than one similarity list
[k for k, v in cleandict.iteritems() if  "motor_function" in v]
#outdict.keys()[outdict.values().index("pain")]

outfile="backpain_ivdu_output.txt"
#with open(outfile,'w') as f:
#    for item in list(outset):
#        f.write("%s\n" % item)

