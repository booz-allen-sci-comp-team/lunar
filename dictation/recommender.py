"""
Given a CC and dictation note produce a set of recommended items in patient's history
"""
from dictation.sectionparse import *
import pandas as pd
import getpass, pymssql
from dictation.semantics import mm
from sqlalchemy import create_engine
from sectionparse import parse

def section_text(doc):
    try:
        parsed = parse2(doc)
        result = "".join([parsed[k] for k in parsed.keys() if k in target_keys]).replace("\n","")
    except:
        print "couldnt section parse"
        parsed = None
    return result

def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")
    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn


query = """select top 10 AttendingMDNumber,Result from
(select AdmitDateTime, Race, Sex, DateOfBirth, b.AttendingMDNumber, b.EID, Complaint from
(SELECT AttendingMDNumber,count(AttendingMDNumber) N /*,count(distinct EID) mdn_cnt*/
  FROM [azADT].[dbo].[v_VST601] (nolock)
   where AttendingMDNumber<> ''
  GROUP BY AttendingMDNumber
HAVING count(AttendingMDNumber) > 100 ) as a
inner join
[azADT].[dbo].[v_VST601] (nolock) b
on a.AttendingMDNumber = b.AttendingMDNumber
where b.Complaint in ('chest pain','cp', 'sob','shortness of breath', 'chest discomfort', 'difficulty breathing', 'chest paints', 'cp sob', 'resp distress', 'c/p', 'resp. distress')
  and b.AdmitDateTime > '2008-01-01 00:00:00.000'
  and b.AdmitHospitalService = 'MED') c
inner join
[azDIC].[dbo].[v_OBX_TXT] (nolock) d
on c.EID = d.EID
  WHERE TBL = 603
  and UniversalServiceIdentifier = 'HP'"""
connection = mssql_connect(server='10.130.172.93', database='azDIC')
print "Reading from DB and writing CSV."
df = pd.read_sql_query(query, connection)
print "Found table of shape", df.shape
target_keys = [u'Family History:', u'Social History:',u'Past Medical History:',u'History of Present Illness:']

df['section_text'] = df.Result.apply(section_text)
df2 = df[['AttendingMDNumber','section_text']]
result_df = []
for i in range(len(df2)):
    mmi = mm.metamapstr(df2.section_text[i])
    temp1 = mm.ade(mmi, simplified=True)
    temp1['clin_id'] = df2.AttendingMDNumber[i]
    if len(result_df)==0:
        result_df = temp1
    else:
        result_df = result_df.append(temp1, ignore_index=True)



if __name__ == "__main__":
    pass


