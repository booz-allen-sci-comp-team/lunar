#Julia Hu

import os
import glob
import pydeid
from pydeid import dob, address, mrn
import pandas as pd
from collections import namedtuple
from sklearn.datasets import load_files


if __name__ == "__main__":
    annotation_folder = './annotations/'
    #annotation_folder = 'c:/DataExtracts/annotations/'

    if not os.path.exists(annotation_folder):
        os.makedirs(annotation_folder)

    #load files 
    files = load_files('./sampledata')
    filenames = files['filenames']

    df = pd.DataFrame(files['data'])
    df['key'] = df.index
    df['data'] = df[0]

    #pydeid.date.date(df)
    pydeid.dob.dob(df, annotation_folder + 'dob.txt')
    pydeid.age.age(df, annotation_folder + 'age.txt')
    pydeid.name.name(df, annotation_folder + 'name.txt')
    pydeid.mrn.mrn(df, annotation_folder + 'mrn.txt') # MRN needs to go ahead of telephone because there may be overlap with the MRNs
    pydeid.email.email(df, annotation_folder + 'email.txt')
    pydeid.ssn.ssn(df, annotation_folder + 'ssn.txt')
    # pydeid.address.address(df)
    pydeid.telephone.telephone(df, annotation_folder + 'telephone.txt') #do this after dates

    foldername = './deidentified/'
    filename = './deidentified/'

    if not os.path.exists(foldername):
        os.makedirs(foldername)

    for index, row in df.iterrows():
        deid_file = filename + filenames[index][24:]
        f = open(deid_file, 'w+')
        f.write(row['data'])
        f.close()
    
    print "Deidentified Files."
