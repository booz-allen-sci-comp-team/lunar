#Julia Hu

import os, string, sys, inspect
import glob
import pydeid
from pydeid import dob, address, mrn
import pandas as pd
from collections import namedtuple
import getpass
import pymssql
import argparse
from sqlalchemy import create_engine
import re
import numpy as np

def load_sample_reports(engine):
    #df = pd.read_sql_table('sampledicts', engine)
    df = pd.read_sql("select top 100 [key], [data] from dbo.deidentified", engine)
    return df

def count_regex(doc_index, document, count_df):
    reg_ex = ['\[dob\]','\[age\]','\[name\]','\[mrn\]','\[email\]','\[ssn\]','\[phone\]']
    identifiers = ['dob','age','name','mrn','email','ssn','phone']

    for index in range(len(identifiers)):
        count_df[identifiers[index]][doc_index] = len(re.findall(reg_ex[index], document))


if __name__ == "__main__":

    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    engine_name = "mssql://" + user + ":" + pw + "@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server"
    engine = create_engine(engine_name)
    print "Connected to database."
    df = load_sample_reports(engine)

    identifiers = ['dob','age','name','mrn','email','ssn','phone']
    documents = df.key.tolist()

    count_df = pd.DataFrame(index = documents, columns = identifiers)

    for doc_index, row in df.iterrows():
        count_regex(row['key'], row['data'], count_df)

    counts_file = 'c:/DataExtracts/Deidentification/identifier_counts_100_docs.csv'
    count_df.to_csv(counts_file, if_exists='replace')

    print "\nCounts of identifiers per document:"
    print count_df
