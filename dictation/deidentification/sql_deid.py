# Julia Hu
# Connect to sql db, deidentify records, write deidentified column

import os, string, sys, inspect
import glob
import pydeid
from pydeid import dob, address, mrn
import pandas as pd
from collections import namedtuple
import getpass
import pymssql
import argparse
from sqlalchemy import create_engine

def load_sample_reports(engine):
    #df = pd.read_sql_table('sampledicts', engine)
    df = pd.read_sql("select top 100 * from dbo.sampledicts", engine)
    #result = list(df.result.values)
    return df

if __name__ == "__main__":

    # annotation_folder = 'c:/DataExtracts/Deidentification/annotations/'
    # if not os.path.exists(annotation_folder):
    #     os.makedirs(annotation_folder)

    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    engine_name = "mssql://" + user + ":" + pw + "@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server"
    engine = create_engine(engine_name)
    print "Connected to database."
    #sample_docs = load_sample_reports(engine)
    #print sample_docs
    df = load_sample_reports(engine)
    df['key'] = df.index
    df['data'] = df['result']

    pydeid.dob.dob(df)
    pydeid.age.age(df)
    #name used to go here
    pydeid.mrn.mrn(df)  # MRN needs to go ahead of telephone b/c overlap
    pydeid.email.email(df)
    pydeid.ssn.ssn(df)
    pydeid.phone.phone(df)  # do this after dates
    pydeid.name.name(df)

    df.to_sql('deidentified', engine, if_exists='replace')