# -*- coding: utf-8 -*-
#Author: Julia Hu
#Purpose: De-identifies MRNs

import re

def mrn(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [
                    #MRN: 111-11-11, MRN: 11-11-11, MRN: 111-11-1111
                    r'(?:Medical Record No|MED REC NO.|MED REC NO.|MEDICAL RECORD NO.|[Mm][Rr][Nn]|[Mm]edical [Rr]ecord \#|MR \#|Med Rec#)\s{0,7}:?\s{0,7}(\d{2,3}(?:\-)\d{2}(?:\-)\d{2,4})',
                    #MRN: 1111-111, MRN: 1111111
                    r'(?:Medical Record No|MED REC NO.| MED REC NO.|MEDICAL RECORD NO.|[Mm][Rr][Nn]|[Mm]edical[Rr]ecord \#|MR \#|Med Rec#)\s{0,7}:?\s{0,7}(\d{4}-?\d{3,5})',
                  ]

    # For each row in the data
    for index, row in data.iterrows():
           
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)
            for m in re.finditer(pattern, row[deidfield]):
                data['data'][index] = data['data'][index].replace(m.group(1),"[mrn]")

if __name__ == "__main__":
    mrn()