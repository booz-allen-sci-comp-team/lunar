#Author: Julia Hu
#Purpose: De-identifies emails

import re

def match_text_pattern(data, pattern, rule_name, text, row_id):
    '''
    Match text with pattern
    '''
    for m in re.finditer(pattern, text):
        try:
            detected_fields = m.groupdict()
            text = detected_fields['email']
        except KeyError:
            text = m.group(0)
        data['data'][row_id] = data['data'][row_id].replace(m.group(0),"[email]")

def email(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [
                  r'([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)'
                  ]

    # For each row in the data
    for index, row in data.iterrows():
            
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)
            match_text_pattern(data, pattern, rule_name, row[deidfield], row[uniquekey])

if __name__ == "__main__":
    email()