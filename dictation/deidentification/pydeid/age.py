#Author: Julia Hu
#Purpose: De-identifies ages

import re

def match_text_pattern(data, pattern, rule_name, text, row_id):
    '''
    Match text with pattern
    '''
    for m in re.finditer(pattern, text):
        try:
            detected_fields = m.groupdict()
            text = detected_fields['age']
            start = m.start('age')
            end = m.end('age')
        except KeyError:
            text = m.group(0)
            start = m.start()
            end = m.end()
        data['data'][row_id] = data['data'][row_id].replace(m.group(0),"[age]")

def age(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    fn = filename for annotation file. 
    """

    deidfield = 'data'
    uniquekey = 'key'

    age_digits_noname = '([1-9]?\d)'
    age_digits = '(?P<age>[1-9]?\d)'
    month_digits = '(0?[1-9]|10|11|12)'
    month_digits_frac = '(0?\d|10|11|12)(\.\d)?'
    age_suffix = '(y|yo|y. ?o.|y ?/ ?o|yo ?m|yo ?f|ym|yf)'
    ordinal = '(st|nd|rd|th)'
    age_ordinal = '{0}[ \-]? ? ?{1}'.format(age_digits_noname, ordinal)
    
    patternlist = [
    			r'(?:AGE|[Aa]ge)(?:\s{0,5}\:\s{0,5})(\d{1,3})', #AGE: 70
                r'(?i)\baged? {0}\b'.format(age_digits), # age 73
		        r'(?:[Aa]ge)\s{0,1}\-\s{0,1}(\d{1,3})', #age-72
                r'(?i)\bage of {0}\b'.format(age_digits), # age of 43
                r'(?i)\b{0} years? of age\b'.format(age_digits), # 35 years of age
                r'\b(\d{1,3}( |)yo)\b', #30yo
                r'\d{1,3}( |)y\.o\.', #30y.o.
                r'\b\d{1,3}( |)y\/o\b', #30y/o
                r'\b\d{1,3}( |)y\b', #30y
                r'(?i)\b{0}[ \-]?(year|yr)s?[ \-]old\b'.format(age_digits, age_suffix), # 18-year-old, 18 years old, 18 year old, 18yr-old
                r'(?i)\b{0}y{1}m\b'.format(age_digits_noname, month_digits_frac), # 12y3.5m
                r'(?i)\b(\d0 ? ? ?\'s)\b', # y0's    # MIGHT BE CONFUSED WITH DATE
                r'(?i)\b(\d0 ? ? ?s)\b', # y0s    # MIGHT BE CONFUSED WITH DATE
                  ]

    # For each row in the data
    for index, row in data.iterrows():
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)
            match_text_pattern(data, pattern, rule_name, row[deidfield], row[uniquekey])

if __name__ == "__main__":
    age()