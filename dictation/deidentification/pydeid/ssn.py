#Author: Julia Hu
#Purpose: De-identifies SSNs

import re

def ssn(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [r'\b(?:[Ss]SN:|ssn:)\s{0,5}(\d{3}-?\d{2}-?\d{4})\b'] #SSN: XXX-XX-XXXX
        
    # For each row in the data
    for index, row in data.iterrows():

        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)

            for m in re.finditer(pattern, row[deidfield]):

                data['data'][index] = data['data'][index].replace(m.group(1),"[ssn]")

if __name__ == "__main__":
    ssn()