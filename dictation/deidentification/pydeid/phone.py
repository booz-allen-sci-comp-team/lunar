#Author: Julia Hu
#Purpose: De-identifies phone numbers

import re
import datetime

def valid_seven_digit_date(datestring):
    '''
    Determine if a 6 to 8 digit number is a valid date of format mmddyyyy
    :param datestring:
    :return:
    '''
    #remove punctuation
    datestring = datestring.strip(',.? \n')

    try:
        datetime.datetime.strptime(datestring, '%m%d%Y')
        return True
    except ValueError:
        return False

def phone(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [
                    r'(\(?\d{3}\)?(?: |\-)\d{3}(?: |\-)\d{4}\)?)', #10 digit phone number
                    r'(\(?\d{3}(?: |\-)\d{4}\)?)', #7 digit phone number
                    ]

    # For each row in the data
    for index, row in data.iterrows():
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)
            for m in re.finditer(pattern, row[deidfield]):
                if valid_seven_digit_date(m.group(0)) == False:
                    data['data'][index] = data['data'][index].replace(m.group(0), "[phone]")

if __name__ == "__main__":
    phone()