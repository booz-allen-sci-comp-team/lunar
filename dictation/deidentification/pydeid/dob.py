#Author: Julia Hu
#Purpose: De-identifies dob

import re

def dob(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [
        #DOB: 01-01-2011
        r'(?:DATE OF BIRTH|[Dd]\.?[Oo]\.?[Bb]\.?|Date of Birth)\s{0,7}(?:\:)\s{0,7}(\d{2}(?:-|\/| ?)\d{2}(?:-|\/| ?)\d{4})',
        #DOB: 01-01-11
        r'(?:DATE OF BIRTH|[Dd]\.?[Oo]\.?[Bb]\.?|Date of Birth)\s{0,7}(?:\:)\s{0,7}(\d{2}(?:-|\/| ?)\d{2}(?:-|\/| ?)\d{2})',
        #DOB: Jan 1, 2011
        r'\b(?:DATE OF BIRTH|[Dd]\.?[Oo]\.?[Bb]\.?|Date of Birth)\s{0,7}(?:\:)\s{0,7}((Jan|Feb|Mar|Apr|May|\
        Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August\
        |September|October|November|December)?\.? \d{1,2}?\,? \d{4}\b)',
        ]

    # For each row in the data
    for index, row in data.iterrows():
           
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)

            for m in re.finditer(pattern, row[deidfield]):
                data['data'][index] = data['data'][index].replace(m.group(1), "[dob]")

if __name__ == "__main__":
    dob()