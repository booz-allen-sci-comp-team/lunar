﻿# -*- coding: utf-8 -*-
# Author: Julia Hu
# Purpose: De-identifies doctor's names and patient's names

import re

def valid_word(string):
    string = string.strip('.')
    prohibited_list = ['and', 'TEMPORARY', 'PHYSICIAN', 'Temporary', 'Physician', 'MD', 'DO','PA','NP', 'PAC']

    #check if string is a letter
    if len(string) == 1:
        return False

    #Determine if the string is on the list of "do not replace" words
    for index in range(len(prohibited_list)):
        if string == prohibited_list[index]:
            return False

    return True

def name(data):
    """
    Apply deid function to a column of a Pandas DataFrame.

    Arguments:
    data = Pandas DataFrame.
    """

    deidfield = 'data'
    uniquekey = 'key'

    patternlist = [
        # 4 words: John Matthew Stevens Doe, MD or Doe, John Matthew Stevens, MD
        r'([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )(?:M\.B\.CH\.B|C\.R\.N\.P|CRNP|P\.A\.\-C\.|PAC|M\.D\.|P\.A\.|D\.O\.|MD|PA|DO )',
        # 3 words: Doe, John M. M.D., John M. Doe, M.D.
        r'([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )(?:M\.B\.CH\.B|C\.R\.N\.P|CRNP|P\.A\.\-C\.|PAC|M\.D\.|P\.A\.|D\.O\.|MD|PA|DO )',
        # 2 words: Doe, John MD, John Doe, MD
        r'([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )(?:M\.B\.CH\.B|C\.R\.N\.P|CRNP|P\.A\.\-C\.|PAC|M\.D\.|P\.A\.|D\.O\.|MD|PA|DO )',
        # 4 words: Name: John Matthew Stevens Doe, Name: Doe, John Matthew Stevens
        r'(?:[Nn]ame|NAME|[Pp]atient|PATIENT|Pt Name)\s{0,3}\:\s{0,3}([A-Z][\-\'\.A-z]+)(?:\, | )([A-Z][\-\'\.A-z]+)(?:\, | )([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)',
        # 3 words: Name: John Matthew Doe, Name: Doe, John Matthew
        r'(?:[Nn]ame|NAME|[Pp]atient|PATIENT|Pt Name)\s{0,3}\:\s{0,3}([A-Z][\-\'\.A-z]+)(?:\, | )([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)',
        # 2 words: Name: John Doe, Name: Doe, John
        r'(?:[Nn]ame|NAME|[Pp]atient|PATIENT|Pt Name)\s{0,3}\:\s{0,3}([A-Z][\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)',
        # 2 words: Dr. John Doe
        r'(?:[Dd]r|Ms|Miss|Mr|Mrs)\.?\s{1,2}([A-Z][\-\'A-z]+)\s{0,2}([A-Z][\-\'A-z]+)',
        # 1 word: Dr. Doe
        r'(?:[Dd]r|Ms|Miss|Mr|Mrs)\.?\s{1,2}([A-Z][\-\'A-z]+)',
        # 4 words: Dictated by: John Matthew Stevens Doe
        r'(?:cc|ReviewedAUTHEnticated by|Electronically signed by|Dictated By|DICTATED BY|ATTENDING PHYSICIAN|Surgeon|Referring\(s\))\s{0,3}\:?\s{0,3}([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)',
        # 3 words: Dictated by: John Matthew Doe
        r'(?:cc|ReviewedAUTHEnticated by|Electronically signed by|Dictated By|DICTATED BY|ATTENDING PHYSICIAN|Surgeon|Referring\(s\))\s{0,3}\:?\s{0,3}([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)',
        # 2 words: Dictated by: John Doe
        r'(?:cc|ReviewedAUTHEnticated by|Electronically signed by|Dictated By|DICTATED BY|ATTENDING PHYSICIAN|Surgeon|Referring\(s\))\s{0,3}\:?\s{0,3}([\-\'\.A-z]+)(?:\, | )([\-\'\.A-z]+)'
    ]

    # For each row in the data
    for index, row in data.iterrows():
        # Look for a pattern and write a new line for each annotation
        for pattern_number, pattern in enumerate(patternlist):
            rule_name = 'regex{0:02d}'.format(pattern_number)

            for m in re.finditer(pattern, row[deidfield]):
                # 4 words
                if pattern_number == 3 or pattern_number == 8:
                    if valid_word(m.group(1)):
                        data['data'][index] = data['data'][index].replace(m.group(1), "[name]")
                    if valid_word(m.group(2)):
                        data['data'][index] = data['data'][index].replace(m.group(2), "[name]")
                    if valid_word(m.group(3)):
                        data['data'][index] = data['data'][index].replace(m.group(3), "[name]")
                    if valid_word(m.group(4)):
                        data['data'][index] = data['data'][index].replace(m.group(4), "[name]")
                # 3 words
                if pattern_number == 4 or pattern_number == 9:
                    if valid_word(m.group(1)):
                        data['data'][index] = data['data'][index].replace(m.group(1), "[name]")
                    if valid_word(m.group(2)):
                        data['data'][index] = data['data'][index].replace(m.group(2), "[name]")
                    if valid_word(m.group(3)):
                        data['data'][index] = data['data'][index].replace(m.group(3), "[name]")

                # 2 words Name: John Doe, Name: Doe, John
                elif pattern_number == 5 or pattern_number == 10:
                    if valid_word(m.group(1)):
                        data['data'][index] = data['data'][index].replace(m.group(1), "[name]")
                    if valid_word(m.group(2)):
                        data['data'][index] = data['data'][index].replace(m.group(2), "[name]")

                else:
                    data['data'][index] = data['data'][index].replace(m.group(0), "[name]")

if __name__ == "__main__":
    name()