# Julia Hu
# Parameters: document as a string
# Returns a de-identified string

import pydeid
import pandas as pd

def deid(document):

    documents = [document]
    df = pd.DataFrame(data=documents)
    df['key'] = df.index
    df['data'] = df[0]

    pydeid.dob.dob(df)
    pydeid.age.age(df)
    pydeid.mrn.mrn(df)  # MRN needs to go ahead of telephone b/c overlap
    pydeid.email.email(df)
    pydeid.ssn.ssn(df)
    pydeid.phone.phone(df)  # do this after dates
    pydeid.name.name(df)
    pydeid.identifier.identifier(df) #captures any other identifiers

    return df['data'][0]

if __name__ == "__main__":
    deid()

