"""
Explores ways to move between verbatim and w2v vocabulary terms
"""
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


import pandas as pd
from build_terms import termset
from dictation.semantics import to_w2v_vocab
#load sample chief complaints to test similarity metric
#df = pd.read_excel('C:\DataExtracts\chief_complaints\\top_500_complaints.xlsx')
#rget = requests.get('https://127.0.0.1:8000/terms',
#                      params = {'cc':query}, verify=False)
# def chief_complaint(query):
#         print "Chief Complaint:", query
#         target_vocab = sorted(target_list(query)['result'])
#         if not target_vocab:
#             print "Chief complaint cannot be processed in an automated way. Using default search terms."
#             status="WARNING. Model vocabulary match could not be found for chief complaint. Using default search terms."
#             #response = {'result':{'header':format_header(user='Kevin', cc=chief_complaint, query=None, terminology=['Error'])}}
#         result = target_vocab
#         return result

