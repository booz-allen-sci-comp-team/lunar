__author__ = 'sergeyblok'

import string
import os
import sys
import inspect
import pickle
from sectionparse import parse2
from nltk.corpus import stopwords
import argparse
stopwords_eng = stopwords.words('english')


# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

import logging
import nltk
import gensim
from gensim.models import Phrases
import multiprocessing
from dictation.utils import tokenize
import getpass
import pymssql
import argparse
import pandas as pd
sec_con = pd.read_excel('C:\DataExtracts\Dictations\word2vec\section_concepts.xlsx') #used if sections are parsed out
target_section_terms = list(sec_con[sec_con.kmname.isin(['history_present_illness','chief_complaint','reason_for_consult'])]['str'].values)
target_section_terms.remove('cc') #this turns out to cause problems with some dictation tgat use cc: to copy other docs


def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")
    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#SQL Query
sampledicts = """
SELECT Result
  FROM [azDIC].[dbo].[v_OBX_TXT] (nolock)
  where tbl= 603 and UniversalServiceIdentifier in ('CR','HP')
"""

def section_parse(doc):
    if not doc:
        return ""
    secparse = parse2(doc)
    #combine values into single string
    result =  [k for k in secparse.items() if k[0].strip(":").lower() in target_section_terms]
    return " ".join([j[1] for j in result])


def clean_tokenize(docs):
    #docs = [filter(lambda x: x in string.printable, d[0]) for d in rows if d[0]]
    print "Tokenizing corpus"
    #unigram_features = []
    pool = multiprocessing.Pool(processes=8)
    unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    unigram_features = [[[t for t in s if t not in stopwords_eng and t not in ["//","/"] and len(t)>1]
                         for s in d if s] for d in unigram_features if d]
    pool.close()
    pool.join()
    print "Done with unigrams"
    unigram = [item for sublist in unigram_features for item in sublist]
    unigram = [w for w in unigram if w not in stopwords_eng]
    bigram = Phrases(unigram, threshold=10)
    trigram = Phrases(bigram[unigram], threshold=10)
    print "Done with bigrams & trigrams. "
    return unigram, bigram, trigram

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--retrain', action='store_true', help='retrain model based on local data')
    parser.add_argument('-s', '--sectionparse', action='store_true', help='only use HPI sections')
    args = parser.parse_args()
    if args.retrain:
        print "Not yet implemented"
        exit()
         # with open('C:\DataExtracts\Dictations\word2vec\\temp.sentences','r') as f:
         #    sentences = pickle.load(f)
    else:
        print "Training Word2Vec model on MS dictations"
        print 'Connecting to Database...'
        connection = mssql_connect(server='10.130.172.93', database='azDIC')
        print "ok"
        print "Reading Dictation Data"
        cur = connection.cursor()
        cur.execute(sampledicts)
        rows = cur.fetchall() #initial model is based on 1 mil reports
        print "Finished reading %i documents" %len(rows)
        if args.sectionparse:
            docs = [section_parse(r[0]) for r in rows]
            print "Parsed HPI, CC and RC sections"
        else:
            docs = rows
        unigram, bigram, trigram = clean_tokenize([d for d in docs if d]) #ignore any blank content
        print "Training Word2Vec on dictation data"
        print "Training  model now."
        sentences = trigram[bigram[unigram]]
    #Train Word2Vec model using Skip-gram encoding and window size of 10
    model = gensim.models.Word2Vec(sentences, window=10, min_count=5, workers=8, sg=1, sample=0)
    model.save('C:\DataExtracts\Dictations\word2vec\w2v.model')
    print "Done."
    connection.close()
    #finalize model
    print "Training complete. Saved model to w2v.model"
