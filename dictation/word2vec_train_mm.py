__author__ = 'sergeyblok'

import string
import os
import sys
import inspect
import logging
import nltk
from sklearn.datasets import load_files
import gensim
from gensim.models import Phrases

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)



logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import pickle



if __name__ == '__main__':
    with open("mm_output.pickle", 'r') as f:
        docs = pickle.load(f)
    #Please find way to parralelize this loop
    mm_terms = [[f['umls_term']+"_"+f['cui'] for f in doc] for doc in docs]
    model = gensim.models.Word2Vec(mm_terms, window=5, min_count=2, workers=8)
    model.save('mimic_metamap.w2v')
    print "done"
