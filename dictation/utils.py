import nltk
import string
import os, sys, inspect
from gensim.models import Phrases
#import pymssql, getpass
import re
from datetime import datetime
import numpy as np
import datetime

def n64_to_datetime(t):
    ts = int((t- np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's'))
    return ts

def flatten(l):
    return [item for sublist in l for item in sublist]

def num(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s in ["True","true"]

project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from negex.negex import negTagger, sortedRules

# def mssql_connect(server=None, database=None):
#     print server
#     if server is None:
#         server = raw_input("Please enter server IP: ")
#     if database is None:
#         database = raw_input("Please enter database : ")
#     user = raw_input("User name: ")
#     pw = getpass.getpass("Password: ")
#     conn = pymssql.connect(server, user, pw, database)
#     return conn

def memoize(func):
    class memodict(dict):

        def __init__(self, func):
            self.func = func
            self.__name__ = func.__name__
            self.__doc__ = func.__doc__

        def __call__(self, *args):
            print "__call__ :    " + func.__name__ + ", args: " + str(args)
            return self[args]

        def __missing__(self, key):
            print "__missing__ : " + func.__name__ + ", args: " + str(key)
            ret = self[key] = self.func(*key)
            return ret
    return memodict(func)


def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return:
    '''
    document = filter(lambda x: x in string.printable, document)
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]  #todo store precompile
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]
    sent_tokens = [[j.strip().lower() for j in sentence.split()] for sentence in sentences if sentence]

    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    return stripped

def get_asserted_tokens(document):
    '''
    Useful as an online helper function
    :param document:
    :return:
    '''
    return [item for sublist in tokenize(document) for item in sublist]

def get_document_features(doc):
    unigram_features = tokenize(doc)
    bigram = Phrases(unigram_features, threshold=1)
    trigram = Phrases(bigram[unigram_features], threshold=1)
    doc_features = list(set([k[0].replace("_", " ") for k in trigram.vocab.items()]))
    return doc_features

def dict_to_kv(dct):
    return [item for sublist in [[(k,j) for j in dct[k]] for k in list(dct)] for item in sublist]