import urllib, requests
from utils import n64_to_datetime, flatten
import pandas as pd
import argparse
import config
from sqlalchemy import create_engine


local_sample = [('chiefComplaint', 'bp_ivdu'),
                 ('dictations[0][BLOBIN]',
                  '   MEDSTAR WASHINGTON HOSPITAL CENTER\n   Department of Radiology\n   acid reflux Ward/Clinic:WHCEMR\n   Patient Name:xxxxxx, xxxxxx MARIE\n   MRN:xxxxxxx Patient Age:xx years\n   Admitting Provider:<Unknown>\n\n\n                                 Diagnostic Imaging\n\n   Accession Number   Exam Date/Time      Procedure          Ordering Doctor\n   DX-xx-xxxxxxx      x/xx/xx xx:xx:xx    DX Chest PA/Lat    Amirshahi MD,\n                      AM                                      MaryAnn E\n\n   CPTx Codes\n   xxxxx (xxxxx),  xxxxx (DX Chest PA/Lat)\n\n\n   Reason for Exam\n   Cough/Fever/Pneumonia\n\n\n   Read\n   EXAM:  PA and lateral views of the chest.\n\n   REASON: Cough\n\n   FINDINGS: The lungs are well expanded and clear.\n   The heart and mediastinum are normal.\n   There is no pleural effusion or pneumothorax.\n   The bones and soft tissues are unremarkable for age.\n\n   IMPRESSION:\n\n   Normal chest\n   Dictating Radiologist: Haveric MD, Namik              xx/xx/xx xx:xx\n   Electronically Signed by: Haveric MD, Namik           xx/xx/xx xx:xx\n                                                        chest pain, sob, acid_reflux, cough, dyspnea xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n   Transcribed by: NH                                    xx/xx/xx xx:xx '),
                 ('dictations[0][TEXTLEN]', '781'),
                 ('dictations[0][EVENT_TAG]', 'Chest PA and LAT'),
                 ('dictations[0][EVENT_START_DT_TM]', '1468833901'),
                 ('dictations[0][TYPE]', 'RAD'),
                 ('dictations[0][EVENT_ID]', '1234'),
                 ('dictations[0][EVENT_CD]', '54335800'),
                 ('dictations[0][CATALOG_CD]', 'Radiology'),
                 ('[0]_', '1468859992856'),
                 ('dictations[1][BLOBIN]',
                  '   MEDSTAR WASHINGTON HOSPITAL CENTER\n   Department of Radiology\n   Ward/Clinic:WHCEMR\n   Patient Name:xxxxxx, xxxxxx MARIE\n   MRN:xxxxxxx Patient Age:xx years\n   Admitting Provider:<Unknown>\n\n\n                                 Diagnostic Imaging\n\n   Accession Number   Exam Date/Time      Procedure          Ordering Doctor\n   DX-xx-xxxxxxx      x/xx/xx xx:xx:xx    DX Chest PA/Lat    Amirshahi MD,\n                      AM                                      MaryAnn E\n\n   CPTx Codes\n   xxxxx (xxxxx),  xxxxx (DX Chest PA/Lat)\n\n\n   Reason for Exam\n   Cough/Fever/Pneumonia\n\n\n   Read\n   EXAM:  PA and lateral views of the chest.\n\n   REASON: Cough\n\n   FINDINGS: The lungs are well expanded and clear.\n   The heart and mediastinum are normal.\n   There is no pleural effusion or pneumothorax.\n   The bones and soft tissues are unremarkable for age.\n\n   IMPRESSION:\n\n   Normal chest\n   Dictating Radiologist: Haveric MD, Namik              xx/xx/xx xx:xx\n   Electronically Signed by: Haveric MD, Namik           xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n                                                         xx/xx/xx xx:xx\n   Transcribed by: NH                                    xx/xx/xx xx:xx '),
                 ('dictations[1][TEXTLEN]', '781'),
                 ('dictations[1][EVENT_TAG]', 'Chest PA and LAT'),
                 ('dictations[1][EVENT_START_DT_TM]', '1468833902'),
                 ('dictations[1][TYPE]', 'RAD'),
                 ('dictations[1][EVENT_ID]', '1235'),
                 ('dictations[1][EVENT_CD]', '54335801'),
                 ('dictations[1][CATALOG_CD]', 'Radiology'),
                 ('[1]_', '146885999285')]




def load_sample_reports():
    engine = create_engine("mssql://pandas:p4nd4s@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server")
    df = pd.read_sql_table('caselevel', engine)
    result = df[df.mrn_masked==2][['ObservationDateTime','cleantext']]
    mdentries =  [[('dictations[%s][BLOBIN]' %k,d),
                 ('dictations[%s][TEXTLEN]' %k, '781'),
                 ('dictations[%s][EVENT_TAG]' %k, 'Chest PA and LAT'),
                 ('dictations[%s][EVENT_START_DT_TM]' %k, n64_to_datetime(t)),
                 ('dictations[%s][TYPE]' %k, 'ADM'),
                 ('dictations[%s][EVENT_ID]' %k, '1'),
                 ('dictations[%s][EVENT_CD]' %k, '54335800'),
                 ('dictations[%s][CATALOG_CD]' %k, 'Clinical Note'),
                 ('[%s]_' %k, '1468859992856')] for k, d, t in zip(range(len(result)),result.cleantext.values,
                                                                   result.ObservationDateTime.values)]
    cc =  df.groupby('Complaint').count().cleantext.sort_values(ascending=False).index[0]
    multidict_formatted_result = dict([('chiefComplaint',cc)]+flatten(mdentries))
    return multidict_formatted_result


def test(params, resource_url='http://127.0.0.1:8000/dictationlensapi'):
    print "Testing POST"
    rpost = requests.post(resource_url,
                         params = params )
    print rpost.json()
    print "Testing GET"
    rget = requests.get(resource_url,
                         params = params , verify=False)
    print rget.text


if __name__ == "__main__":
    if config.local:
        print "Running in local mode."
        sample_docs = local_sample
    else:
        sample_docs = load_sample_reports()
    params = urllib.urlencode(sample_docs)
    print "loaded sample docs"
    test(params)