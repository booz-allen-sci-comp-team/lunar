"""
This module is the main interface for the dictation lens.
It defines and handles requests for a REST API.
"""
import os, uuid
import string
import config
from utils import n64_to_datetime, flatten

this_home = 'C:\Users\SXB806\lunar\dictation\\'
input_path = 'C:\Medex_UIMA_1.3.6\input'
output_path = 'C:\Medex_UIMA_1.3.6\output'
medex_home = "C:\Medex_UIMA_1.3.6\\"

#from semantics import to_w2v_vocab, get_closest4
from build_terms import ivduterms, termset

import re
import pandas as pd
from sqlalchemy import create_engine
# load basic seed terms
from utils import memoize, get_document_features
# with open('terms.pickle', 'r') as f:
#     terms = pickle.load(f)
# print "Loaded terminology list"

termbank = pd.read_excel(config.termbank_path)
categorybank = pd.read_excel(config.categorybank_path)

def format_count_dicts(term_list, count_dict):
    term_list = [term.replace('_',' ') for term in term_list] #remove underscores
    tcounts = dict([(t,0) for t in term_list]) #init counts
    for t in term_list:
        if t in count_dict:
            tcounts[t] = count_dict[t]
    return [{'term': k[0],'occurrences':k[1]} for k in tcounts.items()]


def format_header(user, cc, matched_words,  query, terminology,  cc_vocab, default_vocab, term_counts, model_params):

    rj = {
        "user": user,
        "chiefComplaint": cc,
        "matchedChiefComplaintWords":matched_words,
        "query": query,
        "id": "15",
        "oid": str(uuid.uuid4()),
        "keyEvents": None,
        "facility": "Example Center",
        "_version_": 1528814374916980700,
        "targetTerms": {'recommended': format_count_dicts(cc_vocab,term_counts),
                        'universal': format_count_dicts(default_vocab,term_counts)},
        # "chiefComplaint_terms":cc_vocab,
        # "defaultTerms":default_vocab,
        "model_params":model_params
    }
    return rj

def format_response(meta, doc, annotated, found_spans_all, match_terms, all_matches_for_this_patient, score, cc, tokens=None):
    rj = {
        "chiefComplaint": cc,
        "dictationText": doc,
        "tokens": tokens,
        "matchedTerms": match_terms,
        "dictationTextAnnotated": annotated,
        "actualWords": None,
        "actualWordsTokenized": None,
        "spans": found_spans_all,
        "score": score
    }
    rj.update(meta)
    return rj



def add_span(document, id_label, class_label, pos):
    spanstring = '<span id=%s class=%s>%s</span>'%("'" + id_label + "'", "'" + class_label + "'", document[pos[0]+1:pos[1]])
    newdoc = document[:pos[0]+1] + spanstring + document[pos[1]:]
    # print '<span class=%s>%s</span>' % ("'" + class_label + "'", document[pos[0]:pos[1]])
    return {'document':newdoc, 'string':spanstring}


def annotate_one(dictation, termlist, meta, universal, recommended):
    '''

    Args:
        dictation: string
        termlist: list of strings
        meta: dict

    Returns:

    '''
    doc = dictation
    doc = filter(lambda x: x in string.printable, doc)
    d_annotated = doc
    document_features = get_document_features(doc) # tokenize
    common_features = list(set(termlist).intersection(document_features))
    matches = []
    score = len(common_features)
    found_spans_universal = []#[k.span() for k in re.finditer(re.compile('<span class=keyword-global(.*?)span>'), d_annotated)]
    found_spans_recommended = []#[k.span() for k in re.finditer(re.compile('<span class=keyword-recommended(.*?)span>'), d_annotated)]

    if common_features:
        id = 0
        for phrase in common_features:
            matches.append(phrase)
            if phrase in universal:
                match_type = 'U'
                #universal_matches.append(phrase)
            elif phrase in recommended:
                match_type = 'R'
                #recommended_matches.append(phrase)
            #first_match = [k for k in re.finditer(re.compile(phrase, re.IGNORECASE), d_annotated)][0]
            done = False
            while not done:
                m = re.search(re.compile(" "+phrase, re.IGNORECASE), d_annotated)
                if m:
                    pos = m.span()
                    if match_type == 'U':
                        sp = add_span(d_annotated, str(id), 'keyword-global keyword', pos)
                        d_annotated = sp['document']
                        found_spans_universal.append(sp['string'])
                    elif match_type == 'R':
                        sp = add_span(d_annotated, str(id), 'keyword-recommended keyword', pos)
                        d_annotated = sp['document']
                        found_spans_recommended.append(sp['string'])
                else:
                    done = True
            id += 1
    #get snippets from formatted doc

    #snippets_universal = list(set([d_annotated[max(0,span[0]-50):max(0,span[1]+50)] for span in found_spans_universal]))
    #snippets_recommended = list(set([d_annotated[max(0,span[0]-50):max(0,span[1]+50)] for span in found_spans_recommended]))
    found_spans_all = get_spans_snippets(d_annotated)
    # result = format_response(meta, doc, d_annotated, found_spans_universal, found_spans_recommended, matches, None, score, tokens=None, cc=None, )
    result = format_response(meta, doc, d_annotated, found_spans_all, matches, None, score, tokens=None, cc=None, )
    return result

def get_spans_snippets(doc):
    found_spans_all = []
    # found_spans_all = re.findall('(<span[^<]+<\/span>)', doc, re.IGNORECASE)
    pattern = "((?:[\dA-z'-]+[^A-z'->]+){0,4}<span[^<]+<\/span>(?:[^A-z'-<]+[\dA-z'-]+){0,4})"
    found_spans_all = re.findall(pattern, doc, re.IGNORECASE)
    return found_spans_all

def run_medex():
    os.chdir(os.path.dirname(medex_home))
    cmd = "java -Xmx1024m -cp {0}lib/*;{0}bin org.apache.medex.Main -i {1} -o {2}".format(medex_home, input_path, output_path)
    os.system(cmd)
    os.chdir(os.path.dirname(this_home))


def critical_meds(docs, medlist=None):
    '''

    Use MedEX to extract medications and match against critical medlist

    Args:
        doc:
        medlist:

    Returns:

    '''

    #write doc files to input
    for i, doc in enumerate(docs):
        with open(input_path+"/"+str(i), 'w') as f:
            f.write(doc)

    #run medEx
    run_medex()
    results = []
    for i, doc in enumerate(docs):
        with open(output_path+"/"+str(i), 'r') as f:
            results.append(f.read())
    cols = ['Sentence_index',
            'Sentence_text',
            'Drug_name',
            'Brand_name',
            'Drug_form',
            'Strength',
            'Dose_amount',
            'Route',
            'Frequency',
            'Duration',
            'Neccessity',
            'UMLS_CUI',
            'RXNORM_RxCUI',
            'RXNORM_generic_RxCUI',
            'Generic_name']
    med_results = []
    for i in range(len(docs)):
        df = pd.DataFrame([j.split("|") for j in [k.replace("\t","|") for k in results[i].split("\n") if k]], columns = cols)
        df['doc_index'] = i
        med_results.append(df.to_dict(orient='records'))
    #erase files
    filelist = [f for f in os.listdir(input_path)]
    for f in filelist:
        os.remove(input_path+"\\"+f)

    filelist = [f for f in os.listdir(output_path)]
    for f in filelist:
        os.remove(output_path+"\\"+f)
    return med_results


#@memoize
def cc_related_terms(chief_complaint, **kwargs):
    status = "OK"
    ts = termset(chief_complaint, **kwargs) #initialize the related terms object
    tl = ts.related_terms
    target_vocab,  cue_set = tl['result'], tl['cue_set']

    termsf = sorted(list(set(target_vocab + ts.default_terms)))
    #add header
    response = {'result':{'header': format_header(user='Kevin',
                                                  cc=chief_complaint,
                                                  matched_words = cue_set,
                                                  query=None,
                                                  terminology=termsf,
                                                  cc_vocab=target_vocab,
                                                  default_vocab=ts.default_terms,
                                                  term_counts = {},
                                                  model_params=dict([(k,ts.__dict__[k]) for k in ts.__dict__.keys()
                                                                     if k not in ['related_terms','default_terms']]))}}
    return response

def get_stored_terms(chief_complaint):

    try: # check to see if verbatim term is in category bank
        category = (categorybank.loc[categorybank['complaint'] == chief_complaint]['category']).values[0]
    except:
        # print "Verbatim term did not match. Trying again with titlecase."
        try:
            category = (categorybank.loc[categorybank['complaint'] == chief_complaint.title()]['category']).values[0]
        except:
            try:
                # print "Title casing did not match. Trying again with lowercase."
                category = (categorybank.loc[categorybank['complaint'] == chief_complaint.lower()]['category']).values[0]
            except:
                try:
                    # print "Lowercase did not match. Trying again with uppercase."
                    category = (categorybank.loc[categorybank['complaint'] == chief_complaint.upper()]['category']).values[0]
                except:
                    print "ERROR: Chief Complaint %s not in category bank" % chief_complaint
                    return []

    print "SUCCESS: Chief Complaint %s matched in category bank" % chief_complaint
    terms = list(termbank[termbank.chief_complaint == category.lower().replace(" ","_")]['candidate_term'].sort_values().values)
    if not terms: print "ERROR: Chief Complaint %s not found in term bank" % chief_complaint
    else: print "SUCCESS: Chief Complaint %s found in term bank" % chief_complaint
    return terms

def main_annotator(chief_complaint, dictations, metadata, stored=False):
    if stored:
        ts = termset('', curated=True)
        target_vocab = get_stored_terms(chief_complaint)
        if target_vocab:
            cue_set = [chief_complaint]
        else:
            cue_set = []
    elif chief_complaint == 'bp_ivdu':
        ts = termset(chief_complaint)
        target_vocab=ivduterms
        cue_set = ['back pain','ivdu']
    else:
        ts = termset(chief_complaint) #initialize the related terms object
        tl = ts.related_terms
        target_vocab,  cue_set = tl['result'], tl['cue_set']
    termsf = sorted(list(set(target_vocab + ts.default_terms)))
    response = {'result':{'dictations': []}}
    term_counter = {}
    for i in range(len(dictations)):
        annotation_result = annotate_one(dictations[i], termsf, metadata[i], ts.default_terms,target_vocab)
        for m in annotation_result['matchedTerms']:
            if m in  term_counter:
                term_counter[m]+=1
            else:
                term_counter[m]=1
        response['result']['dictations'].append(annotation_result)

   # response['result']['header']['term_count'] = term_counter
    response['result']['header'] = format_header(user='Kevin', cc=chief_complaint, matched_words = cue_set, query=None,
                                             terminology=termsf,  cc_vocab=target_vocab,
                                              default_vocab=ts.default_terms, term_counts = term_counter,
                                              model_params=dict([(k,ts.__dict__[k]) for k in ts.__dict__.keys()
                                                                 if k not in ['related_terms','default_terms']]))

    return response



def meds(docs):
    if not docs:
        docs = sample_docs
    #add header
    response = {'result':{'header':format_header(user='Kevin', cc=None, query='medications', terminology=None)}}
    #add body
    response['result']['dictations'] = critical_meds(docs, None)
    return response


