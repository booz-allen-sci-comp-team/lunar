# Julia Hu, Section Parsing

import re

def parse2(document):
    p = re.compile('^(?<!\d\.  )([A-Za-z \/]+:)', re.MULTILINE)
    parsed = p.split(document)
    result = dict(zip([k for (i,k) in enumerate(parsed[1:]) if not i % 2],[k for (i,k) in enumerate(parsed[1:]) if i % 2]))
    return result

def parse(document):
    myDict = {}
    currKey = ''
    currVal = ''

    for line in document:
        # print 'This line is: ' + line
        match = re.findall('^(?<!\d\.  )([A-Z \/]+:)', line)
        if match != []:
            if currVal != '' and currKey != '':
                # print 'new dict entry has key: ' + currKey + ' and value: ' + currVal
                myDict[currKey] = currVal
                currVal = ''
            currKey = match[0].strip()
            currVal = line.replace(currKey, '')
        # print 'currKey is ' + currKey + ' and currVal is ' + currVal
        else:
            currVal += line

    if currKey not in myDict:
        # print 'new dict entry has key: ' + currKey + ' and value: ' + currVal
        myDict[currKey] = currVal

    return myDict


if __name__ == '__main__':
    with open('00003-013045.txt', 'r') as f:
        lines = f.readlines()
    dictionary = parse(lines)
    print '********************'
    print dictionary.keys()
    print dictionary.values()
