__author__ = 'juliahu'
    # """
    # Only for one complaint
    # """
    # # documents = result.groupby(['category'])['parsed'].apply(lambda x: x.tolist()).reset_index() # group by category
    # # complaint_list = (documents.loc[documents['category'] == 'Alcohol']['parsed']).values[0]
    # # docs = pd.DataFrame(complaint_list)[0] 
    # """
    # """
import pandas as pd
import getpass
import multiprocessing
import nltk
from gensim.models import Phrases, Word2Vec
from gensim import corpora, models
from sqlalchemy import create_engine

import string
import os
import sys
import inspect
import pymssql
import argparse
from sectionparse import parse2
from nltk.corpus import stopwords
stopwords_eng = stopwords.words('english')

sec_con = pd.read_excel('C:\DataExtracts\Dictations\word2vec\section_concepts.xlsx')
target_section_terms = list(sec_con[sec_con.kmname.isin(['history_present_illness','chief_complaint','reason_for_consult'])]['str'].values)
target_section_terms.remove('cc') #this turns out to cause problems with some dictation tgat use cc: to copy other docs

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from negex.negex import *

from metamap import metamap_interface
mm = metamap_interface.MetamapInterface(ade_types=['sosy', 'fndg', 'dsyn', 'mobd', 'patf', 'pdsu', 'phsu','topp'])
if not mm.metamapstr("tylenol"):
    print "======>Warning: MetaMap did not start."

def query_mm(vocab):
    filter_by_types = ['dsyn', 'mobd', 'patf', 'pdsu','phsu', 'topp']

    phrase = vocab.replace("_"," ")
    mm_result = mm.metamapstr(phrase)
    if mm_result:
        umls_type = mm_result[0][5].replace("[", "").replace("]", "")

        for word_type in umls_type.split(','):
            if word_type in filter_by_types:
                return vocab

def add_mm_data(term_list):
    pool = multiprocessing.Pool(processes=8)
    vocab_postproc = pool.map(query_mm,
                              term_list)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()
    # vocab_postproc = [query_mm(k) for k in model.vocab.iteritems()]
    return vocab_postproc

def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")
    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn

def section_parse(doc):
    if not doc:
        return ""
    secparse = parse2(doc)
    #combine values into single string
    result =  [k for k in secparse.items() if k[0].strip(":").lower() in target_section_terms]
    return " ".join([j[1] for j in result])


def clean_tokenize(docs):
    #docs = [filter(lambda x: x in string.printable, d[0]) for d in rows if d[0]]
    print "Tokenizing corpus"
    #unigram_features = []
    pool = multiprocessing.Pool(processes=8)
    unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    unigram_features = [[[t for t in s if t not in stopwords_eng and t not in ["//","/"] and len(t)>1]
                         for s in d if s] for d in unigram_features if d]
    pool.close()
    pool.join()
    print "Done with unigrams"
    unigram = [item for sublist in unigram_features for item in sublist]
    unigram = [w for w in unigram if w not in stopwords_eng]
    bigram = Phrases(unigram, threshold=15)
    trigram = Phrases(bigram[unigram], threshold=15)
    print "Done with bigrams & trigrams. "
    return unigram, bigram, trigram

def load_sample_reports():
    '''
    Connect to sql database and queries dictations
    :return DataFrame of dictations
    # '''
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    engine_name = "mssql://" + user + ":" + pw + "@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server"
    engine = create_engine(engine_name)
    print "Connected to database."
    DICTS = pd.read_sql("select [complaint],[result] from dbo.complaints_1000 where result <> ''", engine)
    # DICTS = pd.read_excel('c:/DataExtracts/chief_complaints/DICTS.xlsx')
    return DICTS

def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return list of tokens:
    '''
    document = filter(lambda x: x in string.printable, document)
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]
    sent_tokens = [[j.strip().lower() for j in sentence.split()] for sentence in sentences if sentence]

    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    flattened_stripped = [item for sublist in stripped for item in sublist] # flatten lists
    return [flattened_stripped]

def clean_tokenize(docs):
    #docs = [filter(lambda x: x in string.printable, d[0]) for d in rows if d[0]]
    print "Tokenizing corpus"
    #unigram_features = []
    pool = multiprocessing.Pool(processes=8)
    unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    unigram_features = [[[t for t in s if t not in stopwords_eng and t not in ["//","/"] and len(t)>1]
                         for s in d if s] for d in unigram_features if d]
    pool.close()
    pool.join()
    print "Done with unigrams"
    unigram = [item for sublist in unigram_features for item in sublist]
    unigram = [w for w in unigram if w not in stopwords_eng]
    bigram = Phrases(unigram, threshold=15)
    trigram = Phrases(bigram[unigram], threshold=15)
    print "Done with bigrams & trigrams. "
    return list(trigram[bigram[unigram]])

def getComplaints(num_complaints):

    tfidf_model = corpora.MmCorpus('c:/DataExtracts/tfidf_models/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/tfidf_models/dictionary')
    categories = pd.read_hdf('c:/DataExtracts/tfidf_models/categories')
    print "Loaded TFIDF model, dictionary, and categories."

    complaints_list = [] #list of top complaints
    count = 0

    for doc in tfidf_model:
        terms = ''
        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True)[:num_complaints] # get top num_complaints

        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary

            if index != 0:
                terms += ', ' + (str(dictionary[dict_index]))
            elif index == 0:
                terms += (str(dictionary[dict_index]))

        category = categories[count]
        complaints_list.append([str(category).upper(), terms]) # write top terms to list
        count += 1

    return complaints_list

def getTermWeights():
    tfidf_model = corpora.MmCorpus('c:/DataExtracts/tfidf_models/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/tfidf_models/dictionary')
    categories = pd.read_hdf('c:/DataExtracts/tfidf_models/categories')
    print "Loaded TFIDF model, dictionary, and categories."

    complaints_list = [] #list of top complaints
    weights_list = [] #list of top weights
    categories_list = [] #list of categories
    count = 0

    doc_index = 0
    for doc in tfidf_model:
        # if count  == 1: break

        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True) # get top num_complaints
        # print top_complaints
        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary
            complaints_list.append((str(dictionary[dict_index])))
            weights_list.append(top_complaints[index][1])
            categories_list.append(categories[doc_index])

        count += 1
        doc_index += 1

    complaints_df = pd.DataFrame({'chief_complaint': categories_list, 'terms': complaints_list, 'weights': weights_list})

    return complaints_df

def getTermCounts():
    bow_model = corpora.MmCorpus('c:/DataExtracts/tfidf_models/bow.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/tfidf_models/dictionary')
    categories = pd.read_hdf('c:/DataExtracts/tfidf_models/categories')
    print "Loaded BOW model, dictionary, and categories."

    complaints_list = [] #list of top complaints
    counts_list = [] #list of top weights
    categories_list = [] #list of categories
    count = 0

    doc_index = 0
    for doc in bow_model:
        # if count  == 1: break

        top_complaints = sorted(doc, key = lambda count: count[1], reverse = True) # get top num_complaints
        # print top_complaints
        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary
            complaints_list.append((str(dictionary[dict_index])))
            counts_list.append(top_complaints[index][1])
            categories_list.append(categories[doc_index])

        count += 1
        doc_index += 1

    complaints_df = pd.DataFrame({'chief_complaint': categories_list, 'terms': complaints_list, 'counts': counts_list})

    return complaints_df
if __name__ == "__main__":

    DICTS = load_sample_reports()
    print "Done loading documents."
    res = DICTS['result']

    DICTS['parsed'] = [section_parse(r) for r in res]
    print "Parsed HPI, CC and RC sections"

    CMPL = pd.read_excel('c:/DataExtracts/tfidf_models/top_1000_complaints_new.xlsx')
    result = pd.merge(CMPL, DICTS, on = 'complaint', how = 'right') # merge data with top 1000 complaints
    documents = result.groupby(['category'])['parsed'].apply(lambda x: '\n'.join(x)).reset_index() # group by category
    documents['category'].to_hdf('c:/DataExtracts/tfidf_models/categories', 'category', mode = 'w') # save categories
    docs = documents['parsed'] # store dictations in docs

    features = clean_tokenize([d for d in docs if d])

    print "Filtering through metamap."
    # filter through metamap
    dictionary_list = corpora.Dictionary(features).values() #removes duplicates
    dictionary = corpora.Dictionary([filter(None, add_mm_data(dictionary_list))]) #filters out none types

    print "Training TFIDF model on dictation data."
    bow_corpus = [dictionary.doc2bow(feature) for feature in features] # vectorize docs
    corpora.MmCorpus.serialize('c:/DataExtracts/tfidf_models/bow.mm', bow_corpus)
    tfidf = models.TfidfModel(bow_corpus, normalize = True)
    corpus_tfidf = tfidf[bow_corpus]

    corpora.MmCorpus.serialize('c:/DataExtracts/tfidf_models/tfidf.mm', corpus_tfidf) # save vectorized docs
    dictionary.save('c:/DataExtracts/tfidf_models/dictionary')
    print "Done with TFIDF model. Saved model and dictionary."

    # # GET TOP N COMPLAINTS
    # complaints_list = getComplaints(2)
    # print complaints_list
    # complaints_df = getAllComplaints()
    complaints_df = getTermWeights()
    print "Done with complaints list."

    writer = pd.ExcelWriter('c:/DataExtracts/tfidf_models/top_1000_complaints_weights.xlsx')
    complaints_df.to_excel(writer,'Sheet1', index = False)
    print "Done saving to excel."


    # """
    # Test Metamap
    # """
    # vocab_list = ['abuse', 'back_pain', 'chest_pain', 'bird_poop']
    # filter_by_types = ['dsyn', 'mobd', 'patf', 'pdsu','phsu', 'topp']
    # for vocab in vocab_list:
    #     phrase = vocab.replace("_"," ")
    #     mm_result = mm.metamapstr(phrase)
    #     if mm_result:
    #         umls_type = mm_result[0][5].replace("[", "").replace("]", "")
    #         print umls_type
    #         if umls_type in filter_by_types: print True
