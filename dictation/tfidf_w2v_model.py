__author__ = 'juliahu'

import inspect
import os
import sys
import pandas as pd
import getpass
import multiprocessing
import nltk
import string
from gensim.models import Phrases, Word2Vec
from gensim import corpora, models
from sqlalchemy import create_engine

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from negex.negex import *

def load_sample_reports():
    '''
    Connect to sql database and queries dictations
    :return DataFrame of dictations
    '''
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    engine_name = "mssql://" + user + ":" + pw + "@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server"
    engine = create_engine(engine_name)
    print "Connected to database."
    DICTS = pd.read_sql("select [complaint],[result] from dbo.complaints where result <> ''", engine)
    # DICTS = pd.read_excel('c:/DataExtracts/chief_complaints/DICTS.xlsx')
    return DICTS

def tokenize(document):
    '''
    Get ngram feautures for a document and remove likely negated tokens sa well as negation determiners
    :param document:
    :return list of tokens:
    '''
    document = filter(lambda x: x in string.printable, document)
    sentences = nltk.sent_tokenize(document)
    sentences = [re.sub(re.compile('<.*?>|[0-9]|\[.*?\]|([A-Z]){2,}'),'', s) for s in sentences]
    sentences = ["".join([char for char in sentence if char not in string.punctuation.replace("/", "")]) for sentence in sentences]
    sent_tokens = [[j.strip().lower() for j in sentence.split()] for sentence in sentences if sentence]

    # remove negated tokens
    tagged = [negTagger(sentence=" ".join(sent), phrases=sent, rules=sortedRules, negP=False).getNegTaggedSentence() for sent in sent_tokens]
    # negex_annotated = nt.getNegTaggedSentence()
    stripped = [[re.sub('\[.*?\]', '', i) for i in sent.split() if 'NEGATED' not in i] for sent in tagged]
    flattened_stripped = [item for sublist in stripped for item in sublist] # flatten lists
    return [flattened_stripped]

def getComplaints(num_complaints):

    tfidf_model = corpora.MmCorpus('c:/DataExtracts/chief_complaints/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/chief_complaints/w2v_dict')
    categories = pd.read_hdf('c:/DataExtracts/chief_complaints/categories')
    print "Loaded TFIDF model, w2v dictionary, and categories."

    complaints_list = [] #list of top complaints
    count = 0

    for doc in tfidf_model:
        terms = ''
        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True)[:num_complaints] # get top num_complaints

        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary

            if index != 0:
                terms += ', ' + (str(dictionary[dict_index]))
            elif index == 0:
                terms += (str(dictionary[dict_index]))

        category = categories[count]
        complaints_list.append([str(category).upper(), terms]) # write top terms to list
        count += 1

    return complaints_list

def getAllComplaints():

    tfidf_model = corpora.MmCorpus('c:/DataExtracts/chief_complaints/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/chief_complaints/w2v_dict')
    categories = pd.read_hdf('c:/DataExtracts/chief_complaints/categories')
    print "Loaded TFIDF model, w2v dictionary, and categories."

    complaints_list = [] #list of top complaints
    count = 0

    for doc in tfidf_model:
        # if count  == 1: break
        terms = []
        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True) # get top num_complaints

        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary
            terms.append((str(dictionary[dict_index])))

        count += 1
        complaints_list.append(terms)

    complaints_df = pd.DataFrame(complaints_list)
    complaints_df = complaints_df.transpose()
    complaints_df.columns = categories.tolist()

    return complaints_df

def getWeights():
    tfidf_model = corpora.MmCorpus('c:/DataExtracts/chief_complaints/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/chief_complaints/w2v_dict')
    categories = pd.read_hdf('c:/DataExtracts/chief_complaints/categories')
    print "Loaded TFIDF model, w2v dictionary, and categories."

    complaints_list = [] #list of top complaints
    count = 0

    for doc in tfidf_model:
        # if count  == 1: break
        terms = []
        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True) # get top num_complaints
        # print top_complaints
        for index in range(len(top_complaints)): # print top complaints
            terms.append(top_complaints[index][1])
            # dict_index = top_complaints[index][0] # get index of term in dictionary
            # terms.append((str(dictionary[dict_index])))

        count += 1
        complaints_list.append(terms)

    complaints_df = pd.DataFrame(complaints_list)
    complaints_df = complaints_df.transpose()
    complaints_df.columns = categories.tolist()

    return complaints_df

def getTermWeights():
    tfidf_model = corpora.MmCorpus('c:/DataExtracts/chief_complaints/tfidf.mm')
    dictionary = corpora.Dictionary.load('c:/DataExtracts/chief_complaints/w2v_dict')
    categories = pd.read_hdf('c:/DataExtracts/chief_complaints/categories')
    print "Loaded TFIDF model, w2v dictionary, and categories."

    complaints_list = [] #list of top complaints
    weights_list = [] #list of top weights
    categories_list = [] #list of categories
    count = 0

    doc_index = 0
    for doc in tfidf_model:
        # if count  == 1: break

        top_complaints = sorted(doc, key = lambda weight: weight[1], reverse = True) # get top num_complaints
        # print top_complaints
        for index in range(len(top_complaints)): # print top complaints
            dict_index = top_complaints[index][0] # get index of term in dictionary
            complaints_list.append((str(dictionary[dict_index])))
            weights_list.append(top_complaints[index][1])
            categories_list.append(categories[doc_index])

        count += 1
        doc_index += 1

    complaints_df = pd.DataFrame({'chief_complaint': categories_list, 'terms': complaints_list, 'weights': weights_list})

    return complaints_df


if __name__ == "__main__":

    # DICTS = load_sample_reports()
    # print "Done loading documents."
    # print len(DICTS)
    # CMPL = pd.read_excel('c:/DataExtracts/chief_complaints/top_100_complaints.xlsx')
    # result = pd.merge(CMPL, DICTS, on = 'complaint', how = 'right') # merge data with top 100 complaints
    # documents = result.groupby(['category'])['result'].apply(lambda x: '\n'.join(x)).reset_index() # group by category
    # documents['category'].to_hdf('c:/DataExtracts/chief_complaints/categories', 'category', mode = 'w') # save categories
    # docs = documents['result'] # store dictations in docs
    #
    # # Tokenize dictations in parallel
    # pool = multiprocessing.Pool(processes = 8)
    # unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    # pool.close()
    # pool.join()
    #
    # features = []
    # for l1 in unigram_features:
    #     for l2 in l1:
    #         if l2 != []:
    #             features.append(l2)
    #
    # print "Done with unigrams. \n"
    #
    # # Create unigrams, bigrams, trigrams
    # unigram = [item for sublist in unigram_features for item in sublist]
    # bigram = Phrases(unigram, threshold=5)
    # trigram = Phrases(bigram[unigram], threshold=5)
    # features = trigram[bigram[unigram]]
    #
    # print "Done with bigrams & trigrams. Training model now. \n"
    #
    # w2v_model = Word2Vec.load('C:\DataExtracts\Dictations\word2vec\w2v.model') # load word2vec model
    # w2v_dict = corpora.Dictionary([w2v_model.vocab.keys()]) # load word2vec dictionary
    # print "Loaded word2vec model and dictionary. \n"
    #
    # bow_corpus = [w2v_dict.doc2bow(feature) for feature in features] # vectorize docs
    # tfidf = models.TfidfModel(bow_corpus)
    # corpus_tfidf = tfidf[bow_corpus]
    #
    # corpora.MmCorpus.serialize('c:/DataExtracts/chief_complaints/tfidf.mm', corpus_tfidf) # save vectorized docs
    # w2v_dict.save('c:/DataExtracts/chief_complaints/w2v_dict')
    # print "Done with TFIDF model. Saved model and dictionary."

    # GET TOP N COMPLAINTS
    # complaints_df = getAllComplaints()
    complaints_df = getTermWeights()
    print "Done with complaints list."

    writer = pd.ExcelWriter('c:/DataExtracts/chief_complaints/top_complaints_weights.xlsx')
    complaints_df.to_excel(writer,'Sheet1', index = False)
    print "Done saving to excel."

    # f = open('c:/DataExtracts/chief_complaints/top_complain ts.txt', 'w')
    # for x in complaints_list:
    #     f.write(str(x)+'\n')
