from sqlalchemy import create_engine
from dictation.lens import annotate_one
from dictation.backpain_ivdu_termSimilarity_JB import outset as ivduterms
import pandas as pd
engine = create_engine("mssql://pandas:p4nd4s@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server")
df = pd.read_sql_table('Tbl603', engine)
smaller_df = df[0:1000]
print "Looking for sample docs that contain relevant terms."
smaller_df['bp_ivdu'] = smaller_df.result.apply(lambda x: annotate_one(x,list(ivduterms))['matchedTerms'])
smaller_df['score'] = smaller_df.bp_ivdu.apply(lambda x: len(smaller_df.bp_ivdu[x]))
sample = smaller_df.sort('score', ascending=False)[0:100]
sample[['idx','result']].to_sql('sampledicts', engine, if_exists='replace', index=False)
print "Done writing sample of %i records to DB table sampledicts" %len(sample)
