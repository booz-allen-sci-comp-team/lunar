__author__ = 'sergeyblok'

import inspect
import logging
import os
import string
import sys
from sklearn.datasets import load_files
import gensim
from gensim.models import Phrases
import multiprocessing
from utils import tokenize

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

if __name__ == '__main__':
    filename = 'mimic_win20.w2v'
    print "Training Word2Vec on MIMIC data"
    df = load_files('dictation/data')
    print "Loaded MIMIC data"
    docs = df['data']
    print "Removing unprintable characters"
    docs = [filter(lambda x: x in string.printable, d) for d in docs]
    print "Tokenizing corpus"
    unigram_features = []
    pool = multiprocessing.Pool(processes=8)
    unigram_features = pool.map(tokenize, docs)  # a list of a list of a list [[[z,b],[a,b]],[[a,b,c],[d,g,f]]]
    pool.close()
    pool.join()
    print "Done with unigrams"
    unigram = [item for sublist in unigram_features for item in sublist]
    bigram = Phrases(unigram, threshold=5)
    trigram = Phrases(bigram[unigram], threshold=5)
    print "Done with bigrams & trigrams. Training model now."
    model = gensim.models.Word2Vec(trigram[bigram[unigram]], window=7, min_count=5, workers=8)
    model.init_sims(replace=True)
    model.save(filename)
    print "Done."
    print "saved model to", filename
