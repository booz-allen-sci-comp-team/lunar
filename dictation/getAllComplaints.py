__author__ = 'juliahu'

import inspect
import os
import sys
import pandas as pd
import getpass
import pymssql, getpass

from sqlalchemy import create_engine

def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")
    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn

def load_sample_reports():
    server = '10.130.172.93'
    adt_connection = mssql_connect(server=server, database="azADT")
    print "Connected to database."

    query = "select complaint, count(complaint) as counts from azADT.dbo.v_VST601 (nolock) where	complaint <> '' group by complaint order by counts desc"
    complaints_df = pd.read_sql(query, adt_connection)

    writer = pd.ExcelWriter('c:/DataExtracts/chief_complaints/top_complaints_all.xlsx')
    complaints_df.to_excel(writer,'Sheet1', index = False)
    print "Done saving to excel."

if __name__ == "__main__":
	load_sample_reports()
