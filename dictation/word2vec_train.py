"""
Implement GenSim word2vec representation and train model
"""

from gensim import models
import getpass
import pymssql
import nltk
import pandas as pd
from collections import defaultdict
from sklearn.datasets import load_files
import logging
import re
import csv
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from nltk.corpus import stopwords
from nltk.tag import pos_tag
def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")
    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn


def doc_clean_tokenize(report_text):
    report_text = str(report_text)
    if not report_text:
        return []
    else:
        sentences = report_text.split(".")# too slow->nltk.sent_tokenize(report_text)

        words =  [re.compile("[a-zA-Z]+").findall(sentence) for sentence in sentences
                  if len(re.findall(r'(?<!\.\s)\b[A-Z][a-z]*\b', sentence))<10]

        # words = [re.compile("[a-zA-Z]+").findall(sentence) for sentence in sentences]
        result = [item.lower() for sublist in words for item in sublist
                  if item.lower() not in stopwords.words('english') and len(item)>1]
        return result
stop = stopwords.words('english')
wordfnd = re.compile("[a-zA-Z]+")

stop_mini = set('for a of the and to in'.split())

def doc_clean_tokenize_faster(report_text):
    report_text = str(report_text.lower())
    sentences = report_text.split(".")# too slow->nltk.sent_tokenize(report_text)

    words =  [wordfnd.findall(sentence) for sentence in sentences]
    #words = [nltk.word_tokenize(sentence) for sentence in sentences]

    # words = [re.compile("[a-zA-Z]+").findall(sentence) for sentence in sentences]
    result = [item for sublist in words for item in sublist
              if item not in stop_mini and len(item)>1]
    return result


def tokenize(str_seq):
    # remove common words and tokenize
    stop_list = set('for a of the and to in'.split())
    texts = [[word for word in document.lower().split() if word not in stop_list]
             for document in str_seq]
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    texts = [[token for token in text if frequency[token] > 1]
             for text in texts]
    return texts

def is_mostly_header(a):
    b = a.lower()
    ratio_lower_to_upper = len([j for j in [k for k in a if b[a.index(k)]==k] if j != ' '])/float(len(a))
    if ratio_lower_to_upper < .5:
        return True
    else:
        return False

def incremental_train():
    model = models.Word2Vec([doc_clean_tokenize_faster(row[0]) for row in cur.fetchmany(1000)], size=200, window=10, min_count=10, workers=4)
    model.save('dictations.w2v')
    c=0
    new_sentences = [doc_clean_tokenize_faster(row[0]) for row in cur.fetchmany(1000)]
    for row in cur:
        if c % 1000 == 0:
            model = models.Word2Vec.load('dictations.w2v')
            model.train(new_sentences, total_examples=1000)
            new_sentences = []
            print "Row", c
            print "Adding to model"
            model.save('dictations.w2v')
        c = c + 1
        new_sentences.append(doc_clean_tokenize_faster(row[0]))

def conn_raw():
    connection = mssql_connect(server='10.130.172.93', database='azDIC')
    #myconn <- odbcDriverConnect('driver={SQL Server};server=10.130.172.93;database=azDIC;trusted_connection=yes')
    #tables=sqlQuery(myconn, 'select * from information_schema.tables')
    #tables = tables[tables$TABLE_TYPE=="BASE TABLE",]

    #targetTables = as.character(tables[nchar(as.character(tables$TABLE_NAME))==10,"TABLE_NAME"])


    results = sqlQuery(connection, 'select TOP 100 * from WHC_OBR601')




if __name__ == '__main__':
    print 'Connecting to Database...'
    connection = mssql_connect(server='MI2-BAH-DSDB01', database='Medstar')
    print "ok"
    print "Reading Data"
    cur = connection.cursor()
    cur.execute("SELECT observationvalue FROM [Medstar].[dbo].[DictationProcessedRollup]")
    rows = cur.fetchall()
    sentences = []
    for i, row in enumerate(rows):
         if i % 1000 == 0:
             print "Working on ROW:", i
         text = row[0]
         if text:
            sentences.append(doc_clean_tokenize_faster(text))
    print "Getting bigrams."
    bigram_transformer = models.Phrases(sentences)
    print "Training Model"
    model = models.Word2Vec(bigram_transformer[sentences], size=100, window=5, min_count=10, workers=4)
    model.save('dictations2.w2v')


