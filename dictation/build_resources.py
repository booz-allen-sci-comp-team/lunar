from sqlalchemy import create_engine

from lens import annotate_one
#from backpain_ivdu_termSimilarity_JB import outset as ivduterms
import pandas as pd
from utils import mssql_connect
import deident
import warnings
warnings.filterwarnings('ignore')


case_level_select_script = """
select cmplnt.EID, FillerOrderNumber, PatientMedicalRecordNumber, usecase, Complaint, InstitutionName,
	UniversalServiceIdentifier, ObservationDateTime, Result from (
select PID2.EID, PID2.PatientMedicalRecordNumber, usecase, PID2.InstitutionName from
(select distinct top %i PID.PatientMedicalRecordNumber, compl.Complaint as usecase from
((SELECT [EID]
      ,[Complaint]
	  ,[AdmitDateTime]
      ,[DischargeDateTime]
      ,[Dispo]
      ,[DateOfBirth]
      ,[DiagnosisCode]
      ,[DiagnosisDescription]
  FROM [azADT].[dbo].[v_VST601] (nolock)
where complaint in ('back pain','backache', 'lower-back pain')) as compl
left join
(SELECT PatientMedicalRecordNumber, EID
 FROM [azADT].[dbo].[v_PID601_IP_OP] (nolock)) as PID
on PID.EID = compl.EID )) as MRNS
join
(select * from
		[azADT].[dbo].[v_PID601_IP_OP] (nolock)) as PID2
on PID2.PatientMedicalRecordNumber = MRNS.PatientMedicalRecordNumber) AS MRNFIN
inner join
(SELECT *
  FROM azDIC.dbo.v_OBX_TXT (nolock)
  where tbl = 603) AS C
on C.EID = MRNFIN.EID
left join (SELECT *
  FROM [azADT].[dbo].[v_VST601] (nolock)) cmplnt
on MRNFIN.EID = cmplnt.EID
order by PatientMedicalRecordNumber, ObservationDateTime
"""

server = '10.130.172.93'
adt_connection = mssql_connect(server=server, database="azADT")
local_connection = create_engine("mssql://pandas:p4nd4s@MI2-BAH-DSAB02\SQLEXPRESS/Dictation?driver=SQL+Server")

def create_sampledicts(searchterms):
    df = pd.read_sql_table('Tbl603', local_connection)
    smaller_df = df[0:1000]
    print "Looking for sample docs that contain relevant terms."
    smaller_df['bp_ivdu'] = smaller_df.result.apply(lambda x: annotate_one(x,list(searchterms))['matchedTerms'])
    smaller_df['score'] = smaller_df.bp_ivdu.apply(lambda x: len(smaller_df.bp_ivdu[x]))
    sample = smaller_df.sort('score', ascending=False)[0:100]
    sample[['idx','result']].to_sql('sampledicts', local_connection, if_exists='replace', index=False)
    print "Done writing sample of %i records to DB table sampledicts" %len(sample)


def create_caselevel(n_patients):
    '''
    Create patient level case series deidentified data set
    Args:
        n_patients:

    Returns:

    '''
    df = pd.read_sql(case_level_select_script %n_patients, adt_connection)
    print "Finished data load from Amalga"
    df['mrn_masked'] = pd.factorize(df.PatientMedicalRecordNumber)[0]
    df['eid_masked'] = pd.factorize(df.EID)[0]
    df['fon_masked'] = pd.factorize(df.FillerOrderNumber)[0]
    df = df.drop(['EID', u'FillerOrderNumber',
                  u'PatientMedicalRecordNumber',
                  u'usecase','InstitutionName',
                  u'UniversalServiceIdentifier'],1)
    print "Loaded dictations for %i patients." %len(df.groupby('mrn_masked'))
    print "Started deidentification."
    df['cleantext'] = df.Result.apply(deid.deidentify)
    df = df.drop(['Result'],1)
    print ".........................Completed deidentification."

    #Write to DB
    df.to_sql('caselevel', local_connection, if_exists='replace', index=False)
    print "Done writing db table caselevel"


create_caselevel(50)
