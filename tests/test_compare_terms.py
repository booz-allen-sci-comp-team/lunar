import nose

import sys
sys.path.append('../')
import analyze.compare_terms


class TestDataQuality(object):
    """
    Run all tests by running
    nosetests ./
    from the test directory
    """
    def setUp(self):
        self.crit_term_dict = {"crit1":["cui1", "cui2", "cui3"], "crit2": ["cui2"], "crit3": ["cui4", "cui5"],
                               "crit4":["cui10"]}
        self.term_list = ["cui2", "cui4"]

    def test_get_critical_overlap(self):
        frac_dict = analyze.compare_terms.get_critical_overlap(self.crit_term_dict, self.term_list)
        nose.tools.assert_almost_equal(frac_dict["crit1"], 1/3.0)
        nose.tools.assert_almost_equal(frac_dict["crit2"], 1.0)
        nose.tools.assert_almost_equal(frac_dict["crit3"], 0.5)
        nose.tools.assert_almost_equal(frac_dict["crit4"], 0.0)


