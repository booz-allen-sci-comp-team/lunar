import nose
from bs4 import BeautifulSoup

import sys
sys.path.append('../')
from metamap import metamaplib



class TestDataQuality(object):
    """
    Run all tests by running
    nosetests ./
    from the test directory
    """
    def setUp(self):
        self.mri_brain_soup = BeautifulSoup(open("../radiology_sample_output/MRI-Brain-2"), from_encoding='utf-8')

    def test_negation(self):
        negation_tag = self.mri_brain_soup.find("negations").find("negation")
        neg = metamaplib.Negation(negation_tag)

        nose.tools.assert_equal(len(neg.neg_concepts), 2)
        nose.tools.assert_equal(neg.neg_concepts[0]["neg_concept_cui"], "C3278923")

        nose.tools.assert_equal(neg.neg_trigger, "without")
        nose.tools.assert_equal(neg.neg_type, "nega")

        report = metamaplib.Report(self.mri_brain_soup)
        nose.tools.assert_equal(len(report.negations), 5)
        nose.tools.assert_equal(report.negations[2].neg_trigger, "no")