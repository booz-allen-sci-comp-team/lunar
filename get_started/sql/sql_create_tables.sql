CREATE TABLE sem_types (tui varchar(4), abbrev varchar(10), name varchar(80));

CREATE TABLE utterance (uid SERIAL, utt_text text, PRIMARY KEY(uid));

CREATE TABLE phrase (pid SERIAL, phr_text text, PRIMARY KEY(pid));

CREATE TABLE mapping (mid SERIAL, PRIMARY KEY(mid));

CREATE TABLE candidate (cid serial, cui varchar(15), matched varchar(80), preferred text, semtype varchar(10), PRIMARY KEY(cid));

CREATE TABLE rel_utt_phr (uid INTEGER, pid INTEGER, PRIMARY KEY(uid, pid));

CREATE TABLE rel_phr_map (pid INTEGER, mid INTEGER, map_score INTEGER, PRIMARY KEY(pid, mid));

CREATE TABLE rel_map_can (mid INTEGER, cid INTEGER, can_score INTEGER, PRIMARY KEY(mid, cid));

CREATE TABLE rel_rep_utt (report_id INTEGER, uid INTEGER, PRIMARY KEY(report_id, uid));

CREATE TABLE reports (report_id SERIAL, eid text NOT NULL, PRIMARY KEY(report_id));

CREATE TABLE negation (nid SERIAL, neg_type varchar(80), neg_trigger varchar(80), neg_concepts jsonb, PRIMARY KEY(nid));

CREATE TABLE rel_rep_neg (report_id INTEGER, nid INTEGER, PRIMARY KEY(report_id, nid));
