DROP TABLE utterance;
DROP TABLE phrase;
DROP TABLE mapping;
DROP TABLE candidate;
DROP TABLE rel_utt_phr;
DROP TABLE rel_phr_map;
DROP TABLE rel_map_can;
DROP TABLE rel_rep_utt;
DROP TABLE reports;
DROP TABLE negation;
DROP TABLE rel_rep_neg;
DROP TABLE sem_types;
