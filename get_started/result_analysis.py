'''
analyze stored results, enter into data base
'''
import pickle
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import matplotlib.pyplot as plt
import pginterface
import pandas as pd

def plot_roc(Y, y_prob, plot_title='PE Critical Result Prediction: AdaBoost Model', plot=True):
    fpr, tpr, thresholds = roc_curve(Y, y_prob)
    roc_auc = auc(fpr, tpr)
    if plot:
        print "plotting"
        plt.plot(fpr, tpr, label='ROC Curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(plot_title)
        plt.legend(loc='lower right')
        plt.plot(fpr, tpr)
        plt.show()

def adj_decision_criterion(df, theta):
    df['prediction_bin'] = (df['prediction_cont'] > theta).astype(int)
    return df

file = 'results_pe.pickle'
with open('results_pe.pickle', 'r') as f:
    results = pickle.load(f)
print classification_report(results['actual'], results['pred'])
print "Writing to Oracle Table pe_results"
pginterface.store_model_results(results.values, append=False)
pginterface.pg_conn.close()
print "Writing flat file"
results.to_csv('results_pe.txt', index=False, sep="|")

fp = results[(results['pred'] == 1) & (results['actual'] == 0)]
pd.crosstab(results['actual'], results['pred'])
results['pred_adj'] = results['prob'].apply(lambda x: x > .5)
pd.crosstab(results['actual'], results['pred_adj'])


results[(results.prob > 1.0) & (results.actual==0)].to_excel('bad_fp.xls')







