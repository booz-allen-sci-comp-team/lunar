
"""
Given a text string document, extract the following feature types:
1) unigrams (normalized, i.e. lemmas)
2) bigrams
3) verb tense
4) lemma+part of speech
5) UMLS CUI+concept
The output is a string of space-delimited features, with the understanding that it can be passed as a document to
 sklearn...CountVectorizer(...,stopwords='english')

Critical dependencies:
-Stanford CoreNLP Folder (http://nlp.stanford.edu/software/corenlp.shtml). Tested under CoreNLP 3.5.2.
-$CORENLP variable defines CoreNLP folder location
-NLTK for bigram generation
-Pandas for formatting and filtering
"""
import string
import time
import re
import sys
import inspect
import os
import os.path
import re
import warnings
import pandas as pd
import subprocess
from xml.dom.minidom import parse, parseString
from nltk import bigrams
import pandas as pd
#import corenlp




# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from metamap.metamap_interface import MetamapInterface



warnings.filterwarnings("ignore")

penn_tree_bank_parse_cd = dict([('VBD', 'VERB_TENSE_PAST'),
                                ('VBG', 'VERB_TENSE_PRES'),
                                ('VBN', 'VERB_TENSE_PAST'),
                                ('VBP', 'VERB_TENSE_PRES'),
                                ('VBZ', 'VERB_TENSE_PRES')])
#start MetaMap
mm = MetamapInterface()
if mm.metamapstr("tylenol") :
    print "MetaMap started."
else:
    print "MetaMap test failed. Exiting now."
    sys.exit()
# #
# # start CoreNLP
# cnlp = corenlp.StanfordCoreNLP()
# if cnlp.isalive():
#     print "Core NLP Started."
# else:
#     print "Failed to start CoreNLP"
#     sys.exit(0)


def get_target_cuis(filename):
    """
    Given a text file with a list of terms, use MetaMap to get a set of CUI's
    :param filename:
    :return: set of UMLS CUI's associated with a target concept
    """
            # add custom vocabulary match flags
    with open(filename, 'r') as f:
        lines = f.readlines()
    df = pd.DataFrame([(i, mm.ade(mm.metamapstr(i.strip()))) for i in lines])
    return set([k[0]['cui'] for k in df[1].values if type(k) == list and len(k)>0])


customvocab = 'pe.txt'
tclist = get_target_cuis(customvocab)
#get verbatim matches
# with open(customvocab, 'r') as f:
#         lines = f.readlines()
# verbatim_terms = [l.strip() for l  in lines]
# print "loaded verbatim match terms"




# Make sure to add $CORENLP path as Stanford CoreNLP folder
def nlp_feature_pipeline(doc, verbose=False, use_metamamp=False):
    """Converts a text document (string) to a space delimited list of features

    :type use_metamamp: bool
    :rtype : string
    :param doc: a string containing raw text
    :param verbose: print detailed output, default is False
    :returns: string space delimited list of features
    """
    try:
        os.remove('temp.txt.xml')
    except:
        pass
    tempfile='temp.txt'
    with open(tempfile, 'w') as f:
        f.write(doc)
    arg = "java -mx1g -cp \"$CORENLP/*\" edu.stanford.nlp.pipeline.StanfordCoreNLP " \
            "-annotators tokenize,ssplit,pos,lemma -file %s" %tempfile
    #arg = "java -mx1g -cp \"$CORENLP/*\" edu.stanford.nlp.pipeline.StanfordCoreNLP " \
    #       "-annotators tokenize,ssplit,pos,lemma -file %s" %tempfile
    p = subprocess.Popen(arg, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
    wait_time = 0
    while not os.path.exists('temp.txt.xml'): #Wait for CoreNLP to get done
        time.sleep(1)
        wait_time = wait_time + 1
        if wait_time > 30:
            print "*************************ERROR: Timeout on CoreNLP Processing...**************************8"
            break
        sys.stdout.write(".\r")
    dom = parse('temp.txt.xml')
    words = []
    lemmas = []
    partsofspeech = []
    for sentence in dom.getElementsByTagName('sentence'):
        for token in sentence.getElementsByTagName('token'):
            for word in token.getElementsByTagName('word'):
                words.append(str(word.firstChild.toxml()))
            for lemma in token.getElementsByTagName('lemma'):
                lemmas.append(str(lemma.firstChild.toxml()))
            for pos in token.getElementsByTagName('POS'):
                partsofspeech.append(str(pos.firstChild.toxml()))
    df = pd.DataFrame(zip(words,lemmas, partsofspeech), columns=['Text', 'Lemma','PartOfSpeech'])
    df['TextLower'] = df['Text'].apply(lambda x: x.lower())
    df['Lemma'] = df['Lemma'].apply(lambda x: x.lower())
    df['vtense'] = df['PartOfSpeech'].map(penn_tree_bank_parse_cd)
    df_clean = df[(df['TextLower'].str.len() > 1) & ~df['PartOfSpeech'].isin(['CD', '-RRB-', '-LRB-', 'SYM'])][
        ~df['TextLower'].isin(list(string.punctuation))]
    df_clean['token_pos'] = df_clean['Lemma'] + "_" + df_clean['PartOfSpeech']
    df_final = df_clean[['Lemma', 'vtense', 'token_pos']]
    df_final.columns = ['lemma', 'vtense', 'tokenpos']
    bgr = bigrams(list(df_clean['Text'].values))
    bigram_feats = [bg[0].lower() + "_" + bg[1].lower() + "[2-gram]" for bg in bgr]

   # tc_verbatim_match_list = [w for w in words if w in verbatim_terms]+[bg for bg in bgr if bg in verbatim_terms]

    if use_metamamp:
       mmi = mm.metamapstr(doc)
       mappings = mm.ade(mmi)
       umls_concepts = ['UMLS_CUI_' + e['cui'] + '_' +
                     e['umls_term'].translate(string.maketrans("",""),
                                                             string.punctuation).replace(" ", "_")
                     for e in mappings]
       tc_match_list = ['Target_Concept_MM_Match' for e in mappings if e['cui'] in tclist]
    else:
        umls_concepts=[]
        tc_match_list=[]

    combined_features = (
        "{0} {1} {2} \n".format(" ".join(" ".join(map(str, l)) for l in df_final.values), " ".join(bigram_feats), " ".join(umls_concepts))).replace("nan", '').strip()
    return combined_features


def umls_features(doc):
    mmi = mm.metamapstr(doc)
    mappings = mm.ade(mmi)
    umls_concepts = [e['cui'] for e in mappings]
    #combined_features = (
    #    "{0}\n".format(" ".join(umls_concepts))).replace("nan", '').strip()
    return umls_concepts



if __name__ == '__main__':
    #with open('sampledata/radiology/findings.txt', 'r') as f:
    #    sample = f.read()
    sample = 'No CT evidence for pulmonary embolism.'

    start = time.time()
    result = nlp_feature_pipeline(sample, verbose=False, use_metamamp=True)
    print result
    timer = round(time.time() - start,2)
    print "Done with document\n~{0:d} tokens in document; {1:d} features extracted; {2:f} tokens per sec." \
        .format(len(sample.split(" ")), len(result.split(" ")), len(sample.split(" ")) / float(timer))

