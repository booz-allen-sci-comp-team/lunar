import pandas as pd
import pickle
import sys


# report = results.ix['8560992']['report_text']
# fpreports = results[(results.prob > 1.0) & (results.actual==0)]
# sections = [s[1:] for s in  [r.split(":") for r in report.split("\n\n\n")] if s[0].strip() in ['IMPRESSION']]

def impression_string(doc):
    try:
        impstr = [s[1] for s in [r.split(":") for r in doc.split("\n\n\n")] if
                         s[0].strip() in ['IMPRESSION']][0].strip().replace("ICD","")
    except:
        print "error parsing impression section"
        impstr = doc
    return impstr



from sklearn.feature_extraction.text import CountVectorizer
import psycopg2
from nlp_features import nlp_feature_pipeline

print "Getting syntactic features"
conn_str = 'dbname=msdb user=postgres password=boozah1!'
pg_conn = psycopg2.connect(conn_str)
df = pd.read_sql('select * from results_pe', pg_conn)
df['report_text'] = df['report_text'].apply(lambda x: impression_string(x))
corpus = df['report_text'].values
count = 1
count_vectorizer = CountVectorizer()
flist = []
for report in corpus:
    try:
        print "Working on report %s of %s" % (count, len(corpus))
        flist.append(nlp_feature_pipeline(report, use_metamamp=True))
    except:
        print "Failed to extract syntactic features."
        flist.append("None")
        sys.exit()
    count += 1
print "...Done extracting text features..."
with open ('textfeatures.pickle','wb') as f:
    pickle.dump(flist, f)
count = count_vectorizer.fit_transform(flist)