"""
Collect data around case series for a timeline display

"""

import pickle
import psycopg2
import numpy as np
import pandas as pd

conn_str = 'dbname=msdb user=postgres password=boozah1!'
pg_conn = psycopg2.connect(conn_str)
df = pd.read_sql('select * from rad_data_flags2', pg_conn)
df.index=df.eid
print "finished loading data"

filter = (df.groupby(['eid']).count() > 1)['fillerordernumber']
df_cases_indeces= df.groupby(['eid']).count()[list(filter.values)].index
df_cases = df.ix[df_cases_indeces]
df_cases = df_cases.sort(['eid', 'observationdatetime'])
debug_case = df_cases.ix['002db68f-0d49-42e8b31b-ccd9-11e4-b9fa-e61f1356ea0f']

