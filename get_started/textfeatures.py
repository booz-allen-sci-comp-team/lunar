
import os
import sys
import inspect

import re
import numpy as np
import nltk
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from nlp_features import *
from nltk.stem import PorterStemmer

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)



#import load.load_processed_data as loader
#import load.rawdata


class TextFeatures(object):
    def  __init__(self, corpus, pg_conn=None):
        #__init__(self, eid_fillerorder_filepath, pg_conn=None):#
       # self.eid_fillerorder_filepath = eid_fillerorder_filepath

        """

        :rtype : object
        """
        self.count_matrix = None
        self.tfidf_matrix = None
        self.vocab = None
        self.corpus = corpus

        #self.pg_conn = pg_conn

        #self.set_corpus()

    def set_corpus(self):
        #raw_data_loader = load.rawdata.RawData(self.eid_fillerorder_filepath, pg_conn=self.pg_conn) # list of report texts
        #corpus = raw_data_loader.load_report_text()
        """

        :rtype : object
        """
        #corpus = self.corpus
        print "Setting test corpus"
        #self.corpus = corpus

    def set_impression_corpus(self):
        print "Parsing impression section. Should be done BEFORE removing ICD-9 lines"
        if self.corpus is None:
            self.set_corpus()

        def find_impression(report_text):
            section_start = report_text.find("IMPRESSION:") + len("IMPRESSION:")
            section_stop = report_text.find("ICD-9:")
            impression_text = report_text[section_start:section_stop]
            return impression_text

        impressions = []
        for report in self.corpus:
            impression = find_impression(report)
            impressions.append(impression)

        print """Setting corpus to be only the impression section of reports!
                    Full text corpus is no longer saved."""
        self.corpus = impressions

    def remove_line_containing(self, input_string, remove_string):
        return re.sub(".*" + remove_string + ".*\n?", "", input_string)

    def remove_icd9_line(self):
        if not self.corpus:
            self.set_corpus()
        num_reports = len(self.corpus)
        print "Removing all lines which contain ICD9 from report corpus"
        for i in range(num_reports):
            report = self.corpus[i]
            self.corpus[i] = self.remove_line_containing(report, "ICD")

    def remove_critical_findings_line(self):
        if not self.corpus:
            self.set_corpus()
        num_reports = len(self.corpus)
        print "Removing all lines which contain 'Critical findings, first recognized' from report corpus"
        for i in range(num_reports):
            report = self.corpus[i]
            self.corpus[i] = self.remove_line_containing(report, "Critical findings, first recognized")

    def remove_were_communicated_line(self):
        if not self.corpus:
            self.set_corpus()
        num_reports = len(self.corpus)
        print "Removing all lines which contain 'were communicated to' from report corpus"
        for i in range(num_reports):
            report = self.corpus[i]
            self.corpus[i] = self.remove_line_containing(report, "were communicated to")


    def set_count_matrix(self, max_features=None):
        if not self.corpus:
            self.set_corpus()

        # Optional vocab - list of strings
        #count_vectorizer = CountVectorizer(vocabulary=vocab)

        # Convert corpus to sparse matrix of count features
        count_vectorizer = CountVectorizer(tokenizer=self.tokenize_and_stem, max_features=max_features)
        count_matrix = count_vectorizer.fit_transform(self.corpus)

        self.count_matrix = count_matrix
        self.vocab = count_vectorizer.get_feature_names()

    def tokenize_and_stem(self, string):
        tokenized = nltk.word_tokenize(string)
        stemmer = PorterStemmer()
        res = [stemmer.stem(w) for w in tokenized]
        return res

    def get_word_count_feature(self):
        word_count_feature = [len(nltk.word_tokenize(doc_string)) for doc_string in self.corpus]
        return word_count_feature, ['WORD_COUNT']

    def get_unigram_flags_feature(self):
        custom_vocab = ['if', 'further', 'evaluation', 'recommended', 'follow', 'recommend', 'consider',
                        'clinically', 'indicated']
        #if not self.corpus:
        #    self.set_corpus()

        # Convert corpus to sparse matrix of binary vocab flags
        count_vectorizer = CountVectorizer(vocabulary=custom_vocab, binary=True)
        count_matrix = count_vectorizer.fit_transform(self.corpus)

        return count_matrix.toarray(), count_vectorizer.get_feature_names()

    def get_ngram_counts(self, n=2, max_features=None):
        count_vectorizer = CountVectorizer(ngram_range=(1, n), max_features=max_features)
        ngram_matrix = count_vectorizer.fit_transform(self.corpus)
        ngram_vocab = count_vectorizer.get_feature_names()
        #print ngram_vocab
        return ngram_matrix, ngram_vocab

    def get_custom_bigrams(self):
        """
        Get flags (not counts) for custom bigram list
        :return: Sparse matrix of flags
        """
        bigram_vocab = ["further evaluation", "for further", "follow up", "is recommended", "in months",
                        "evaluation with", "if clinically", "could be", "if there", "be obtained"]

        count_vectorizer = CountVectorizer(ngram_range=(2, 2), vocabulary=bigram_vocab, binary=True)
        flag_matrix = count_vectorizer.fit_transform(self.corpus)
        return flag_matrix, count_vectorizer.get_feature_names()

    def get_syntactic_features(self, usemm=False):
        print "Getting syntactic features"
        count = 1
        count_vectorizer = CountVectorizer()
        flist = []
        for report in self.corpus:
            try:
                print "Working on report %s of %s" % (count, len(self.corpus))
                flist.append(nlp_feature_pipeline(report, use_metamamp=usemm))
            except:
                print "Failed to extract syntactic features."
                flist.append("None")
                sys.exit()
            count += 1
        print "...Done extracting text features..."
        with open ('textfeatures.pickle','wb') as f:
            pickle.dump(flist, f)
        count = count_vectorizer.fit_transform(flist)
        return count, count_vectorizer.get_feature_names()


    def get_custom_trigrams(self):
        trigram_vocab = ["for further evaluation", "further evaluation with", "if clinically indicated",
                         "helpful for further", "be helpful for", "could be obtained", "clinical concern for",
                         "if there is"]

        count_vectorizer = CountVectorizer(ngram_range=(3, 3), vocabulary=trigram_vocab, binary=True)
        flag_matrix = count_vectorizer.fit_transform(self.corpus)
        return flag_matrix, count_vectorizer.get_feature_names()

    def print_corpus_counts(self):
        dist = np.sum(self.count_matrix.toarray(), axis=0)
        print "Counts for entire corpus are: "
        for tag, count in zip(self.vocab, dist):
            print count, tag

if __name__ == "__main__":
    #eid_fillerorder_filepath = "tmp"
    # Example no longer works, need to create test filepath
    demo = ['There is no appreciable change in the '
            'appearance of the chest since the last '
            'chest xray at 5:41 PM on [**2011-10-08**]. '
            'There is no pneumothorax. There is a good '
            'deal of opacification at the left base. '
            'The heart is normal in size. There is '
            'pulmonary venous cephalization '
            'and probably a small effusion at the '
            'right base as well. ETT, RIJ line, right '
            'subclavian line, and NGT are all unchanged.',
            'There has been placement of a right sided subclavian '
            'catheter, with the tip in the proximal SVC. Additionally, '
            'there is a left sided PICC line with the tip in the distal SVC. '
            'There has been interval intubation with endotracheal tube 6.5 cm '
            'from the carina. The heart is enlarged, with increased pulmonary vascular markings as '
            'well as hilar haziness. Additionally, there is bilateral '
            'atelectasis and bilateral small effusions. '
            'There are no focal opacities or pneumothorax.']

    text_features = TextFeatures(corpus=demo) #eid_fillerorder_filepath)
    print text_features.get_syntactic_features()

    #text_features.print_corpus_counts()

    wcf = text_features.get_word_count_feature()
    print wcf

    unigram_flags = text_features.get_unigram_flags_feature()
    print unigram_flags
    #text_features.get_ngram_counts()

    bi_mat = text_features.get_custom_bigrams()
    tri_mat = text_features.get_custom_trigrams()
    print bi_mat, tri_mat
