import os
import sys
import inspect
import pickle
import numpy as np
# import matplotlib.pyplot as plt

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import normalize
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import classification_report
from sklearn.decomposition import PCA
from sklearn import preprocessing

import argparse
import pandas as pd
import re

# import compare_terms
# from metamapfeatures import MetamapFeatures
import textfeatures
#
# testcorpus = ['There is no appreciable change in the '
#         'appearance of the chest since the last '
#         'chest xray at 5:41 PM on [**2011-10-08**]. '
#         'There is no pneumothorax. There is a good '
#         'deal of opacification at the left base. '
#         'The heart is normal in size. There is '
#         'pulmonary venous cephalization '
#         'and probably a small effusion at the '
#         'right base as well. ETT, RIJ line, right '
#         'subclavian line, and NGT are all unchanged.',
#         'There has been placement of a right sided subclavian '
#         'catheter, with the tip in the proximal SVC. Additionally, '
#         'there is a left sided PICC line with the tip in the distal SVC. '
#         'There has been interval intubation with endotracheal tube 6.5 cm '
#         'from the carina. The heart is enlarged, with increased pulmonary vascular markings as '
#         'well as hilar haziness. Additionally, there is bilateral '
#         'atelectasis and bilateral small effusions. '
#         'There are no focal opacities or pneumothorax.']
#

# with open('sampledata/radiology/findings.txt', 'r') as f:
#     testcorpus = f.readlines()
# testcorpus = testcorpus[0:2]






# load train and test data
inputfilepath = "../radsdata/rad_data_flags.csv"
df = pd.DataFrame.from_csv(inputfilepath, header=None)
df.head()
colnames = [
    'fillerordernumber',
    'critical',
    'pneumothorax',
    'pneumonia',
    'pulmonaryemb',
    'notified',
    'discussedwith',
    'universalservicename',
    'observationdatetime',
    'reporttext',
    'icd']

df.columns = colnames
df_clean = df.drop_duplicates().dropna()  # 53771 rows
df_clean['target'] = df_clean.icd.apply(lambda x: bool(re.search('415.', x))).astype(int)  # 92 targets


df_clean = pd.concat([df_clean[df_clean.target==1], df_clean[df_clean.target==0].sample(92)],0) #take all targets and select equal n controls
df_clean = pd.concat([df_clean[df_clean.target == 1], df_clean[df_clean.target == 0].sample(92)],
                     0)  # take all targets and select equal n controls

df_numeric = df_clean[['critical', 'pneumothorax', 'pneumonia',
                       'pulmonaryemb']].convert_objects(convert_numeric=True)
df_text = list((df_clean['universalservicename'] + " " + df_clean['reporttext']).values)

df_outcome = list(df_clean.target.values)


# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)


def get_numeric_features(mat):
    scaled = preprocessing.scale(mat)
    return scaled


# import load.load_processed_data as loader
# import load.rawdata

def get_all_text_features(corpus):
    ######
    # Set basic metapmap feature matrices
    ######
    print "STATUS: Connecting to postgres"

    # print "STATUS: Setting metamap features"
    # mmFeatures = MetamapFeatures(report_id_list, pg_connection)
    # print "STATUS: Setting CUI count matrix"
    # mmFeatures.set_cui_count_matrix(max_features=200)
    print "Shape of count matrix is: "
    # print np.shape((mmFeatures.count_matrix))

    # Optionally, use tf-idf matrix instead of counts [converted from count matrix, should have same size)
    # mmFeatures.set_tfidf_matrix()
    # mm_tfidf_features = mmFeatures.tfidf_matrix.toarray()

    ######
    # Various options for reducing feature matrix - uncomment for other choices
    ######
    print "STATUS: Reducing metamap features"
    # mm_count_features = mmFeatures.pca_feature_reduction(n_components=0.99, type="count")
    # mm_count_features = mmFeatures.rbm_feature_reduction(type='count', n_components=256)
    # mm_count_features = mmFeatures.count_matrix.toarray()  # full count matrix

    # Get metamap match features
    print "STATUS: Getting metmap match features"
    # lung_dict = loader.get_critical_dict_from_file()
    # crit_compare_features = compare_terms.get_comparison_features(report_id_list, lung_dict, pg_connection)

    ######
    # Get raw text features
    #####
    print 'STATUS: Getting raw text features'
    text_features = textfeatures.TextFeatures(corpus)  # eid_fon_filepath, pg_conn=pg_connection)
    text_features.remove_icd9_line()
    # text_features.set_count_matrix(max_features=200)

    unigram_features, unigram_features_labels = text_features.get_unigram_flags_feature()
    bigram_features, bigram_features_labels = text_features.get_custom_bigrams()  # .toarray()
    trigram_features, trigram_features_labels = text_features.get_custom_trigrams()  # .toarray()
    word_count_feature, word_count_features_labels = text_features.get_word_count_feature()
    syntactic_features, syntactic_features_labels = text_features.get_syntactic_features()
    ###
    # Get impression section features
    ###
    # print "STATUS: Getting impression section (raw text) features"
    # impression_text_features = TextFeatures(eid_fon_filepath, pg_conn=pg_connection)
    # impression_text_features.set_impression_corpus()
    # impression_text_features.set_count_matrix(max_features=200)
    # Various options for reducing raw count feature matrix - uncomment for other choices
    print "STATUS: Reducing metamap features"
    # impression_raw_count_features = impression_features.pca_feature_reduction(n_components=0.99, type="count")
    # impression_raw_count_features = impression_features.rbm_feature_reduction(type='count', n_components=256)
    # impression_raw_count_features = impression_text_features.count_matrix.toarray()  # full count matrix
    #
    # impression_unigram_features = impression_text_features.get_unigram_flags_feature()
    # impression_bigram_features = impression_text_features.get_custom_bigrams().toarray()
    # impression_trigram_features = text_features.get_custom_trigrams().toarray()
    # impression_word_count_feature = impression_text_features.get_word_count_feature()
    #
    # # Additional optional counts of all bi/tri grams - unigram already in count matrix
    # impression_bigram_count_features = text_features.get_ngram_counts(n=2, max_features=200).toarray()
    # impression_trigram_count_features = text_features.get_ngram_counts(n=3, max_features=200).toarray()


    # Append all features together
    all_features = []

    # Metamap features (counts matrix, tfidf matrix, critical comparison feature
    # all_features += [mm_count_features]
    # all_features += [mm_tfidf_features]
    # all_features += [crit_compare_features]

    # Raw text features- NO RAW COUNTS
    all_features += [unigram_features, bigram_features.toarray(),
                     trigram_features.toarray()]  # custom uni/bi/tri gram flags
    # all_features += [bigram_count_features, trigram_count_features]  # bi/trigram counts
    all_features += [np.asarray([[l] for l in word_count_feature])]

    # Impression section features - including raw count
    # all_features += [impression_raw_count_features]
    # all_features += [impression_unigram_features, impression_bigram_features, impression_trigram_features]
    # all_features += [impression_word_count_feature]
    # all_features += [impression_bigram_count_features, impression_trigram_count_features]
    # syntactic features
    all_features += [syntactic_features.toarray()]

    return all_features


def score_set(model, X, Y, plot=False, plot_title='Critical Result Prediction'):
    y_predicted = model.predict(X)
    print "Precision is: "
    print precision_score(Y, y_predicted)
    print "Recall is:"
    print recall_score(Y, y_predicted)

    # if plot:
    #     # ROC curve
    #     y_prob = model.decision_function(X)
    #     # y_prob = model.predict_proba(X)[:, 1] # For naive bayes, etc
    #     fpr, tpr, thresholds = roc_curve(Y, y_prob)
    #     roc_auc = auc(fpr, tpr)
    #     print "plotting"
    #     plt.plot(fpr, tpr, label='ROC Curve (area = %0.2f)' % roc_auc)
    #     plt.plot([0, 1], [0, 1], 'k--')
    #     plt.xlim([0.0, 1.0])
    #     plt.ylim([0.0, 1.05])
    #     plt.xlabel('False Positive Rate')
    #     plt.ylabel('True Positive Rate')
    #     plt.title(plot_title)
    #     plt.legend(loc='lower right')
    #     plt.plot(fpr, tpr)
    #     plt.show()
    #     # print thresholds


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help='filepath for pipe separated eid, fillerorder list')
    parser.add_argument('-o', '--outfile', nargs='?', help='output file path to save model', default='model.p')
    parser.add_argument('-c', '--code', help='ICD9 code for outcome', default='415')
    parser.add_argument('-p', '--plot', action='store_true', help='Plot ROC curves')
    parser.add_argument('-s', '--score', action='store_true', help='Score model that has been saved to disk')
    parser.add_argument('-f', '--file', help='filepath for flat text input, assumed to be a csv')
    args = parser.parse_args()
    try:
        with open('fcount_mat.pickle', 'r') as f:
            X = pickle.load(f)
        print "Loaded feature counts from disk"
        with open('outcomes.pickle', 'r') as f:
            Y = pickle.load(f)
        print "Loaded outcomes from disk"
    except:
        # # pg_connection = loader.connect_postgres()
        #
        # # Set training set and outcome column
        # target_col_name = 'icd'
        #
        # eid_fillerorder_filepath = args.infile  # eid, fillerordernumber combos
        #
        # Instantiate raw data object to get report id list corresponding to eid/fillordernumber list
        print "Creating feature count and outcome vectors"
        #print "STATUS: Loading report id list from eid, fillerordernumber list"
        # raw_data = load.rawdata.RawData(eid_fillerorder_filepath, pg_connection)
        # report_id_list = raw_data.get_report_id_list()

        # get features from above fcn and split into train/test
        print "STATUS: Getting all features for model"
        # X = get_all_features(eid_fillerorder_filepath, report_id_list, pg_connection)
        all_features = get_all_text_features(df_text)
        all_features += [get_numeric_features(df_numeric)]
        non_empty_features = []
        for feature_matrix in all_features:
            if len(np.shape(feature_matrix)) > 1 and (np.size(feature_matrix) != 0):
                non_empty_features.append(feature_matrix)
        all_features = np.hstack(non_empty_features)
        X = all_features
        with open('fcount_mat.pickle', 'wb') as f:
            pickle.dump(X, f)
        print "Done extracting feature counts. Saved to disk.\n Starting to train and test model"
        print "Shape of feature matrix is %s"
        print np.shape(X)

        # print "STATUS: Reducing features via pca"
        # pca = PCA(n_components=0.95)
        # X = pca.fit_transform(X)
        # print "Shape of reduced feature matrix is: "
        # print np.shape(X)

        # load outcome data
        print "STATUS: Loading outcome data from rad_data_flags"
        Y = df_outcome
        with open('outcomes.pickle', 'wb') as f:
            pickle.dump(Y, f)
        # Y = raw_data.load_target_vector(target_col_name)
        # Y = [int(args.code in icd9code) for icd9code in Y] # For icd9 label only
        # raw_data.close_pg_connection()
    # fit and score model
    print "Normalizing across features"
    X = normalize(X.astype('double'), axis=0)
    print "STATUS: Fitting and scoring model"
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=42)
    # regularization parameter
    C = 1
    # model = GaussianNB()
    model = LogisticRegression(C=C)
    model.fit(X_train, Y_train)

    print "*" * 10
    print "* Accuracy of model on training set is %f" % model.score(X_train, Y_train)
    print "*" * 10

    print "Scoring on training set"
    score_set(model, X_train, Y_train)

    print "*" * 10
    print "* Accuracy of model on test set is %f" % model.score(X_test, Y_test)
    print "*" * 10

    print "Scoring on test set"
    score_set(model, X_test, Y_test, plot=args.plot)
    Y_pred = model.predict(X_test)
    print classification_report(Y_test, Y_pred)
    # print sum(y)

    # pg_connection.close()

    # Save model
    with open(args.outfile, 'w') as outfile:
        pickle.dump(model, outfile)
