import argparse
import inspect
import os, sys

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join( os.path.split(inspect.getfile( inspect.currentframe() ))[0], '../..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

from metamap import ms_config

def parse_terms(input_file, output_file, metamap_path):
    with open(input_file) as f:
        for line in f:
            print line.strip()
            cmd = "echo \"{0}\" | {1}/bin/metamap --XMLf >> {2}".format(line.strip(), metamap_path, output_file)
            os.system(cmd)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help='Critical terms raw input file.')
    parser.add_argument('output_file', help='Critical terms output file. This should be a ".xml" file')
    args=parser.parse_args()
    metamap_path = ms_config.metamap_path
    output_file = args.output_file
    parse_terms(args.input_file, args.output_file, metamap_path)
