import csv
import getpass
#import psycopg2
import inspect, os, sys
import pandas as pd

# # Move one directory up and append to path if it is not there to import local packages
# project_root_dir = os.path.abspath(os.path.join( os.path.split(inspect.getfile( inspect.currentframe() ))[0], '../..'))
# if project_root_dir not in sys.path:
#     sys.path.insert(0, project_root_dir)
#
# # from metamap import postgres_conn as p
# #
# # pg_cur = None

#an alternative way -- write from a pandas dataframe
#from sqlalchemy import create_engine
#engine = create_engine('postgresql://postgres:pwd@localhost:5432/msdb')





# def insert_semtype_data(tui, abbrev, name):
#     query = """INSERT INTO sem_types(tui, abbrev, name) VALUES (%s,%s,%s)"""
#     pg_cur = pg_conn.cursor()
#     pg_cur.execute(query, (tui,abbrev,name))
#
# def read_subject_data(rows):
#     for row in rows:
#         #print row
#         insert_semtype_data(row[1], row[0], row[2])
#
# def load_semtypes(filename):
#     with open(filename, 'rb') as csvfile:
#         rows = csv.reader(csvfile, delimiter='|')
#         read_subject_data(rows)

if __name__ == '__main__':
    if (len(sys.argv) < 2):
        print 'Invalid arguments'
        sys.exit(0)
    #pg_conn = p.connect_postgres()
    #load_semtypes(sys.argv[1])
    df = pd.DataFrame.from_csv(sys.argv[1], sep="|", header=None, index_col=None)
    df.columns = ['tui','abbrev','name']
    #df.to_sql('sem_types', engine, if_exists='replace')
    #pg_conn.commit()
    #pg_conn.close()
    print 'Semantic types successfully stored.'
