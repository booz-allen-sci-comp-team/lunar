"""
Revised predictive modeling framework utilizing the SciKit Learn Pipeline and FeatureUnion functionality

"""
import pickle
import psycopg2
import numpy as np
import pandas as pd
import re
import os
import sys
import inspect
import argparse
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import classification_report
from sklearn.pipeline import FeatureUnion
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from nlp_features import nlp_feature_pipeline
from sklearn.ensemble import AdaBoostClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_curve
from sklearn.metrics import auc


# import matplotlib.pyplot as plt


def remove_line_containing(input_string, remove_string):
    return re.sub(".*" + remove_string + ".*\n?", "", input_string)


class ItemSelector(BaseEstimator, TransformerMixin):
    """For data grouped by feature, select subset of data at a provided key.

    Parameters
    ----------
    key : hashable, required
        The key corresponding to the desired value in a mappable.
    """

    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]


class TextStats(BaseEstimator, TransformerMixin):
    """Extract features from each document for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def get_feature_names(self):
        return ['length', 'num_sentences']

    def transform(self, records):
        return [{'length': len(text.split(" ")),
                 'num_sentences': text.count('.')}
                for text in records]


class NumericFeatures(BaseEstimator, TransformerMixin):
    """Extract features from each document for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def transform(self, records_df):
        return np.transpose(np.array(records_df['numeric']))


class UMLS_CUI(BaseEstimator, TransformerMixin):
    """Extract features from each document for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def transform(self, d):
        cui_mat = []
        counter = 0
        for r in d['report_id']:
            counter = counter + 1
            df_cui = pd.read_sql(
                'SELECT candidate.cui FROM candidate, rel_rep_utt, rel_utt_phr, rel_phr_map, rel_map_can '
                'WHERE rel_rep_utt.report_id=%s AND rel_utt_phr.uid=rel_rep_utt.uid AND '
                'rel_utt_phr.pid=rel_phr_map.pid AND rel_phr_map.mid = rel_map_can.mid AND '
                'rel_map_can.cid=candidate.cid;' % r, pg_conn)
            df_negated_cui = pd.read_sql(
                'SELECT neg_concepts FROM negation, rel_rep_neg WHERE rel_rep_neg.report_id=%s '
                'AND rel_rep_neg.nid = negation.nid;' % r, pg_conn)
            remove_negated = [str(list(c)[0]) for c in df_cui.values if c not in [str(k[0][0]['neg_concept_cui'])
                                                                                  for k in df_negated_cui.values]]
            cui_mat.append(remove_negated)
            print "Report ", str(counter), " of ", len(d), "--", str(
                len(remove_negated)) + " UMLS Concepts Retrieved.\t", \
                len(df_cui), "assertions", len(df_negated_cui), "negations."
        return [" ".join(list(row)) for row in cui_mat]


class HigherLevelTextFeatures(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self

    def transform(self, records):
        count = 1
        flist = []
        for report in records:
            print ("Working on report %s of %s" % (count, len(records)))
            try:
                nlp_feats = nlp_feature_pipeline(report)
                flist.append(nlp_feats)
                print len(nlp_feats.split(" ")), 'syntactic and semantic features extracted.'
            except:
                print ("Failed to extract syntactic features.")
                flist.append("None")
            count += 1
        print ("...Done extracting text features...")
        return flist


class Remove_ICD9_line():
    def fit(self, x, y=None):
        return self

    def transform(self, records_dict):
        num_reports = len(records_dict['reporttext'])
        print ("Removing all lines which contain ICD9 from report corpus")
        for i in range(num_reports):
            report = records_dict['reporttext'][i]
            records_dict['reporttext'][i] = remove_line_containing(report, "ICD")
        return records_dict

def df_clean(df):
    df_clean = df[~df['reporttext'].isnull()]
    # df_clean = df.drop_duplicates().dropna()  # 53771 rows
    df_clean = df_clean[~df_clean['icd'].isnull()]
    df_clean = df_clean.groupby('reporttext').first()  # 50398
    df_clean['reporttext'] = df_clean.index
    df_clean.index = df_clean['fillerordernumber']
    df_clean['pe_target'] = df_clean['icd'].apply(lambda x: bool(re.search('415.', x))).astype(int)
    df_numeric = df_clean[
        ['eid', 'critical', 'pneumothorax', 'pneumonia', 'pulmonaryemb', 'report_id']].convert_objects(
        convert_numeric=True)
    df_text = df_clean['universalservicename'] + " " + df_clean['reporttext']
    df_outcome = df_clean.pe_target
    df_text = df_text.to_frame('df_text')
    df_text.columns = ['reporttext']
    df_final = pd.concat([df_numeric, df_text, df_outcome], 1)
    df_final['fillerordernumber'] = df_final.index
    df_final = df_final.convert_objects(convert_numeric=True)  # to be used for final results

    # create data structure which will track inputs and results.
    # in future, this will be written to a database
    df_train_test = pd.concat([df_final[df_final.pe_target == 1],
                               df_final[df_final.pe_target == 0].sample(65)],
                              0)  # force equal numer of train and test instances
    # reports_train_test = df_train_test.to_dict(outtype='records')
    df_test_non_targets = df_final[df_final.pe_target == 0][0:10]  # 400 non target sample
    reports_test_non_targets = df_test_non_targets.to_dict(orient='records')
    # df_train_test['numeric'] = [df_train_test[v] for v in numeric_variables]
    df_Y = df_train_test['pe_target']  # Y
    df_X = df_train_test.drop('pe_target', 1)  # X
    X_train, X_test, Y_train, Y_test = train_test_split(df_X, df_Y, test_size=0.2, random_state=42)
    numeric_variables = ['critical', 'pneumonia', 'pneumothorax', 'pulmonaryemb']
    df_train = X_train.to_dict(orient='list')
    df_train['numeric'] = [df_train[v] for v in numeric_variables]
    df_test = X_test.to_dict(orient='list')
    df_test['numeric'] = [df_test[v] for v in numeric_variables]
    return df_final, df_train, df_test, X_train, X_test, Y_train, Y_test

def score_set(model, X, Y, plot=False, plot_title='PE Critical Result Prediction: AdaBoost Model'):
    y_predicted = model.predict(X)
    y_prob = model.decision_function(X)
    # y_prob = model.predict_proba(X)
    # print "Precision is: "
    # print precision_score(Y, y_predicted)
    # print "Recall is:"
    # print recall_score(Y, y_predicted)
    print (classification_report(y_predicted, Y))
    if plot:
        # ROC curve

        # y_prob = model.predict_proba(X)[:, 1] # For naive bayes, etc
        fpr, tpr, thresholds = roc_curve(Y, y_prob)
        roc_auc = auc(fpr, tpr)
        print "plotting"
        plt.plot(fpr, tpr, label='ROC Curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(plot_title)
        plt.legend(loc='lower right')
        plt.plot(fpr, tpr)
        plt.show()
        # print thresholds
    return y_predicted, y_prob


def print_top10(vectorizer, clf, class_labels):
    """Prints features with the highest coefficient values, per class"""
    feature_names = vectorizer.get_feature_names()
    for i, class_label in enumerate(class_labels):
        top10 = np.argsort(clf.coef_[i])[-10:]
        print("%s: %s" % (class_label,
                          " ".join(feature_names[j] for j in top10)))


custom_unigrams = ['if', 'further', 'evaluation', 'recommended', 'follow', 'recommend', 'consider',
                   'clinically', 'indicated']
custom_bigrams = ["further evaluation", "for further", "follow up", "is recommended", "in months",
                  "evaluation with", "if clinically", "could be", "if there", "be obtained"]
custom_trigrams = ["for further evaluation", "further evaluation with", "if clinically indicated",
                   "helpful for further", "be helpful for", "could be obtained", "clinical concern for",
                   "if there is"]

pipeline = Pipeline([
    ('remove_icd', Remove_ICD9_line()),
    ('features', FeatureUnion(transformer_list=[
        ('text_stats', Pipeline([
            ('selector', ItemSelector(key='reporttext')),  # returns a list of dicts
            ('stats', TextStats()),  # returns a list of dicts
            ('vect', DictVectorizer(sparse=False)),
        ])),
        ('cust_uni', Pipeline([
            ('selector', ItemSelector(key='reporttext')),
            ('vect', CountVectorizer(vocabulary=custom_unigrams, binary=True))])),
        ('cust_bi', Pipeline([
            ('selector', ItemSelector(key='reporttext')),
            ('vect', CountVectorizer(ngram_range=(2, 2), vocabulary=custom_bigrams, binary=True))])),
        ('cust_tri', Pipeline([
            ('selector', ItemSelector(key='reporttext')),
            ('vect', CountVectorizer(ngram_range=(3, 3), vocabulary=custom_trigrams, binary=True))])),
        ('hg_level', Pipeline([
            ('selector', ItemSelector(key='reporttext')),
            ('hl_extract', HigherLevelTextFeatures()),  # get UMLS concepts and grammar features
            ('vect', CountVectorizer())])),
        ('umls_cui', Pipeline([
            ('cui_extract', UMLS_CUI()),  # get UMLS concepts and grammar features
            ('vect', CountVectorizer())])),
        ('numeric_f', Pipeline([('numeric_features', NumericFeatures()),
                                ]))
    ])),
    # Use ADA Boost classifier on the combined features
    ('adaboost', AdaBoostClassifier())])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fit_mode', action='store_true', help='fit instead of train/test')
    args = parser.parse_args()

    # Move one directory up and append to path if it is not there to import local packages
    # project_root_dir = os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '..'))

    #    inputfilepath = "../radsdata/rad_data_flags.csv"

    conn_str = 'dbname=msdb user=postgres password=boozah1!'
    pg_conn = psycopg2.connect(conn_str)
    df = pd.read_sql('select * from rad_data_flags2', pg_conn)
    print "finished loading data"
    df_final, df_train, df_test, X_train, X_test, Y_train, Y_test = df_clean(df)
    print "prepared data for modeling"
    if args.fit_mode:
        with open('PE_AdaBoost_PARTIAL_N_104.model', 'r') as f:
            pipeline = pickle.load(f)
        print "*" * 20 + "Fit Mode" + "*" * 20
        df_sample = df_final
        df_Y = df_sample['pe_target']
        df_X = df_sample
        result_df = df_X[['eid', 'fillerordernumber', 'pe_target', 'reporttext']]
        result_df.columns = ['eid', 'fon', 'actual', 'report_text']
        df_X = df_X.drop('pe_target', 1)
        # df_Y = df_test_non_targets['pe_target'] #Y
        # df_X = df_test_non_targets.drop('pe_target',1) #X
        numeric_variables = ['critical', 'pneumonia', 'pneumothorax', 'pulmonaryemb']
        df_X_dict = df_X.to_dict(orient='list')
        df_X_dict['numeric'] = [df_X[v] for v in numeric_variables]
        print "getting predicted values"
        y_predicted = pipeline.predict(df_X_dict)
        print "getting decision function"
        y_prob = pipeline.decision_function(df_X_dict)
        print 'Setting decision cutoff at .2'
        y_predicted = list(np.array([yp > 0.2 for yp in y_prob]).astype(int))
        print "done"
        print (classification_report(df_Y, y_predicted))
        # y_pred_bin, y_pr  = score_set(pipeline, df_X_dict, df_Y)
        result_df['pred'], result_df['prob'] = y_predicted, y_prob
        with open('results_pe.pickle', 'w') as outfile:
            pickle.dump(result_df, outfile)
    else:
        print "*" * 20 + "Train-Test Mode" + "*" * 20
        pipeline.fit(df_train, Y_train)
        score_set(pipeline, df_test, Y_test)
        with open('PE_AdaBoost_PARTIAL_N_%s.model' % len(X_train), 'w') as outfile:
            pickle.dump(pipeline, outfile)
