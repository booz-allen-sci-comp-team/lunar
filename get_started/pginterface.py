import psycopg2, pandas as pd
conn_str = 'dbname=msdb user=postgres password=boozah1!'
pg_conn = psycopg2.connect(conn_str)
cur = pg_conn.cursor()


def store_model_results(d, append=False):
    '''

    :param d: array of values  that correspond to fields
    eid, fillerordernumber, report_id, model, prediction_bin, prediction_num
    :return:
    '''
    if not append:
        cur.execute("DROP TABLE IF EXISTS pe_results;")
        pg_conn.commit()
        cur.execute("CREATE TABLE pe_results "
                    "(eid varchar, fon varchar, "
                    "prob numeric, pred smallint, actual smallint, report_text text);")
        pg_conn.commit()
    query = """INSERT INTO pe_results(eid, fon, prob, pred, actual, report_text) VALUES (%s,%s,%s,%s,%s,%s);"""
    for j in d:
        cur.execute(query, j)
    pg_conn.commit()
    print len(d), 'record(s) inserted'
    cur.execute("""select count(*) from pe_results;""")
    print "table pe_results has", int(cur.fetchone()[0]), "record(s)"

if __name__ == "__main__":
    print "Demo mode"
    testdata = [('fcd2eb27-1265-231c5e55-ae13-11e4-b6bf-e61f1358464b', 8469707L, 0L,'RIBS, RIGHT',0L, -0.5231911189684404)]
    store_model_results(testdata, append=True)
    pg_conn.close()
