__author__ = 'sergeyblok'
import pandas as pd
from metamap.metamap_interface import MetamapInterface




mm = MetamapInterface() #init first just to get the custom NegEx rule location
custom_neg_rules_loc = "/".join(mm.METAMAP_BINARY.split("/")[:-2])+"/negex_triggers.pl"
mm = MetamapInterface(optionsStr="-N --negex --negex_trigger_file %s" %custom_neg_rules_loc)

text = "Uncomplicated pulmonary embolism.  High clot burden. " \
       "1.1 cm speculated nodule in the outer left breast.  Recommend mammography evaluation.  " \
       "Findings are worrisome for a breast cancer."
def check_negation(text):
    mmresult = mm.metamapstr(text)
    #df = pd.DataFrame(mmresult)
    return mm.ade(mmresult)

tests =[("no pulmonary embolism", 0),
            ("no acute pulmonary embol", 0),
            ("uncomplicated embol", 0),
            ("no thrombus",0),
            ("no evidence of", 0),
            ("no obstruction", 0),
            ("no acute",0),
            ( "no deep vein thromb",0),
            ("ivc filter",0),
            ("no pulmonary nodules", 0),
            ("no pulmonary", 0),
            ("no filling defect", 0),
            ("no pulmonary embolism", 0),
            ("no evidence of thrombus", 0),
            ("no evidence for pulmonary", 0),
            ("no evidence of ", 0),
            ('no filling defect', 0),
            ('no acute thromb', 0),
             ('low probability',0),
             ('no acute',0),
             ('resolving thrombus',0),
             ('no sufficient evidence of thrombus',0),
             ('not enough information to rule out thrombus or some edema',0)]

for t in tests:
    assert len(check_negation(t[0])) == t[1], "failed negation detection: %s" % t[0]