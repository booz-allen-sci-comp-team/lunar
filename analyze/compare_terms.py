import os
import sys
import inspect
import argparse

import pandas as pd

# Move one directory up and append to path if it is not there to import local packages
project_root_dir = os.path.abspath(os.path.join( os.path.split(inspect.getfile( inspect.currentframe() ))[0], '..'))
if project_root_dir not in sys.path:
    sys.path.insert(0, project_root_dir)

import load.load_processed_data as loader


def get_critical_overlap(critcal_term_dict, report_term_list):
    frac_dict = {}
    for crit_term in critcal_term_dict:
        crit_term_list = critcal_term_dict[crit_term]
        term_overlap = set(crit_term_list) & set(report_term_list)
        fraction_overlap = len(term_overlap)/float(len(crit_term_list))
        frac_dict[crit_term] = fraction_overlap

    return frac_dict

def get_exact_matches(frac_dict):
    all_terms_matched = [k for k, v in frac_dict.items() if v == 1.0]
    return all_terms_matched

def is_negation(crit_term_dict, crit_term, neg_list):
    """
    Determine if list of negated terms overlaps with critical term list
    :param crit_term_dict:
    :param crit_term:
    :param neg_list:
    :return:
    """
    term_overlap = set(crit_term_dict[crit_term]) & set(neg_list)
    if term_overlap:
        return True
    else:
        return False


def get_critical_matches(pg_conn, report_id):
    crit_term_dict = loader.get_critical_dictionary(pg_conn)
    report_term_list = loader.get_candidate_list(pg_conn, report_id)
    frac_dict = get_critical_overlap(crit_term_dict, report_term_list)
    matches = get_exact_matches(frac_dict)
    return matches

def is_critical_simple(pg_conn, report_id):
    """
    Most basic rules - return a report as falling into a critical category if it has all of the CUIs for a given
    critcal term and does not negate any of them. May return more than one critical result for category.
    :param pg_conn:
    :param report_id:
    :return: List of critical matches, if any, filtered for negation
    """
    crit_term_dict = loader.get_critical_dictionary(pg_conn)
    report_term_list = loader.get_candidate_list(pg_conn, report_id)
    frac_dict = get_critical_overlap(crit_term_dict, report_term_list)
    matches = get_exact_matches(frac_dict)

    negations = loader.get_negations(pg_conn, report_id)
    # If there are negations, check to see if they overlap with the critical terms
    for crit_term in matches:
        if is_negation(crit_term_dict, crit_term, negations):
            matches.remove(crit_term)

    return matches

def get_critical_dataframe(report_start_index, report_stop_index, is_critical_function=is_critical_simple):
    res_list = []
    pg_conn = loader.connect_postgres()
    for report_id in range(report_start_index, report_stop_index):
        crit_results_list = is_critical_function(pg_conn, report_id)
        for result in crit_results_list:
            res_list.append([report_id, result])
    pg_conn.close()
    return pd.DataFrame(res_list,columns=['report_id', 'critical_term'])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_index', help='Numerical index for which to start parsing terms.', default=2)
    parser.add_argument('--stop_index', help='Critical terms output file. This should be a ".xml" file', default=15)
    #parser.add_argument('critical_dictionary_id', help="Report id number of parsed critical term list", default=1)
    parser.add_argument('-v', '--visualize', action='store_true', help='Plot the frequency counts')
    args=parser.parse_args()
    start_index = int(args.start_index)
    stop_index = int(args.stop_index)

    df = get_critical_dataframe(start_index, stop_index)
    df_no_neg = get_critical_dataframe(start_index, stop_index, is_critical_function=get_critical_matches)

    # Print some results
    print "\n"
    print "#"*25
    print "%i reports were analyzed" %(stop_index - start_index)

    # Get top critical terms
    print "#"*25
    term_counts = df.critical_term.value_counts()
    print "The most common critical terms were: "
    print term_counts.head(n=10)

    # Number of reports with more than one critical results
    num_results_by_report = df.groupby('report_id').size()
    reports_with_more_than_one_crit_term = len(num_results_by_report)
    print "#"*25
    print "There were %i reports with more than one critical term" %reports_with_more_than_one_crit_term

    # Frequencies without using negation recognition
    print "#"*25
    print "The most common critical terms (negation not computed) were: "
    no_neg_counts = df_no_neg.critical_term.value_counts()
    print no_neg_counts.head(n=10)
    print "#"*25

    if args.visualize:
        # Plot frequencies of first 20 terms
        term_counts.head(n=20).plot(kind='bar')

        # Join negated with non-negated, and plot
        neg_compare = pd.concat([no_neg_counts,term_counts], axis=1)
        neg_compare.columns=['Before applying negation', 'After applying negation']
        neg_compare.plot(kind='bar')
    print "\n"
