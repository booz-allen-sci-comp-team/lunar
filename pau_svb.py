'''
This script loads and merges PAU data with medical records
'''
import pandas as pd
import pyodbc

# load PAU data from local extract
paupath = 'C:\DataExtracts\PAU\PAU.csv'
paudf = pd.DataFrame.from_csv(paupath, sep=',')

# load PAU data from local extract
conn = pyodbc.connect('DRIVER={SQL Server};SERVER=10.130.172.93;DATABASE=azADT;')
act_df = pd.read_sql('select top 10 * from azADT.dbo.WHC_PV1601', conn)



