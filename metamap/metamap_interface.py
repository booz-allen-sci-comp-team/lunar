"""

Interface to metamap for event extraction

Requires metamap.conf of format:


[CONF]
metamap_binary=<path to metamap executable>


"""
import os
import io
import ConfigParser
import subprocess
import csv
import pandas as pd
import re
from collections import namedtuple
import itertools
from operator import itemgetter, attrgetter
import json
from xml.sax.saxutils import quoteattr
import pickle

metamapOutputLinePattern = re.compile(r'^\d+\|') # output lines will match this
phraseInfoPattern = re.compile(r'"(.*)"-(ti|ab|tx)-(\d+)-"(.*)"-(\w+)-(\d+)')
MMITriggerInfo = namedtuple('MMITriggerInfo',['preferredName','loc','locUtteranceNum','matchText','lexicalCategory','negatedStatus','posList'])

# Load metamap stop list
moduleDirectory = os.path.join(os.path.dirname(__file__))
with open(os.path.join(moduleDirectory,'brown_common.pickle'),'r') as pickleFile:
    metamapStopList = pickle.load(pickleFile)
# Add additional terms to metamap stop list
additionalMetamapStopList = ['hi', 'was', 'bed', 'meds', 'find', 'wanted', 'but',
                             'life', 'used', 'symptom', 'able', 'said', 'plan', 'little',
                             'reading ', 'does', 'hit', 'thinking', 'read', 'sign',
                             'turning', 'related', 'offered', 'ten']
metamapStopList.extend(additionalMetamapStopList)


def tryint(instring):
    try:
        return int(instring)
    except ValueError:
        return None

class MetamapInterface(object):
    #[aggp] and [famg] age group and family group
    def __init__(self,optionsStr="-N",ade_types=None,score_threshold=0, temp_outfile="temp.txt"):
        self.optionsStr = optionsStr
        if ade_types is not None: # You have to be careful with mutable objects as defaults
            self.ade_types = ade_types
        else:
            self.ade_types = ['sosy', 'fndg', 'dsyn', 'mobd', 'patf', 'pdsu', 'topp']
        self.score_threshold = score_threshold
        self.temp_outfile = temp_outfile
        # Read config file to get binary location
        module_directory = os.path.dirname(__file__)
        config = ConfigParser.SafeConfigParser()
        with io.open(os.path.join(module_directory,'metamap.conf'),'r') as configFile:
            config.readfp(configFile)
        self.METAMAP_BINARY = config.get('CONF','metamap_binary') # The location of the metamap executable


    def metamap(self,infile):
        args = self.METAMAP_BINARY+" "+self.optionsStr+" "+infile+" "+self.temp_outfile
        p = subprocess.Popen(args, shell=True)
        p.wait()
        rows = []
        with open(self.temp_outfile, 'r') as f:
            reader = csv.reader(f, delimiter='|')
            for row in reader:
                rows.append(row)
        os.remove(self.temp_outfile)
        return rows
        
    def metamapstr(self,inputString):
        inputString += '\n' # there needs to be at least one newline in the input to process
        args = self.METAMAP_BINARY+" "+self.optionsStr
        p = subprocess.Popen(args, shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdoutdata, stderrdata = p.communicate(inputString)
        outLines = stdoutdata.splitlines()
        rows = [rowstr.split('|') for rowstr in outLines if metamapOutputLinePattern.match(rowstr) is not None] # Skip over the header lines that don't match the data pattern
        return rows
    
    def ade(self, doc, simplified=False):
        if not doc:
            return None
        df = pd.DataFrame(doc)
        df.columns=['1', '2', 'score', 'umls_term', 'cui', 'type_list', 'phrases', '3', 'pos', '4']
        df = df.drop(['1','2','3','4'],1)
        df['score'] = df['score'].convert_objects(convert_numeric=True)
        filtervalues = []
        for entries in df['type_list'].values:
            for ade in self.ade_types:
                filtervalue = 0
                if ade in entries:
                    filtervalue = 1
                    break
            filtervalues.append(filtervalue)
#        filtervalues = [1 for x in filtervalues] # Override type filtering
        df['type_filter'] = filtervalues
        aedf = df[(df['score'] > self.score_threshold) & (df['type_filter'] == 1)]
        if simplified:
            return aedf
        
        # Combine trigger information and positional information together, and process together.
        aedf['pos_list_grouped'] = [[[tuple(tryint(entry) for entry in position.split(':')) for position in positionList.split('^')] for positionList in posStr.split(';')] for posStr in aedf['pos']]
        aedf['rawphrases_list'] = [phrasestr.lstrip('[').rstrip(']').split(',') for phrasestr in aedf['phrases']]        
        aedf['matchAndPosRaw'] = aedf[['rawphrases_list', 'pos_list_grouped']].apply(lambda x: tuple(x), axis=1).apply(lambda x: zip(*x)) # just combine the two columns into a single column with tuples of the two values
        aedf['phrases_list'] = [[self.processPhraseInfoString(phraseinfo[0],phraseinfo[1]) for phraseinfo in phraselist] for phraselist in aedf['matchAndPosRaw']]

        # Take only non-negated matches.
        aedf['nonnegated_phrases_list'] = [[phraseinfo for phraseinfo in phraselist if phraseinfo.negatedStatus=='0'] for phraselist in aedf['phrases_list']]

        # For each concept, get one flat list of all nonnegated positional matches, regardless of the original raw text (for annotation)
        aedf['nonnegated_positions'] = [[posTup for phraseInfo in phraseList for posTup in phraseInfo.posList] for phraseList in aedf['nonnegated_phrases_list']]
        # For each concept, get one flat list of all raw text matches
        aedf['original_text_list'] = [[phraseInfo.matchText for phraseInfo in phraseList] for phraseList in aedf['nonnegated_phrases_list']]        
        # Count up non-negated matches
        aedf['count'] = [len(poslist) for poslist in aedf['nonnegated_positions']]

#        uniques = aedf.groupby('umls_term').first()
#        uniques['count'] = aedf.groupby('umls_term').count()['cui']
#        res = uniques[['count','cui','type_list']].sort_index(1)

        # Filter out concepts that do not contain at least one non-stop-word match
        aedf['nonStopWordCount'] = [sum(1 for text in textList if text.lower() not in metamapStopList) for textList in aedf['original_text_list']]
        aedf = aedf[(aedf['nonStopWordCount'] > 0)]
        
        # Return results        
        res = aedf[['count','cui','umls_term','type_list','nonnegated_positions','original_text_list','nonnegated_phrases_list']].sort_index(1)
        result = res.T.to_dict().values()
        return result
    
    def ade_list(self,filepath):
        doc = self.metamap(filepath)
        ade_dict = self.ade(doc)
        return ade_dict
        
    def ade_list_str(self,inputString):
        rows = self.metamapstr(inputString)
        if rows:
            ade_dict = self.ade(rows)
            return ade_dict
        
    def processPhraseInfoString(self,phrasestr,posList):
        matchObj = phraseInfoPattern.match(phrasestr)
        if matchObj is not None:
            groupResults = list(matchObj.group(1,2,3,4,5,6))
            groupResults[3] = re.sub('""','"',groupResults[3]) # undo escaping of double quotes in MMI fielded output
            return MMITriggerInfo(*groupResults,posList=posList)
        else:
            return MMITriggerInfo(None,None,None,None,None,None,None)


    def hasOverlap(self,inputTup, comparisonTups):
        inputStart = inputTup[0]
        inputEnd = inputTup[1]
        return any(targetStart <= inputStart < targetEnd or targetStart < inputEnd <= targetEnd for (targetStart,targetEnd) in comparisonTups)

    def getAnnotatedOutput(self,inputString):
        ### Get metamap output
        metamapOutput = self.ade_list_str(inputString)
        
        ### First get some basic count data to return
        countData = [{'umls_term':conceptInfo['umls_term'],'cui':conceptInfo['cui'],'count':conceptInfo['count']} for index,conceptInfo in metamapOutput.iteritems()]

        ### Now do annotation handling        
        
        
        allMatches = [[{'start':positionTup[0], 'rangeTup':(positionTup[0],positionTup[0]+positionTup[1]), 'nchar':positionTup[1], 'cui':concept['cui'], 'umls_term': concept['umls_term']} for positionTup in concept['nonnegated_positions']] for index,concept in metamapOutput.iteritems()]
        allMatches = list(itertools.chain.from_iterable(allMatches))
        
        # Test additions
        #allMatches.append({'cui':'test1','rangeTup':(80,85)})
        #allMatches.append({'cui':'test2','rangeTup':(82,87)})
        
        uniqueRanges = list(set(match['rangeTup'] for match in allMatches))
        
        uniqueRanges.sort(key=itemgetter(0)) # sort by start index
#        print(uniqueRanges)
        
        acceptedRanges = []
        rejectedRanges = []
        
        for thisRangeTup in uniqueRanges:
#            print('treating {}'.format(thisRangeTup))
            if thisRangeTup not in rejectedRanges and thisRangeTup not in acceptedRanges:
                if self.hasOverlap(thisRangeTup,acceptedRanges):
                    rejectedRanges.append(thisRangeTup)
#                    print('{} overlaps with accepted ranges'.format(thisRangeTup))
                else:
                    sameStartRanges = [rangeTup for rangeTup in uniqueRanges if rangeTup[0] == thisRangeTup[0]] # to reject possible matches that overlap with later things, add in:  and not self.hasOverlap(rangeTup,uniqueRanges); but this gets us issues if they all overlap
    #                print(sameStartRanges)
                    if len(sameStartRanges) > 1:
                        retainedRange = max(sameStartRanges,key=itemgetter(1)) # retain the longest match from those with the same start
                        sameStartRanges.remove(retainedRange)
                        acceptedRanges.append(retainedRange)
                        rejectedRanges.extend(sameStartRanges) # sameStartRanges now no longer has the retained range
#                        print('rejected ranges is now: {}'.format(rejectedRanges))
                    else:
                        acceptedRanges.append(thisRangeTup)
            else:
#                print('already rejected or accepted: {}'.format(thisRangeTup))
                pass
            
#        print(acceptedRanges)
        
#        charsCovered = (list(range(*rangeTup)) for rangeTup in acceptedRanges)
#        charsCovered = list(itertools.chain.from_iterable(charsCovered))
#        print(charsCovered)
#        charsCoveredSet = set(charsCovered)        
#        assert(len(charsCoveredSet) == len(charsCovered))
        
#        print(rejectedRanges)
        
        
#        acceptedRanges = uniqueRanges # override the filtering, just for testing purposes
        
        acceptedMatches = [{'rangeTup':rangeTup, 'matches':[match for match in allMatches if match['rangeTup'] == rangeTup]} for rangeTup in acceptedRanges]        
        acceptedMatches.sort(key=lambda rangeObj: rangeObj['rangeTup'][0])
        
        endOfLastSpan = 0
        
        outputSections = []
        
        inputText = inputString

        for nextSpan in acceptedMatches:
            spanStart, spanEnd = nextSpan['rangeTup']
            # Add in unmatched text since last match
            unmatchedText = inputText[endOfLastSpan:spanStart]
            outputSections.append(unmatchedText)
            matchedText = inputText[spanStart:spanEnd]
            #matchedTextJson = json.dumps(matchedText)            
            
            sourceInfo = 'metamap'
            
            symptomInfoObject = [match['umls_term'] for match in nextSpan['matches']]
            infoDict = {'source': sourceInfo,
                        'symptom': symptomInfoObject}            
            
            
            infoJson = json.dumps(infoDict)
            
            matchOutput = '<span data-post-tooltip data-word={0} '\
                    ' data-info={1}></span>'.format(quoteattr(matchedText),quoteattr(infoJson))            
            
            outputSections.append(matchOutput)
            endOfLastSpan = spanEnd

        finalUnmatchedText = inputText[endOfLastSpan:]
        outputSections.append(finalUnmatchedText)
        
        annotatedOutput = ''.join(outputSections)
        
        #print(annotatedOutput)
        
        return annotatedOutput, countData
