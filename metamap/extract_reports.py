import argparse
import getpass
import os
import postgres_conn as p
import psycopg2

output_dir = ''

def write_report(eid, report_text):
    output_file = '{0}/{1}.txt'.format(output_dir, eid)
    if os.path.exists(output_file):
        os.remove(output_file)
    with open(output_file, 'w') as f:
        f.write(report_text)
    print 'Created report {0}'.format(output_file)

def extract_reports(report_rows):
    eid = report_rows[0][0]
    report_text = ''
    for report_row in report_rows:
        if report_row[0] == eid:
            report_text += '\n' + report_row[2]
        else:
            write_report(eid, report_text)
            eid = report_row[0]
            report_text=report_row[2]

def get_reports(limit, offset):
    query = """SELECT eid, sequencenumber, observationvalue FROM raw_data_rad WHERE eid in
    (SELECT eid FROM raw_data_rad GROUP BY eid ORDER BY eid LIMIT {0} OFFSET {1})
    ORDER BY eid, fillerordernumber, sequencenumber""".format(limit, offset)
    pg_cur = pg_conn.cursor()
    pg_cur.execute(query)
    rows = pg_cur.fetchall()
    extract_reports(rows)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--numreports', nargs='?', default=10, help='Number of reports to extract starting from the top. To extract all reports enter ALL. (default=10)')
    parser.add_argument('-s', '--offset', nargs='?', default=0, help='Record offset. Number of rows to skip before beginning to return rows.')
    parser.add_argument('-o', '--outputdir', help='Output directory.')
    args=parser.parse_args()
    pg_conn = p.connect_postgres()
    output_dir = args.outputdir
    get_reports(args.numreports, args.offset)
    pg_conn.close()
