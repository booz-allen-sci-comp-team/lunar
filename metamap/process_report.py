import ms_config
import os

def process_all(input_dir_path, output_dir_path, metamap_path):
    for filename in os.listdir(input_dir_path):
        filename = os.path.splitext(filename)[0] # Remove extension from filename
        cmd = '%s/bin/metamap --XMLf %s/%s.txt %s/%s.xml' % (metamap_path, input_dir_path, filename, output_dir_path, filename)
        print cmd
        os.system(cmd)

if __name__ == '__main__':
    # Set MM_HOME to metmap home before running
    input_dir=ms_config.input_dir
    output_dir=ms_config.output_dir
    metamap_path=ms_config.metamap_path
    process_all(input_dir, output_dir, metamap_path)
