import getpass
import psycopg2

def connect_postgres():
    print 'Connecting to Postgres Database:'
    dbname = raw_input('Please database name: ' )
    dbuser = raw_input('Please enter database user: ')
    print 'Enter password for Postgres'
    pg_pass = getpass.getpass()
    conn_str = 'dbname={0} user={1} password={2}'.format(dbname, dbuser, pg_pass)
    pg_conn = psycopg2.connect(conn_str)
    return pg_conn

