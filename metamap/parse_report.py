import argparse
import csv
import ms_config
import os
import psycopg2
import postgres_conn as p
from metamaplib import *
from bs4 import BeautifulSoup

semtypes = {}
store = None

def parse_candidate(mapping, mid):
    for candidatetag in mapping.map_tag.find_all('candidate'):
        candidate = Candidate(candidatetag)
        if debug:
            candidate.display_candidate(semtypes)
        mapping.add_candidate(candidate)

def parse_mapping(phrase, pid):
    mid = None
    for mappingtag in phrase.phr_tag.find_all('mapping'):
        mapping = Mapping(mappingtag)
        if debug:
            mapping.display_mapping()
            print '\n'
        phrase.add_mapping(mapping)
        parse_candidate(mapping, mid)

def parse_report(report):
    pid = None
    uid = None
    for utttag in report.report_soup.find_all('utterance'):
        utt = Utterance(utttag)
        if debug:
            print '---------'
            utt.display_utterance()
            print '\n'
        if store:
            uid = utt.store(pg_conn, report_id)
        report.utterances.append(utt)
        for phrasetag in utt.utt_tag.find_all('phrase'):
            phrase = Phrase(phrasetag)
            if debug:
                phrase.display_phrase()
            utt.add_phrase(phrase)
            parse_mapping(phrase, pid)

def read_semtype_data(rows):
    for row in rows:
        global semtypes
        semtypes[row[0]]=row[2]

def load_semtypes(filename):
    with open(filename, 'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter='|')
        read_semtype_data(rows)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', help='Input directory')
    parser.add_argument('-s', '--store', action='store_true', help='Store the output to database')
    parser.add_argument('-d', '--debug', action='store_true', help='Print parsing output to the screen.')
    args=parser.parse_args()
    debug = args.debug
    semtypes_file = ms_config.lunar_path+'/get_started/semantic_types/sem_types'
    pg_conn = p.connect_postgres()
    for filename in os.listdir(args.input_dir):
        print 'Processing report {0}'.format(filename)
        # Create report
        report_path = args.input_dir+'/'+filename
        soup = BeautifulSoup(open(report_path), from_encoding='utf-8')
        report = Report(soup)
        report.eid = filename.split('.')[0]
        # Parse Report
        load_semtypes(semtypes_file)
        parse_report(report)
        # Store Report
        if args.store:
            report_id = report.store(pg_conn, report.eid, True)
            print 'Successfully stored report {0} with report ID {1}'.format(filename, report_id)
    pg_conn.commit()
    pg_conn.close()
