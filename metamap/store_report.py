import ms_config
import os

def store_all(input_dir_path, lunar_path):
    cmd = 'python %s/metamap/parse_report.py %s -s' % (lunar_path, input_dir_path)
    print cmd
    os.system(cmd)

if __name__ == '__main__':
    # Set MM_HOME to metmap home before running
    output_dir=ms_config.output_dir
    lunar_path=ms_config.lunar_path
    store_all(output_dir, lunar_path)
