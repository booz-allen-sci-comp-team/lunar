import psycopg2
from bs4 import BeautifulSoup
import json

class Candidate:
    def __init__(self, candidatetag):
        self.can_tag = candidatetag
        self.can_xml = str(candidatetag.prettify())
        self.cui = candidatetag.candidatecui.contents[0]
        self.semtype = candidatetag.semtype.contents[0]
        self.matched = candidatetag.candidatematched.contents[0]
        self.preferred = candidatetag.candidatepreferred.contents[0]
        self.can_score = candidatetag.candidatescore.contents[0]

    def display_candidate(self, semtypes):
        print self.cui, self.can_score, self.semtype, semtypes[self.semtype], self.matched, self.preferred

    def store(self, pg_conn, mid):
        pg_cur = pg_conn.cursor()
        query = """INSERT INTO candidate (cui, matched, preferred, semtype) VALUES
        (%s, %s, %s, %s) RETURNING cid"""
        pg_cur.execute(query, (self.cui, self.matched, self.preferred, self.semtype))
        row = pg_cur.fetchone()
        pg_conn.commit()
        query = """INSERT INTO rel_map_can (mid, cid, can_score) VALUES (%s, %s, %s)"""
        pg_cur.execute(query, (mid, row[0], self.can_score))
        pg_conn.commit()

class Mapping:
    def __init__(self, mappingtag):
        self.map_tag = mappingtag
        self.map_xml = str(mappingtag.prettify())
        self.map_score = mappingtag.mappingscore.contents[0]
        self.candidates = []

    def add_candidate(self, candidate):
        self.candidates.append(candidate)

    def display_mapping(self):
        print 'mapping ', self.map_score

    def same_candidates(self, othermapping):
        return set(self.candidates) == set(othermapping.candidates)

    def store(self, pg_conn, pid):
        pg_cur = pg_conn.cursor()
        query = """INSERT INTO mapping (mid) VALUES (DEFAULT) RETURNING mid"""
        pg_cur.execute(query)
        row = pg_cur.fetchone()
        pg_conn.commit()
        query = """INSERT INTO rel_phr_map (pid, mid, map_score) VALUES (%s, %s, %s)"""
        pg_cur.execute(query, (pid, row[0], self.map_score))
        pg_conn.commit()
        return row[0]

class Phrase:
    def __init__(self, phrasetag):
        self.phr_tag = phrasetag
        self.phr_xml = str(phrasetag.prettify())
        self.phr_text = phrasetag.phrasetext.contents[0]
        self.mappings = []

    def display_phrase(self):
        print 'Phrase ', self.phr_text

    def add_mapping(self, mapping):
        self.mappings.append(mapping)

    def store(self, pg_conn, uid):
        pg_cur = pg_conn.cursor()
        query = """INSERT INTO phrase (phr_text) VALUES ($$0$$) RETURNING pid""".format(self.phr_text)
        pg_cur.execute(query)
        row = pg_cur.fetchone()
        pg_conn.commit()
        query = """INSERT INTO rel_utt_phr (uid, pid) VALUES (%s, %s)"""
        pg_cur.execute(query, (uid, row[0]))
        pg_conn.commit()
        return row[0]

class Utterance:
    def __init__(self, utterancetag):
        self.utt_tag = utterancetag
        self.utt_xml = str(utterancetag.prettify())
        self.utt_text = utterancetag.utttext.contents[0]
        self.phrases = []

    def display_utterance(self):
        print 'Utterance ', self.utt_text

    def add_phrase(self, phrase):
        self.phrases.append(phrase)

    def store(self, pg_conn, report_id):
        pg_cur = pg_conn.cursor()
        query = """INSERT INTO utterance (utt_text) VALUES ($${0}$$) RETURNING uid""".format(self.utt_text)
        pg_cur.execute(query)
        row = pg_cur.fetchone()
        pg_conn.commit()
        pg_cur = pg_conn.cursor()
        query = "INSERT INTO rel_rep_utt (report_id, uid) VALUES (%s, %s)"
        pg_cur.execute(query, (report_id, row[0]))
        pg_conn.commit()

        # return utterance id
        return row[0]

class Negation:
    def __init__(self, neg_tag):
        self.neg_tag = neg_tag
        self.neg_xml = str(neg_tag.prettify())
        self.neg_type = neg_tag.negtype.contents[0]
        self.neg_trigger = neg_tag.negtrigger.contents[0]
        self.neg_concepts = []  # list of dictionaries of the negated cui and negated matched term

        # Parse and store negated concepts on instantiation
        self.parse_neg_concepts()

    def add_neg_concept(self, neg_concept_tag):
        cui = neg_concept_tag.negconccui.contents[0]
        matched = neg_concept_tag.negconcmatched.contents[0]
        dict = {"neg_concept_cui": cui, "neg_concept_matched": matched}
        self.neg_concepts.append(dict)

    def parse_neg_concepts(self):
        for neg_concept_tag in self.neg_tag.find_all("negconcept"):
            self.add_neg_concept(neg_concept_tag)

    def store(self, pg_conn, report_id):
        pg_cur = pg_conn.cursor()
        neg_concept_json = json.dumps(self.neg_concepts)
        query = """INSERT INTO negation (neg_type, neg_trigger, neg_concepts) VALUES (%s, %s, %s) RETURNING nid """
        pg_cur.execute(query, (self.neg_type, self.neg_trigger, neg_concept_json))
        row = pg_cur.fetchone()
        pg_conn.commit()

        pg_cur = pg_conn.cursor()
        query = """INSERT INTO rel_rep_neg (report_id, nid) VALUES (%s, %s)"""
        pg_cur.execute(query, (report_id, row[0]))
        pg_conn.commit()

        # Return negated concept id
        return row[0]

class Report():
    def __init__(self, report_soup):
        self.eid = ''
        self.report_soup = report_soup
        self.xml_string = str(self.report_soup.prettify())
        self.utterances = []
        self.negations = []

        # Parse and store negations on instantiation
        self.parse_negations()

    def parse_negations(self):
        negations_list = self.report_soup.find_all('negations')
        for negations in negations_list:
            for negation_tag in negations.find_all('negation'):
                negation = Negation(negation_tag)
                self.negations.append(negation)

    def add_utterance(self, utterance):
        self.utterances.append(utterance)

    def store(self, pg_conn, eid, store_negations):
        pg_cur = pg_conn.cursor()
        query = """INSERT INTO reports (eid) VALUES ('{0}') RETURNING report_id""".format(self.eid)
        pg_cur.execute(query)
        row = pg_cur.fetchone()
        pg_conn.commit()

        report_id = row[0]

        # Store negations
        if store_negations:
            for negation in self.negations:
                negation.store(pg_conn, report_id)

        # Store utterance, phrase, mapping, and candidates
        for utterance in self.utterances:
            uid = utterance.store(pg_conn, report_id)
            for phrase in utterance.phrases:
                pid = phrase.store(pg_conn, uid)
                for mapping in phrase.mappings:
                    mid = mapping.store(pg_conn, pid)
                    for candidate in mapping.candidates:
                        candidate.store(pg_conn, mid)

        return report_id
