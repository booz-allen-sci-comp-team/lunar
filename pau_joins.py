import pymssql
import getpass
from os import getenv

import pandas as pd

#######
# Preliminary steps
########
'''
First install necessary system pacakges:
yum install gcc gcc-c++, python-devel, freetds, freetds-devel, unixODBC, unixODBC-devel

*NOTE* may only need freetds-devel, may try this first

Then pip install pymssql
'''


def mssql_connect(server=None, database=None):
    print server
    if server is None:
        server = raw_input("Please enter server IP: ")

    if database is None:
        database = raw_input("Please enter database : ")
    user = raw_input("User name: ")
    pw = getpass.getpass("Password: ")
    conn = pymssql.connect(server, user, pw, database)
    return conn

def get_columns(pg_conn, table_name):
    query = "select column_name, * from information_schema.columns where table_name='%s' and table_schema='dbo';" %table_name
    cursor = pg_conn.cursor()
    cursor.execute(query)
    data = cursor.fetchall()
    columns = []
    for row in data:
        col_name = row[0]
        columns.append(col_name)

    return columns


def join_pau_data():

    server = '10.130.172.93'
    adt_connection = mssql_connect(server='10.130.172.93', database="azADT")

    pau_connection = mssql_connect(server='198.50.80.31', database='pau')

    # Get demographic data
    demographic_columns = get_columns(adt_connection, 'WHC_PID601')
    adt_cursor = adt_connection.cursor()


    # Get ICD9/10 data
    dg_cols = get_columns(adt_connection, 'WHC_DG1601')
    query = "SELECT top 100000 * from dbo.WHC_DG1601"
    adt_cursor.execute(query)
    dg_data = adt_cursor.fetchall()
    dg_data = pd.DataFrame(dg_data, columns=dg_cols)



    #cursor.execute("SELECT top 10 * from dbo.WHC_OBX603")
    #adt_cursor.execute("SELECT EID, BirthDate, PatientMedicalRecordNumber, GenderCode from dbo.WHC_PID601")
    adt_cursor.execute("SELECT top 100000 * from dbo.WHC_PID601")
    demographic_data = adt_cursor.fetchall()
    demographic_data = pd.DataFrame(demographic_data, columns=demographic_columns)
    #for row in demographic_data:
    #    print row
    adt_connection.close()





    # Get PAU data
    pau_cols = get_columns(pau_connection, 's_all_refactored')
    pau_cursor = pau_connection.cursor()
    query = 'SELECT * from dbo.s_all_refactored'
    pau_cursor.execute(query)
    pau_data = pau_cursor.fetchall()
    pau_data = pd.DataFrame(pau_data, columns=pau_cols)

    # Get ICD9/ICD10 data


    return demographic_data, pau_data, dg_data, pau_cols



dem_data, pau_data, dg_data, pau_cols = join_pau_data()

#if __name__ == "__main__":
#    join_pau_data()