import getpass
import psycopg2

def get_critical_dictionary(pg_conn, critical_list_id=1):
    """

    :param pg_conn:
    :return: Dictionary where key is the critical term and value is list of metamap CUIs
    """
    pg_cur = pg_conn.cursor()
    query = """ select utterance.uid, utterance.utt_text, candidate.preferred, candidate.cui
    from candidate, utterance, rel_rep_utt, rel_utt_phr, rel_phr_map, rel_map_can
    where rel_rep_utt.report_id={0} AND utterance.uid=rel_rep_utt.uid
    AND rel_utt_phr.uid=utterance.uid AND rel_utt_phr.pid=rel_phr_map.pid
    AND rel_phr_map.mid = rel_map_can.mid AND rel_map_can.cid=candidate.cid;
    """.format(critical_list_id)
    pg_cur.execute(query)
    result_list = pg_cur.fetchall()
    result_dict = {}
    for row in result_list:
        key = row[1] # key is text of the critical result
        value = row[3] #value is cui of the mapped candidate
        result_dict.setdefault(key, []).append(value)

    return result_dict

def get_candidate_list(pg_conn, report_id):
    """

    :param pg_conn:
    :param report_id: Value of report_id from postgres table "reports"
    :return: List of all metamap candidate concepts for that report
    """
    pg_cur = pg_conn.cursor()

    query = """SELECT candidate.cui
    FROM candidate, rel_rep_utt, rel_utt_phr, rel_phr_map, rel_map_can
    WHERE rel_rep_utt.report_id={0}
    AND rel_utt_phr.uid=rel_rep_utt.uid AND rel_utt_phr.pid=rel_phr_map.pid
    AND rel_phr_map.mid = rel_map_can.mid AND rel_map_can.cid=candidate.cid;
    """.format(report_id)

    pg_cur.execute(query)
    candidate_list = pg_cur.fetchall()
    candidate_list = [row[0] for row in candidate_list]
    return candidate_list

def get_negations(pg_conn, report_id):
    """

    :param pg_conn:
    :param report_id:
    :return: List of CUI which were negated in the given report
    """
    query="""SELECT neg_concepts
    FROM negation, rel_rep_neg
    WHERE rel_rep_neg.report_id={0}
    AND rel_rep_neg.nid = negation.nid;""".format(report_id)
    pg_cur = pg_conn.cursor()
    pg_cur.execute(query)
    negation_list = pg_cur.fetchall()

    # Drop tuple notation
    negation_list = [tup[0] for tup in negation_list]

    # Condense into single list of dictionaries
    negation_list = [dict for sublist in negation_list for dict in sublist]

    # From there extract cuis
    negation_list = [dict["neg_concept_cui"] for dict in negation_list]
    return negation_list


def connect_postgres():
    print 'Connecting to Postgres Database:'
    dbname = raw_input('Please database name: ' )
    dbuser = raw_input('Please enter database user: ')
    print 'Enter password for Postgres'
    pg_pass = getpass.getpass()
    conn_str = 'dbname={0} user={1} password={2}'.format(dbname, dbuser, pg_pass)
    pg_conn = psycopg2.connect(conn_str)
    return pg_conn


if __name__ == "__main__":
    pg_conn = connect_postgres()
    r1 = get_critical_dictionary(pg_conn)
    get_candidate_list(pg_conn, 2)
    pg_conn.close()
